@extends('layouts.app')
@section('header')
    <strong>Create Invoice</strong>
@endsection
@section('content')
<div class="card card-gray">
    <div class="toolbox">
        <button type="button" onclick="save()" class="btn btn-oval btn-primary btn-sm"> 
            <i class="fa fa-save "></i> Save
        </button>
        <a href="{{route('invoice.index')}}"class="btn btn-warning btn-oval btn-sm">
            <i class="fa fa-reply"></i> Back
        </a>
    </div>
    <div class="card-block" id='app'>
        @component('layouts.coms.alert')
        @endcomponent
        <form action="#" method="POST">
            {{csrf_field()}}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="student_id" class="col-sm-4 form-control-label">Student <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                           <div class="row">
                                <div class="col-sm-11" style='padding-right:1px'>
                                    <select name="student_id" id="student_id" class="form-control chosen-select" required>
                                        <option value="">--Select--</option>
                                        @foreach($students as $c)
                                            <option value="{{$c->id}}">({{$c->code}}) - {{$c->kh_name}} - {{$c->en_name}}</option>
                                        @endforeach
                                    </select>
                                  
                                </div>
                                <div class="col-sm-1" style='padding-left:0'>
                                    <button class="btn btn-primary btn-add" type='button' data-toggle='modal' 
                                        data-target='#studentModal'>+</button>
                                </div>
                           </div>
                            
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="bill_no" class="col-sm-4 form-control-label">Invoice No.</label>
                        <div class="col-sm-8">
                            <input type="text" name='bill_no' id="bill_no" 
                                class='form-control' value="INV000{{$inv+1}}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="invoice_date" class="col-sm-4 form-control-label">Invoice Date<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="date" name='invoice_date' id="invoice_date" 
                                class='form-control' value="{{date('Y-m-d')}}" required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="due_date" class="col-sm-4 form-control-label">Due Date<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="date" name='due_date' id="due_date" 
                                class='form-control' value="{{date('Y-m-d')}}" required>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    
                    <div class="form-group row">
                        <label for="category" class="col-sm-4 form-control-label">Reference</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" name='reference' id='reference'>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="recurring" class="col-sm-4 form-control-label">Is Recurring</label>
                        <div class="col-sm-8">
                            <select name="recurring" id="recurring" class="form-control">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="term" class="col-sm-4 form-control-label">Recurring Term</label>
                        <div class="col-sm-8">
                            <select name="term" id="term" class="form-control">
                                <option value="0"> </option>
                                @foreach($terms as $t)
                                    <option value="{{$t->day}}">{{$t->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="note" class="col-sm-4 form-control-label">Note</label>
                        <div class="col-sm-8">
                            <textarea name="note" id="note" cols="30" rows="2" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-6">
                    
                    <h5 class="text-success">Invoice Items</h5>
                </div>

            </div>
            <div class="row">
                <div class="col-md-5 col-sm-11">
                    <div class="row">
                        <div class="col-sm-11" style='padding-right:1px'>
                            <select name="item" id="item" class="form-control chosen-select"  onchange="getPrice()">
                                <option value="">--Select--</option>
                                @foreach($items as $p)
                                    <option value="{{$p->id}}" pname="{{$p->name}}">{{$p->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-sm-1" style='padding-left: 0'>
                            <button class="btn btn-primary btn-add" type='button' data-toggle='modal' 
                                data-target='#productModal'>+</button>
                        </div>
                    </div>
                   
                </div>
                <div class="col-md-1 col-sm-12">
                    <input type="text" class="form-control" id="quantity" placeholder='Qty' title="Quantity">
                </div>
                
                <div class="col-md-2 col-sm-12">
                    <input type="text" class='form-control' id="price" placeholder='Price' title="Price">
                </div>
                <div class="col-md-2 col-sm-12">
                    <input type="text" class="form-control" id='discount' placeholder="Discount(%)" title="Discount">
                </div>
                <div class="col-md-1 col-sm-1">
                    <button class="btn btn-primary btn-oval" type="button" onclick="addItem()"> Add </button>
                </div>
            </div>
            <p></p>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th>Subtotal</th>
                        <th style="width:150px">Action</th>
                    </tr>
                </thead>
                <tbody id="data">
                    
                </tbody>
            </table>
            <p class="text-right">
                <strong class="text-danger">Total: &nbsp;&nbsp;$ <span id="total">0</span></strong>
            </p>
            <hr>
        </form>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form action="#">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Edit Item</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div class="form-group row">
                  <label for="product1" class="col-sm-3">Item<span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                          <select name="item1" id="item1" class="form-control chosen-select" onchange="getPrice1()" required>
                              <option value=""> --ជ្រើសរើស-- </option>
                              @foreach($items as $item)
                                  <option value="{{$item->id}}">{{$item->name}}</option>
                              @endforeach
                          </select>
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="quanity1" class="col-sm-3" >Quantity</label>
                      <div class="col-sm-8">
                          <input type="number" step="0.1" min="1" class="form-control" name="quanity1" id="quanity1">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="price1" class="col-sm-3" title="">Price($)</label>
                      <div class="col-sm-8">
                          <input type="number" step="1" min="0" class="form-control" name="price1" id="price1">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label for="discount1" class="col-sm-3">Discount(%)</label>
                      <div class="col-sm-8">
                          <input type="number" step="0.1" min="0" class="form-control" name="discount1" id="discount1" >
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <div style='padding: 5px'>
                      <button type="button" class="btn btn-primary" id="btn" onclick="saveItem()">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </form>
    </div>
  </div> 
@endsection
@section('js')
    <script src="{{asset('js/invoice.js')}}"></script>
	<script>
        var url = "{{url('/')}}";
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#payment").addClass("active open");
			$("#payment_collapse").addClass("collapse in");
            $("#menu_invoice").addClass("active");
			
        })
    </script>
@endsection