@extends('layouts.app')
@section('header')
    <strong>Due Invoices</strong>
@endsection
@section('content')
<div class="card card-gray">
    <div class="toolbox">
        <a href="{{route('invoice.create')}}" class="btn btn-primary btn-oval btn-sm">
            <i class="fa fa-plus-circle"></i> Create
        </a>
    </div>
	<div class="card-block">
        @component('layouts.coms.alert')
        @endcomponent
       <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Invoice No.</th>
                <th>Date</th>
                <th>Due Date</th>
                <th>Total</th>
                <th>Paid</th>
                <th>Due Amount</th>
                <th>Student</th>
            </tr>
        </thead>
        <tbody>			
            <?php
                $pagex = @$_GET['page'];
                if(!$pagex)
                    $pagex = 1;
                $i = config('app.row') * ($pagex - 1) + 1;
            ?>
            @foreach($invoices as $p)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        <a href="{{route('invoice.detail', $p->id)}}">INV00{{$p->id}}</a>
                    </td>
                    <td>{{$p->invoice_date}}</td>
                    <td>{{$p->due_date}}</td>
                    <td>$ {{$p->total}}</td>
                    <td>$ {{$p->paid}}</td>
                    <td>$ {{$p->total - $p->paid}}</td>
                    <td>
                        <a href="{{route('student.detail', $p->student_id)}}">{{$p->kh_name}}</a>
                    </td>
                   
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$invoices->links()}}
	</div>
</div>
@endsection
@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#payment").addClass("active open");
			$("#payment_collapse").addClass("collapse in");
            $("#menu_due_invoice").addClass("active");
			
        })
    </script>
@endsection