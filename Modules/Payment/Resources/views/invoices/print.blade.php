<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Print Invoice | Vdoo ERP</title>
    <style>
        @font-face{
            font-family: kh;
            src: url('{{asset('fonts/KhmerOSmuollight.ttf')}}');
        }
        @font-face{
            font-family: kh1;
            src: url('{{asset('fonts/KhmerOSsiemreap.ttf')}}');
        }
        .khmoul{
            font-family: kh;
        }
        .kh{
            font-family: kh1;
        }
        th, td, div, p, span, label{
            font-family: kh1;
        }
    </style>
</head>
<body>
    <table width="95%">
        <tr>
            <td with="50%">
                <img src="{{asset($com->logo)}}" alt="" width="200">
            </td>
            <td>
                <p style="text-align: center;">
                    <strong class="text-primary khmoul">{{$com->kh_name}}</strong>
                    <br>
                    <strong>{{$com->en_name}}</strong>
                    <br>
                    <i>{{$com->address}}a</i>
                   
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <strong>Branch: </strong> {{$com->name}}
            </td>
        </tr>
    </table>
    <h3 align='center'><u>Student Invoice</u></h3>
    <table width="95%">
        <tr>
            <td>Invoice No.</td>
            <td>:</td>
            <td><b>INV00{{$invoice->id}}</b></td>
            <td>Student</td>
            <td>:</td>
            <td><b>{{$invoice->kh_name}}</b></td>
        </tr>
        <tr>
            <td>Invoice Date</td>
            <td>:</td>
            <td>{{$invoice->invoice_date}}</td>
            <td>Due Date</td>
            <td>:</td>
            <td>{{$invoice->due_date}}</td>
        </tr>
        <tr>
            <td>Reference</td>
            <td>:</td>
            <td>{{$invoice->reference}}</td>
            <td>Note</td>
            <td>:</td>
            <td>{{$invoice->note}}</td>
        </tr>
        <tr>
            <td>Issued By</td>
            <td>:</td>
            <td>{{$invoice->name}}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <h4>Invoice Items</h4>
    <table width="95%" border="1" cellspacing="0" cellpadding='5'>
        <thead>
            <tr>
                <th>#</th>
                <th>Item</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Discount</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            @php($i=1)
            @foreach($items as $p)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$p->name}}</td>
                    <td>{{$p->quantity}}</td>
                    <td>$ {{$p->unitprice}}</td>
                    <td>{{$p->discount}}%</td>
                    <td>$ {{$p->subtotal}}</td>
                </tr>
            @endforeach
            <tr>
                <td colspan="5" align="right">Total Amount</td>
                <td class='text-danger'>$ {{$invoice->total}}</td>
            </tr>
            <tr>
                <td colspan="5" align='right'>Paid Amount</td>
                <td class='text-success'>$ {{$invoice->paid}}</td>
            </tr>
            <tr>
                <td colspan="5" align="right">Due Amount</td>
                <td class='text-danger'>$ {{$invoice->total - $invoice->paid}}</td>
            </tr>
        </tbody>
    </table>
    <p>&nbsp;</p>
    <table width="95%">
        <tr>
            <td width="45%">
                <p style="text-align: center;">
                    Issued by <br><br><br>
                    ________________
                </p>
            </td>
            <td>
                <p style="text-align: center;">
                    Received by <br><br><br>
                    ________________
                </p>
            </td>
        </tr>
    </table>
  
    <script>
        print();
    </script>
</body>
</html>