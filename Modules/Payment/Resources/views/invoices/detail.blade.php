@extends('layouts.app')
@section('header')
    <strong>Invoice Detail</strong>
@endsection
@section('content')
<div class="card card-gray">
    <div class="toolbox">
        <a href="{{route('invoice.create')}}" class="btn btn-primary btn-oval btn-sm">
            <i class="fa fa-plus-circle"></i> Create
        </a>
        <a href="{{route('invoice.print', $invoice->id)}}" class="btn btn-primary btn-oval btn-sm" target="_blank">
            <i class="fa fa-print"></i> Print
        </a>
        <a href="{{route('invoice.index')}}"class="btn btn-warning btn-oval btn-sm">
            <i class="fa fa-reply"></i> Back
        </a>
    </div>
    <div class="card-block" id='app'>
        @component('layouts.coms.alert')
        @endcomponent
        <form action="#" method="POST">
            {{csrf_field()}}
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="bill_no" class="col-sm-4">Invoice No.</label>
                        <div class="col-sm-8">
                            : INV000{{$invoice->id}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label class="col-sm-4">Student</label>
                        <div class="col-sm-8">
                            : {{$invoice->kh_name}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label class="col-sm-4">Invoice Date</label>
                        <div class="col-sm-8">
                            : {{$invoice->invoice_date}}
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label for="due_date" class="col-sm-4">Due Date</label>
                        <div class="col-sm-8">
                            : {{$invoice->due_date}}
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-sm-6">
                    
                    <div class="form-group row">
                        <label class="col-sm-4">Reference</label>
                        <div class="col-sm-8">
                            : {{$invoice->reference}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4">Is Recurring</label>
                        <div class="col-sm-8">
                            : {{$invoice->recurring?'Yes':'No'}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-4">Recurring Term</label>
                        <div class="col-sm-8">
                            : {{$invoice->recurring_day}} (Days)
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group row">
                        <label class="col-sm-4">Note</label>
                        <div class="col-sm-8">
                            : {{$invoice->note}}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-sm-6">
                    
                    <h5 class="text-success">Invoice Items</h5>
                </div>

            </div>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Discount</th>
                        <th>Subtotal</th>
                    </tr>
                </thead>
                <tbody id="data">
                    @php($i=1)
                    @foreach($items as $p)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$p->name}}</td>
                            <td>{{$p->quantity}}</td>
                            <td>$ {{$p->unitprice}}</td>
                            <td>{{$p->discount}}%</td>
                            <td>$ {{$p->subtotal}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="5" class='text-right text-danger'>Total Amount</td>
                        <td class='text-danger'>$ {{$invoice->total}}</td>
                    </tr>
                    <tr>
                        <td colspan="5" class='text-right text-success'>Paid Amount</td>
                        <td class='text-success'>$ {{$invoice->paid}}</td>
                    </tr>
                    <tr>
                        <td colspan="5" class='text-right text-danger'>Due Amount</td>
                        <td class='text-danger'>$ {{$invoice->total - $invoice->paid}}</td>
                    </tr>
                </tbody>
            </table>
           
            <h5 class="text-primary">Payments 
                @if(($invoice->total - $invoice->paid) > 0)
                <a href="#" class="btn btn-primary btn-sm btn-oval" data-toggle="modal" data-target="#paymentModal">
                        <i class="fa fa-plus"></i>  Add Payment
                </a>
                @endif
            </h5>
            <div class="row">
                <div class="col-sm-8">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Amount</th>
                                <th>Method</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i=1)
                            @foreach($payments as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p->pay_date}}</td>
                                    <td>$ {{$p->amount}}</td>
                                    <td>{{$p->method}}</td>
                                    <td> 
                                        <a href="{{url('payment/invoice/payment/delete/'.$p->id)}}" class="text-danger" title="Delete" 
                                            onclick="return confirm('You want to delete?')">
                                            <i class="fa fa-trash"></i> 
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <form action="{{url('payment/invoice/payment/save')}}" method="POST">
          {{csrf_field()}}
          <input type="hidden" name="invoice_id" value="{{$invoice->id}}">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Register Payment</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                 
                  <div class="form-group row">
                      <label class="col-sm-3" >Date <span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                          <input type="date" class="form-control" name="pay_date" required 
                              value="{{date('Y-m-d')}}">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-3" title="">Amount($) <span class="text-danger">*</span></label>
                      <div class="col-sm-8">
                          <input type="number" min="0" step="0.01" class="form-control" name="amount" required 
                              value="{{$invoice->due_amount}}" max="{{$invoice->due_amount}}">
                      </div>
                  </div>
                  <div class="form-group row">
                      <label class="col-sm-3">Method</label>
                      <div class="col-sm-8">
                          <select name="method" id="method" class="form-control">
                              <option value="Cash">Cash</option>
                              <option value="Bank">Bank</option>
                              <option value="Check">Check</option>
                          </select>
                      </div>
                  </div>
              </div>
              <div class="modal-footer">
                  <div style='padding: 5px'>
                      <button type="submit" class="btn btn-primary">Save</button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </div>
      </form>
    </div>
  </div>  
@endsection
@section('js')
	<script>
        var url = "{{url('/')}}";
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#payment").addClass("active open");
			$("#payment_collapse").addClass("collapse in");
            $("#menu_invoice").addClass("active");
			
        })
    </script>
@endsection