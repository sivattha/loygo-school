@foreach($categories as $c)
            <div class="row">
                <div class="col-md-12 pr-0 pl-0">
                    <h3 class="underline"> {{$c->name}}</h3>
                    <div id="triangle-topleft2"></div>
                </div>
            </div>
            <p class="text-center sub-title">{!!$c->sub_title!!}</p>
            <?php  
                $artists = DB::table('artists')->where('active', 1)->where('category_id', $c->id)->orderBy('order')->get();
            ?>
    
            <div class="row">
                @foreach($artists as $a)
                <?php  
                    $user_id = Request::header('user-agent');
                    $votes = DB::table('votes')->where('artist_id', $a->id)->where('category_id', $a->category_id)->count();
                    
                    $voted = DB::table('votes')
                        ->where('user_id', $user_id)
                        ->where('artist_id', $a->id)
                        ->where('category_id',$a->category_id)
                        ->count();
                    
                        $voted_cat = DB::table('votes')
                        ->where('user_id',$user_id)
                        ->where('category_id',$a->category_id)
                        ->count();
                        
                ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 frame-vote mb-4 col-6">
                        <div class="card h-100">
                            <img class="card-img-top" src="{{url($a->featured_image)}}" alt="">
                            <div class="card-body">
                                <h4 class="card-title">{{$a->name}}<span class="float-right text-secondary"><img src="{{asset('fronts/img/voted.png')}}" class="like"> {{$votes}}</span>  </h4>
                                {{-- <form action="{{url('vote/list/artist/')}}" method="POST"> --}}
                                <form>
                                    {{-- {{csrf_field()}} --}}
                                    @if($voted != 0 && $voted_cat != 0)
                                    <span class="float-right vote">voted</span> 
                                    @endif
                                    @if($voted == 0 && $voted_cat == 0)
                                    {{-- <input type="hidden" name="category_id" value="{{$a->category_id}}">
                                    <input type="hidden" name="artist_id" value="{{$a->id}}">
                                    <input type="submit" class="btn btn-warning btn-sm btn-block" value="Vote"> --}}
                                        <a href="{{url('vote/list/artist?category_id='.$a->category_id . '&artist_id='.$a->id)}}" 
                                            class="btn btn-warning btn-sm btn-block">Vote</a>
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach