<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('payment')->group(function() {
    Route::get('/', 'PaymentController@index');
    // invoice
    Route::get('invoice', 'InvoiceController@index')->name('invoice.index');
    Route::get('invoice/create', 'InvoiceController@create')->name('invoice.create');
    Route::get('invoice/detail/{id}', 'InvoiceController@detail')->name('invoice.detail');
    Route::get('invoice/print/{id}', 'InvoiceController@print')->name('invoice.print');
    Route::post('invoice/save', 'InvoiceController@save');
    Route::post('invoice/payment/save', 'InvoiceController@save_payment');
    Route::get('invoice/payment/delete/{id}', 'InvoiceController@delete_payment');
    Route::get('invoice/item/price/{id}', 'InvoiceController@get_price');
    Route::get('invoice/payment', 'InvoiceController@payment')
        ->name('invoice.payment');
    Route::get('invoice/due', 'InvoiceController@due')
        ->name('invoice.due');
});
