<?php

namespace Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class InvoiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('invoice', 'l'))
        {
            return view('permissions.no');
        }
        $data['invoices'] = DB::table('invoices')
            ->join('students', 'invoices.student_id', 'students.id')
            ->whereIn('invoices.branch_id', Helper::arr_branch())
            ->orderBy('invoices.id', 'desc')
            ->select('invoices.*', 'students.kh_name', 'students.en_name')
            ->paginate(config('app.row'));
        return view('payment::invoices.index', $data);
    }
    public function detail($id)
    {
        if(!Right::check('invoice', 'l')){
            return view('permissions.no');
        }

        $invoice = DB::table('invoices')
            ->leftJoin('students', 'invoices.student_id', 'students.id')
            ->where('invoices.id', $id)
            ->select('invoices.*', 'students.kh_name', 'students.en_name')
            ->first();
        
        $data['invoice'] = $invoice;
        $data['items'] = DB::table('invoice_details')
            ->join('items', 'invoice_details.item_id', 'items.id')
            ->where('invoice_details.invoice_id', $id)
            ->select('invoice_details.*', 'items.name')
            ->get();
        $data['payments'] = DB::table('invoice_payments')
            ->where('invoice_id', $id)
            ->get();
        return view('payment::invoices.detail', $data);
    }
    public function print($id)
    {
        if(!Right::check('invoice', 'l')){
            return view('permissions.no');
        }

        $invoice = DB::table('invoices')
            ->leftJoin('students', 'invoices.student_id', 'students.id')
            ->leftJoin('users', 'invoices.create_by', 'users.id')
            ->where('invoices.id', $id)
            ->select('invoices.*', 'students.kh_name', 'students.en_name',
                'users.name')
            ->first();
        
        $data['invoice'] = $invoice;
        $data['items'] = DB::table('invoice_details')
            ->join('items', 'invoice_details.item_id', 'items.id')
            ->where('invoice_details.invoice_id', $id)
            ->select('invoice_details.*', 'items.name')
            ->get();
        $data['com'] = DB::table('branches')
            ->join('companies', 'branches.company_id', 'companies.id')
            ->where('branches.id', $invoice->branch_id)
            ->select('companies.*', 'branches.name')
            ->first();
        return view('payment::invoices.print', $data);
    }
    public function create()
    {
        $data['students'] = DB::table('students')
            ->where('active', 1)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        $data['items'] = DB::table('items')
            ->where('active', 1)
            ->get();
        $linv = DB::table('invoices')->orderBy('id', 'desc')->first();
        $data['inv'] = $linv?$linv->id:0;
        $data['terms'] = DB::table('recurring_terms')
            ->get();
        return view('payment::invoices.create', $data);
    }

    public function save(Request $r)
    {
        if(!Right::check('invoice', 'i')){
            return 0;
        }
        $m = json_encode($r->invoice);
        $m = json_decode($m);
        
        $data = array(
            'student_id' => $m->student_id,
            'invoice_date' => $m->invoice_date,
            'total' => $m->total,
            'paid' => 0,
            'due_amount' => $m->total,
            'note' => $m->note,
            'create_by' => Auth::user()->id,
            'due_date' => $m->due_date,
            'reference' => $m->reference,
            'recurring' => $m->recurring,
            'recurring_day' => $m->recurring_day
        );
        if($m->recurring_day > 0)
        {
            $next = date('Y-m-d', strtotime($m->invoice_date . " + {$m->recurring_day} days"));
            $data['next_date'] = $next;
        }
        // get student
        $st = DB::table('students')->find($m->student_id);
        $data['branch_id'] = $st->branch_id;

        $i = DB::table('invoices')->insertGetId($data);
        if($i)
        {
            $items = json_encode($r->items);
            $items = json_decode($items);
            
            foreach($items as $item)
            {
                $data1 = array(
                    'item_id' => $item->item_id,
                    'invoice_id' => $i,
                    'quantity' => $item->quanity,
                    'unitprice' => $item->price,
                    'discount' => $item->discount,
                    'subtotal' => $item->total,
                    'branch_id' => $st->branch_id,
                    'create_by' => Auth::user()->id
                );
               
                $x = DB::table('invoice_details')->insert($data1);
               
            }
            return $i;
        }
        else{
            return 0;
        }
    }
    public function get_price($id)
    {
        $p = DB::table('items')->find($id);
        return json_encode($p);
    }
    public function save_payment(Request $r)
    {
        if(!Right::check('payment', 'i'))
        {
            return view('permissions.no');
        }
        $inv = DB::table('invoices')->find($r->invoice_id);
        $data = array(
            'invoice_id' => $r->invoice_id,
            'pay_date' => $r->pay_date,
            'amount' => $r->amount,
            'method' => $r->method,
            'branch_id' => $inv->branch_id,
            'student_id' => $inv->student_id,
            'create_by' => Auth::user()->id
        );
        $i = DB::table('invoice_payments')->insert($data);
        if($i)
        {
            DB::table('invoices')
                ->where('id', $r->invoice_id)
                ->increment('paid', $r->amount);
            DB::table('invoices')
                ->where('id', $r->invoice_id)
                ->decrement('due_amount', $r->amount);
            $r->session()->flash('success', 'The payment has been saved!');
            return redirect('payment/invoice/detail/'.$r->invoice_id);
        }
        else{
            $r->session()->flash('error', 'Fail to register your payment!');
            return redirect('payment/invoice/detail/'.$r->invoice_id);
        }
    }
    public function delete_payment($id)
    {
        if(!Right::check('invoice', 'd')){
            return view('permissions.no');
        }
        $p = DB::table('invoice_payments')->find($id);
        $i = DB::table('invoice_payments')
            ->where('id', $id)
            ->delete();
        if($i)
        {
            DB::table('invoices')
                ->where('id', $p->invoice_id)
                ->increment('due_amount', $p->amount);
            DB::table('invoices')
                ->where('id', $p->invoice_id)
                ->decrement('paid', $p->amount);
        }
        return redirect('payment/invoice/detail/'.$p->invoice_id)
            ->with('success', 'Payment has been removed!');
    }
    public function payment()
    {
        if(!Right::check('payment', 'l')){
            return view('permissions.no');
        }
        $data['payments'] = DB::table('invoice_payments')
            ->join('invoices', 'invoice_payments.invoice_id', 'invoices.id')
            ->join('students', 'invoice_payments.student_id', 'students.id')
            ->whereIn('invoice_payments.branch_id', Helper::arr_branch())
            ->orderBy('invoice_payments.id', 'desc')
            ->select('invoice_payments.*', 'students.kh_name', 'invoices.total')
            ->paginate(config('app.row'));
        return view('payment::invoices.payment', $data);
    }
    public function due()
    {
        if(!Right::check('invoice', 'l'))
        {
            return view('permissions.no');
        }
        $data['invoices'] = DB::table('invoices')
            ->join('students', 'invoices.student_id', 'students.id')
            ->whereIn('invoices.branch_id', Helper::arr_branch())
            ->where('invoices.due_amount', '>', 0)
            ->orderBy('invoices.due_date')
            ->select('invoices.*', 'students.kh_name', 'students.en_name')
            ->paginate(config('app.row'));
        return view('payment::invoices.due', $data);
    }
}
