<?php

namespace Modules\Student\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class ParentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('parent', 'l'))
        {
            return view('permissions.no');
        }
        $data['q'] = "";
        $data['pts'] = DB::table('parents')
            ->join('students', 'parents.student_id', 'students.id')
            ->where('parents.active', 1)
            ->whereIn('students.branch_id', Helper::arr_branch())
            ->orderBy('parents.id', 'desc')
            ->select('parents.*', 'students.en_name', 'students.kh_name')
            ->paginate(config('app.row'));
        return view('student::parents.index', $data);
    }
    public function search(Request $r)
    {
        if(!Right::check('parent', 'l'))
        {
            return view('permissions.no');
        }
        $q = $r->q;
        $data['q'] = $q;
        $data['pts'] = DB::table('parents')
            ->join('students', 'parents.student_id', 'students.id')
            ->where('parents.active', 1)
            ->where(function($query) use ($q){
                $query->orWhere('parents.name', 'like', "%{$q}%")
                    ->orWhere('parents.phone', 'like', "%{$q}%")
                    ->orWhere('parents.address', 'like', "%{$q}%");
            })
            ->orderBy('parents.id', 'desc')
            ->select('parents.*', 'students.kh_name', 'students.en_name')
            ->paginate(config('app.row'));
        return view('student::parents.index', $data);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('parent', 'i'))
        {
            return view('permissions.no');
        }
        $data['sts'] = DB::table('students')
            ->where('active', 1)
            ->get();
        return view('student::parents.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('parent', 'i'))
        {
            return view('permissions.no');
        }
        $data = array(
            'name' => $r->name,
            'gender' => $r->gender,
            'phone' => $r->phone,
            'address' => $r->address,
            'student_id' => $r->student
        );
        $i = DB::table('parents')->insert($data);
        if($i)
        {
            return redirect()->route('parent.create')
                ->with('success', 'Data has been saved!');
        }
        else
        {
            session()->flash('error', 'Fail to save data, please check again!');
            return redirect()->route('parent.create')
                ->withInput();
        }
    }

    

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('parent', 'u'))
        {
            return view('permissions.no');
        }
        $data['sts'] = DB::table('students')
            ->where('active', 1)
            ->get();
        $data['pt'] = DB::table('parents')->find($id);
        return view('student::parents.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('parent', 'u'))
        {
            return view('permissions.no');
        }
        $data = array(
            'name' => $r->name,
            'gender' => $r->gender,
            'phone' => $r->phone,
            'address' => $r->address,
            'student_id' => $r->student
        );
        $i = DB::table('parents')
            ->where('id', $id)
            ->update($data);
        if($i)
        {
            return redirect()->route('parent.index')
                ->with('success', 'Data has been saved!');
        }
        else
        {
            session()->flash('error', 'Fail to save data, please check again!');
            return redirect()->route('parent.edit', $id);
        }
    }
    public function delete($id)
    {
        DB::table('parents')
            ->where('id', $id)
            ->update(['active'=>0]);
        return redirect()->route('parent.index')
            ->with('success', 'Data has been removed!');
    }
}
