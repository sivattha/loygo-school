<?php

namespace Modules\Student\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class StudentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('student', 'l'))
        {
            return view('permissions.no');
        }
        $data['q'] = "";

        $data['students'] = DB::table('students')
            ->leftJoin('score', 'students.id', '=','student_id')
            ->selectRaw('students.*, sum(score.score) as total_score')
            ->where('active', '<>', '0')
            ->whereIn('branch_id', Helper::get_branch(Auth::user()->id))
            ->orderBy('id', 'desc')
            ->groupBy('students.id')
            ->paginate(config('app.row'));

        return view('student::students.index', $data);
    }
    public function search(Request $r)
    {
        if(!Right::check('student', 'l'))
        {
            return view('permissions.no');
        }
        $q = $r->q;
        $data['q'] = $q;
        $data['students'] = DB::table('students')
            ->where('active', 1)
            ->where(function($query) use ($q){
                $query->orWhere('kh_name', 'like', "%{$q}%")
                    ->orWhere('en_name', 'like', "%{$q}%")
                    ->orWhere('code', 'like', "%{$q}%")
                    ->orWhere('address', 'like', "%{$q}%")
                    ->orWhere('phone', 'like', "%{$q}%");
            })
            ->orderBy('id', 'desc')
            ->paginate(config('app.row'));
        return view('student::students.index', $data);
    }
    public function detail($id)
    {
        if(!Right::check('student', 'l'))
        {
            return view('permissions.no');
        }
        $data['s'] = DB::table('students')
            ->join('branches', 'students.branch_id', 'branches.id')
            ->where('students.id', $id)
            ->select('students.*', 'branches.name')
            ->first();
        $data['invoices'] = DB::table('invoices')
            ->where('student_id', $id)
            ->orderBy('id', 'desc')
            ->get();
        $data['parents'] = DB::table('parents')
            ->where('active', 1)
            ->where('student_id', $id)
            ->get();
        $data['enrollments'] = DB::table('enrollments')
            ->leftJoin('classes', 'enrollments.class_id', 'classes.id')
            ->leftJoin('rooms', 'enrollments.room_id', 'rooms.id')
            ->leftJoin('shifts', 'enrollments.shift_id', 'shifts.id')
            ->leftJoin('branches', 'enrollments.branch_id', 'branches.id')
            ->where('enrollments.student_id', $id)
            ->where('enrollments.active', 1)
            ->select('enrollments.*', 'classes.name as cname', 'rooms.name as rname', 
                'shifts.name as sname', 'branches.name as bname', 'classes.is_finish')
            ->get();
        return view('student::students.detail', $data);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('student', 'i'))
        {
            return view('permissions.no');
        }
        $data['branches'] = Helper::user_branch();
        return view('student::students.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('student', 'i'))
        {
            return view('permissions.no');
        }
        $data = array(
            'code' => $r->code,
            'kh_name' => $r->kh_name,
            'en_name' => $r->en_name,
            'gender' => $r->gender,
            'dob' => $r->dob,
            'phone' => $r->phone,
            'address' => $r->address,
            'branch_id' => $r->branch
        );
        if($r->photo) 
        { 
            $data['photo'] = $r->file('photo')->store('uploads/students', 'custom'); 
        }
        $i = DB::table('students')->insertGetId($data);
        if($i)
        {
            return redirect()->route('student.detail', $i);
        }
        else{
            $r->session()->flash('error', 'Fail to save data, please check again!');
            return redirect()->route('student.create')
                ->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('student', 'u'))
        {
            return view('permissions.no');
        }
        $data['s'] = DB::table('students')->find($id);
        $data['branches'] = Helper::user_branch();
        return view('student::students.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('student', 'u'))
        {
            return view('permissions.no');
        }
        $data = array(
            'code' => $r->code,
            'kh_name' => $r->kh_name,
            'en_name' => $r->en_name,
            'gender' => $r->gender,
            'dob' => $r->dob,
            'phone' => $r->phone,
            'address' => $r->address,
            'branch_id' => $r->branch
        );
        if($r->photo) 
        { 
            $data['photo'] = $r->file('photo')->store('uploads/students', 'custom'); 
        }
        $i = DB::table('students')
            ->where('id', $id)
            ->update($data);
        if($i)
        {
            return redirect()->route('student.edit', $id)
                ->with('success', 'Data has been saved!');
        }
        else{
            return redirect()->route('student.edit', $id)
                ->with('error', 'Fail to save changes, please check again!');
        }
    }
    public function delete($id)
    {
        DB::table('students')
            ->where('id', $id)
            ->update(['active'=>0]);
        return redirect()->route('student.index')
            ->with('success', 'Data has been removed!');
    }
}
