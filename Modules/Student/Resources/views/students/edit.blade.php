@extends('layouts.app')
@section('header')
    <strong>Edit</strong>
@endsection
@section('content')
<form action="{{route('student.update', $s->id)}}" method="POST" enctype="multipart/form-data">                       
    @method('PATCH')
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <a href="{{route('student.create')}}" class="btn btn-primary btn-oval btn-sm">
                <i class="fa fa-plus-circle"></i> Create
            </a>
            <a href="{{route('student.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <div class="row">
                <div class="col-md-7">
                   
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="code" class="col-sm-3">Code <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="code" name="code" 
                                value="{{$s->code}}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kh_name" class="col-sm-3 form-control-label">Khmer Name <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="kh_name" name="kh_name" 
                                value="{{$s->kh_name}}" required >
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="en_name" class="col-sm-3 form-control-label">English Name <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="en_name" name="en_name" 
                                value="{{$s->en_name}}" required >
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">Gender <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                            <select name="gender" class="form-control">
                                <option value="m" {{$s->gender=='m'?'selected':''}}>Male</option>
                                <option value="f" {{$s->gender=='f'?'selected':''}}>Female</option>
                            </select>
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">DOB</label>
                        <div class="col-sm-9">
                            <input type="date" class="form-control" name="dob" value="{{$s->dob}}">
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="phone" class="col-sm-3 form-control-label">Phone </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="phone" name="phone" 
                                value="{{$s->phone}}" >
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label for="address" class="col-sm-3 form-control-label">Address </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" id="address" name="address" 
                                value="{{$s->address}}" >
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label for="branch" class="col-sm-2">Branch <span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                            <select name="branch" id="branch" class="form-control" required>
                                @foreach($branches as $b)
                                    <option value="{{$b->id}}" {{$s->branch_id==$b->id?'selected':''}}>{{$b->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="photo" class="col-sm-2">Photo </label>
                        <div class="col-sm-10">
                            <input type="file" name="photo" class="form-control" id="photo" 
                                accept="image/*" onchange="preview(event)">
                            <div style="margin-top: 9px">
                                <img src="{{asset($s->photo)}}" alt="" id="img" width="150">
                            </div>
                        </div>
                    </div>
                </div>    
            </div>    
        </div>
    </div>

</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_student").addClass("active");
        });
        function preview(evt)
        {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(evt.target.files[0]);
        }
    </script>
@endsection