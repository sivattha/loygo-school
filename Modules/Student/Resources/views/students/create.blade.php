@extends('layouts.app')
@section('header')
    <strong>Create Student</strong>
@endsection
@section('content')
<form action="{{route('student.store')}}" method="POST" enctype="multipart/form-data">                       
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <button class="btn btn-sm btn-danger btn-oval" type="reset">
                <i class="fa fa-refresh"></i> Cancel
            </button>
            <a href="{{route('student.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <div class="row">
            <div class="col-md-7">
                
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="Code" class="col-sm-3 form-control-label">Code <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="code" name="code" placeholder="Code"
                            value="{{old('code')}}" required autofocus>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="kh_name" class="col-sm-3 form-control-label">Khmer Name <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="kh_name" name="kh_name" placeholder="Khmer Name"
                            value="{{old('kh_name')}}" required >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="en_name" class="col-sm-3 form-control-label">English Name <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="en_khmer" name="en_name" placeholder="English Name"
                            value="{{old('en_name')}}" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="gender" class="col-sm-3 form-control-label">Gender <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <select name="gender" id="gender" class="form-control">
                            <option value="#">--Select--</option>
                            <option value="m">Male</option>
                            <option value="f">Female</option>
                        </select>
                    </div>
                </div>
                    
                <div class="form-group row">
                    <label for="dob" class="col-sm-3 form-control-label">DOB</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" id="dob" name="dob" placeholder="Date Of Birth"
                            value="{{old('dob')}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-sm-3 form-control-label">Phone </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone"
                            value="{{old('phone')}}"  >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-3 form-control-label">Address </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address"
                            value="{{old('address')}}">
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
                <div class="form-group row">
                    <label for="branch" class="col-sm-2">Branch <span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                        <select name="branch" id="branch" class="form-control" required>
                            @foreach($branches as $b)
                                <option value="{{$b->id}}">{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="photo" class="col-sm-2">Photo</label>
                    <div class="col-sm-10">
                        <input type="file" name="photo" id="photo" class="form-control"
                            accept="image/*" onchange="preview(event)">
                        <div style="margin-top: 9px">
                            <img src="" alt="" id="img" width="150">
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_add_student").addClass("active");
        });
        function preview(evt)
        {
            var img = document.getElementById('img');
            img.src = URL.createObjectURL(evt.target.files[0]);
        }
    </script>
@endsection