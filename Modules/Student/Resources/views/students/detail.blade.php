@extends('layouts.app')
@section('header')
    <strong>Student Detail</strong>
@endsection
@section('content') 
<div class="card card-gray">
    <div class="toolbox">
        <a href="{{route('student.create')}}" class="btn btn-primary btn-oval btn-sm">
            <i class="fa fa-plus-circle"></i> Create
        </a>
        <a href="{{route('student.edit', $s->id)}}"class="btn btn-primary btn-oval btn-sm">
            <i class="fa fa-edit"></i> Edit
        </a>
        <a href="{{route('student.delete', $s->id)}}"class="btn btn-danger btn-oval btn-sm" onclick="return confirm('You want to delete?')">
            <i class="fa fa-trash"></i> Delete
        </a>
        <a href="{{route('student.index')}}" class="btn btn-warning btn-oval btn-sm"> 
            <i class="fa fa-reply"></i> Back
        </a>
    </div>
    <div class="card-block">
            <div class="row">
                <div class="col-sm-7">
                    <div class="form-group row">
                        <label for="code" class="col-sm-3">Code</label>
                        <div class="col-sm-9">
                            : {{$s->code}}
                        </div>
                    </div>
                
                    <div class="form-group row">
                        <label for="kh_name" class="col-sm-3">Khmer Name</label>
                        <div class="col-sm-9">
                            : <b>{{$s->kh_name}}</b>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="en_name" class="col-sm-3"> English Name</label>
                        <div class="col-sm-9">
                            : <b>{{$s->en_name}}</b>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="gender" class="col-sm-3">Gender</label>
                        <div class="col-sm-9">
                            : {{$s->gender=='m'?'Male':'Female'}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="dob" class="col-sm-3">Date Of Birth</label>
                        <div class="col-sm-9">
                            : {{$s->dob}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="phone" class="col-sm-3">Phone</label>
                        <div class="col-sm-9">
                            : {{$s->phone}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="address" class="col-sm-3">Address</label>
                        <div class="col-sm-9">
                            : {{$s->address}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="branch" class="col-sm-3">Branch</label>
                        <div class="col-sm-9">
                            : {{$s->name}}
                        </div>
                    </div>
                </div>
               
          
                <div class="col-sm-5">
                    <div class="form-group row">
                        <label class="col-sm-2">Stop</label>
                        <div class="col-sm-9">
                            : {{$s->is_stop==1?'Yes':'No'}}
                        </div>
                    </div>
                    <img src="{{asset($s->photo)}}" class="img-responsive " width="150" alt="">
                </div>
                
            </div>    
            <ul class="nav nav-tabs nav-tabs-bordered">
                <li class="nav-item">
                    <a href="#home" class="nav-link active show" data-target="#home" 
                        data-toggle="tab" aria-controls="home" role="tab" 
                        aria-selected="true">Invoices</a>
                </li>
                <li class="nav-item">
                    <a href="#profile" class="nav-link" data-target="#profile" 
                        aria-controls="profile" data-toggle="tab" role="tab" 
                        aria-selected="false">Enrollments</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link" data-target="#messages" 
                        aria-controls="messages" data-toggle="tab" 
                        role="tab">Parents</a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link" data-target="#settings" aria-controls="settings" 
                        data-toggle="tab" role="tab">Documents</a>
                </li>
            </ul>
            <div class="tab-content tabs-bordered">
                <div class="tab-pane fade in active show" id="home">
                    <p>
                        <a href="{{route('invoice.create')}}" class="btn btn-primary btn-sm btn-oval">
                            <i class="fa fa-plus-circle"></i> Add
                        </a>
                    </p>
                    <table class="table table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice No.</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Total Amount</th>
                                <th>Paid Amount</th>
                                <th>Due Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i=1)
                            @foreach($invoices as $inv)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>
                                        <a href="{{route('invoice.detail', $inv->id)}}" target="_blank">INV00{{$inv->id}}</a>
                                    </td>
                                    <td>{{$inv->invoice_date}}</td>
                                    <td>{{$inv->due_date}}</td>
                                    <td>$ {{$inv->total}}</td>
                                    <td>$ {{$inv->paid}}</td>
                                    <td>$ {{$inv->due_amount}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="profile">
                    <p>
                        <a href="#" class="btn btn-primary btn-sm btn-oval">
                            <i class="fa fa-plus-circle"></i> Add
                        </a>
                    </p>
                    <table class="table table-sm table-bordered">
                        <thead class="flip-header">
                            <tr>
                                <th>#</th>
                                <th>Enroll Date</th>
                                <th>Class</th>
                                <th>Room</th>
                                <th>Shift</th>
                                <th>Status</th>
                                <th>Branch</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i=1)
                            @foreach($enrollments as $en)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$en->enroll_date}}</td>
                                    <td>
                                        <a href="{{route('class.detail', $en->class_id)}}" 
                                            target="_blank">{{$en->cname}}
                                        </a>
                                    </td>
                                    <td>{{$en->rname}}</td>
                                    <td>{{$en->sname}}</td>
                                    <td>
                                        {{$en->is_finish==1?'Finished': 'Progress'}}
                                    </td>
                                    <td>{{$en->bname}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="messages">
                    <p>
                        <a href="#" class="btn btn-primary btn-sm btn-oval">
                            <i class="fa fa-plus-circle"></i> Add
                        </a>
                    </p>
                    <table class="table table-sm table-bordered">
                        <thead class="flip-header">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Phone</th>
                                <th>Address</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i=1)
                            @foreach($parents as $p)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$p->name}}</td>
                                    <td>
                                        {{$p->gender=='m'?'Male':'Female'}}
                                    </td>
                                    <td>{{$p->phone}}</td>
                                    <td>{{$p->address}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="settings">
                    <h4>Settings Tab</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                        ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
                        culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div> 
            
    </div>
</div>
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_student").addClass("active");
			
        });
    </script>
@endsection