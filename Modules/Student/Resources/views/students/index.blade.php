@extends('layouts.app')
@section('header')
    <strong><i class="fa fa-users"></i> Students</strong>
@endsection
@section('content')
                       
    <div class="card card-gray">
        <div class="toolbox">
            <div class="row">
                <div class="col-sm-3">
                    <a href="{{route('student.create')}}" class="btn btn-primary btn-oval btn-sm ">
                        <i class="fa fa-plus-circle"></i> Create
                    </a>
                </div>
                <div class="col-sm-8">
                    <form action="{{route('student.search')}}" method="GET">
                        Search:
                        <input type="text" name='q' value='{{$q}}'>
                        <button>Find</button>
                    </form>
                </div>
            </div>
            
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered">
                <thead class="flip-header">
                    <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Code</th>
                        <th>Khmer Name</th>
                        <th>English Name</th>
                        <th>Gender</th>
                        <th>DOB</th>
                        <th>Phone</th>
                        <th>Subject</th>
                        <th>Score</th>
                        
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($students as $s)

                        @php
                            $query_score = DB::table('score')->where('student_id', $s->id)->get();
                        @endphp

                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <img src="{{asset($s->photo)}}" alt="" width="36">
                            </td>
                            <td>{{$s->code}}</td>
                            <td>
                                <a href="{{route('student.detail', $s->id)}}">
                                    {{$s->kh_name}}
                                </a></td>
                                <td>
                                <a href="{{route('student.detail', $s->id)}}">
                                    {{$s->en_name}}
                                </a></td>
                            <td>{{$s->gender}}</td>
                            <td>{{$s->dob}}</td>
                            <td>{{$s->phone}}</td>
                            <td>
                                @foreach ($query_score as $key_score => $each_score)
                                    {{$each_score->subject}} = {{$each_score->score}} <BR>
                                @endforeach
                            </td>
                            <td>{{$s->total_score}}</td>
                           
                            <td> 
                                <a href="{{route('student.edit', $s->id)}}" title="Edit" 
                                    class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="{{route('student.delete', $s->id)}}" title="Delete" onclick="return confirm('Do you really want to delete?')" 
                                    class="text-danger"><i class="fa fa-trash"></i></a>                                    
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $students->appends(request()->except('q'))->links() }}
        </div>
    </div>
                           
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_student").addClass("active");
			
        })
    </script>
@endsection