@extends('layouts.app')
@section('header')
    <strong><i class="fa fa-users"></i> Parents</strong>
@endsection
@section('content')
                       
    <div class="card card-gray">
        <div class="toolbox">
            <div class="row">
                <div class="col-sm-3">
                    <a href="{{route('parent.create')}}" class="btn btn-primary btn-oval btn-sm ">
                        <i class="fa fa-plus-circle"></i> Create
                    </a>
                </div>
                <div class="col-sm-8">
                    <form action="{{route('parent.search')}}" method="GET">
                        Search:
                        <input type="text" name='q' value='{{$q}}'>
                        <button>Find</button>
                    </form>
                </div>
            </div>
            
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered">
                <thead class="flip-header">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Student</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($pts as $s)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$s->name}}</td>
                            <td>
                                {{$s->gender=='m'?'Male':'Female'}}
                            </td>
                            <td>{{$s->phone}}</td>
                            <td>{{$s->address}}</td>
                            <td>{{$s->kh_name}} - {{$s->en_name}}</td>
                            <td> 
                                <a href="{{route('parent.edit', $s->id)}}" title="Edit" 
                                    class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="{{route('parent.delete', $s->id)}}" title="Delete" onclick="return confirm('Do you really want to delete?')" 
                                    class="text-danger"><i class="fa fa-trash"></i></a>                                    
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $pts->appends(request()->except('q'))->links() }}
        </div>
    </div>
                           
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_parent").addClass("active");
			
        })
    </script>
@endsection