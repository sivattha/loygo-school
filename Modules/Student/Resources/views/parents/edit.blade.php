@extends('layouts.app')
@section('header')
    <strong>Edit Parent</strong>
@endsection
@section('content')
<form action="{{route('parent.update', $pt->id)}}" method="POST">                       
    @method('PATCH')
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <a href="{{route('parent.create')}}" class="btn btn-primary btn-oval btn-sm ">
                <i class="fa fa-plus-circle"></i> Create
            </a>
            <a href="{{route('parent.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <div class="row">
            <div class="col-md-7">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name"
                            value="{{$pt->name}}" required >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 form-control-label">Gender <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <select name="gender" id="gender" class="form-control">
                            <option value="m" {{$pt->gender=='m'?'selected':''}}>Male</option>
                            <option value="f" {{$pt->gender=='f'?'selected':''}}>Female</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="phone" class="col-sm-3 form-control-label">Phone </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="phone" name="phone" value="{{$pt->phone}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-3 form-control-label">Address </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="address" name="address" 
                            value="{{$pt->address}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="student" class="col-sm-3">Student <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <select name="student" id="student" class="form-control chosen-select" required>
                            <option value="">-- Select --</option>
                            @foreach($sts as $s)
                                <option value="{{$s->id}}" {{$pt->student_id==$s->id?'selected':''}}>
                                    ({{$s->code}}) {{$s->kh_name}}-{{$s->en_name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
               
            </div>
            </div>
        </div>
    </div>
</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_parent").addClass("active");
        });
        
    </script>
@endsection