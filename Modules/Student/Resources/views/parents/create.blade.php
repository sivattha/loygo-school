@extends('layouts.app')
@section('header')
    <strong>Create Parent</strong>
@endsection
@section('content')
<form action="{{route('parent.store')}}" method="POST" enctype="multipart/form-data">                       
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <button class="btn btn-sm btn-danger btn-oval" type="reset">
                <i class="fa fa-refresh"></i> Cancel
            </button>
            <a href="{{route('parent.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <div class="row">
            <div class="col-md-7">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                            value="{{old('name')}}" required >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-sm-3 form-control-label">Gender <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <select name="gender" id="gender" class="form-control">
                            <option value="#">--Select--</option>
                            <option value="m">Male</option>
                            <option value="f">Female</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="phone" class="col-sm-3 form-control-label">Phone </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone"
                            value="{{old('phone')}}"  >
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-3 form-control-label">Address </label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="address" name="address" placeholder="Address"
                            value="{{old('address')}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="student" class="col-sm-3">Student <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <select name="student" id="student" class="form-control chosen-select" required>
                            <option value="">-- Select --</option>
                            @foreach($sts as $s)
                                <option value="{{$s->id}}">({{$s->code}}) {{$s->kh_name}}-{{$s->en_name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-sm-5">
               
            </div>
            </div>
        </div>
    </div>
</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#student").addClass("active open");
			$("#student_collapse").addClass("collapse in");
            $("#menu_parent").addClass("active");
        });
        
    </script>
@endsection