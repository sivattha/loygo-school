<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('student')->group(function() {
    // student
    Route::get('/', 'StudentController@index');
    Route::resource('student', 'StudentController')
        ->except(['show','destroy']);
    Route::get('student/delete/{id}', 'StudentController@delete')
        ->name('student.delete');
    Route::get('student/detail/{id}', 'StudentController@detail')
        ->name('student.detail');
    Route::get('student/search', 'StudentController@search')
        ->name('student.search');
    // parent
    Route::resource('parent', 'ParentController')
        ->except(['show', 'destroy']);
    Route::get('parent/delete/{id}', 'ParentController@delete')
        ->name('parent.delete');
    Route::get('parent/search', 'ParentController@search')
        ->name('parent.search');
    
});
