@extends('layouts.app')
@section('header')
    <strong>Edit Role</strong>
@endsection
@section('content')
<form action="{{route('role.update', $role->id)}}" method="POST">
    @method('PATCH')
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
                <i class="fa fa-save "></i> Save</button>
            <a href="{{route('role.create')}}" class="btn btn-primary btn-oval btn-sm ">
                <i class="fa fa-plus-circle"></i> Create
            </a>
            <a href="{{route('role.index')}}" class="btn btn-warning btn-oval btn-sm">
                <i class="fa fa-reply"></i> Back</a>
        </div>
        
        <div class="card-block">
            <div class="col-md-7">
                @component('layouts.coms.alert')
                @endcomponent
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="Name" class="col-sm-3">ឈ្មោះ <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="name" class="form-control" id="Name" name="name" 
                            value="{{$role->name}}" required autofocus>
                    </div>
                </div>
                
            </div>
        </div>
    </div>  
</form> 
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_security").addClass("active open");
			$("#security_collapse").addClass("collapse in");
            $("#role_id").addClass("active");
			
        })
    </script>
@endsection

