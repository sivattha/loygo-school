@extends('layouts.app')
@section('header')
	<strong><i class="fa fa-user"></i> Create User</strong>
@endsection
@section('content')
<form action="{{route('user.store')}}" method="POST" enctype="multipart/form-data">
<div class="card card-gray">
	<div class="toolbox">
		<button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
			<i class="fa fa-save "></i> Save</button>
		<button class="btn btn-danger btn-sm btn-oval" type='reset'>
			<i class="fa fa-times"></i> Cancel
		</button>
		<a href="{{route('user.index')}}" class="btn btn-warning btn-oval btn-sm"> 
			<i class="fa fa-reply"></i> Back</a>
	</div>
	<div class="card-block">
		<div class="card card-block sameheight-item" >
			@component('layouts.coms.alert')
			@endcomponent
			<div class='row'>
				<div class="col-md-7">
					{{csrf_field()}}
					<div class="form-group row">
						<label for="Name" class="col-sm-4 form-control-label">Full Name <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="name" class="form-control" id="Name" name="name" placeholder="Name" value="{{old('name')}}" required autofocus>
						</div>
					</div>
					<div class="form-group row">
						<label for="Username" class="col-sm-4 form-control-label">Username <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="username" class="form-control" id="Username" name="username" placeholder="Username" value="{{old('username')}}" required >
						</div>
					</div>
					<div class="form-group row">
						<label for="Email" class="col-sm-4 form-control-label">Email <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="email" class="form-control" id="Email" name="email" placeholder="Email" value="{{old('email')}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label for="Role" class="col-sm-4 form-control-label">Role <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<select name="role_id" class="form-control" id="Role" required>
								<option value="">-- Select --</option>
								@foreach($roles as $role)
								<option value="{{$role->id}}">{{$role->name}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="Password" class="col-sm-4 form-control-label">Password <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="password" class="form-control" id="Password" name="password" placeholder="Password" value="{{old('password')}}" required>
						</div>
					</div>
					
				</div>
				<div class="col-sm-5">
					<div class="form-group row">
						<label for="language" class="col-sm-3 form-control-label">Language</label>
						<div class="col-sm-9">
							<select name="language" class="form-control" id="language">
								<option value="en">English</option>
								<option value="km">ភាសាខ្មែរ</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="photo" class="col-sm-3 form-control-label">Photo </label>
						<div class="col-sm-9">
							<input type="file" id="photo" name="photo" onchange="preview(event)"
								accept="image/x-png,image/gif,image/jpeg">
							<p style="margin-top: 9px;">
								<img src="" alt="" id='img' width="120">
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_security").addClass("active open");
			$("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
			
        });
		function preview(evt)
		{
			let img = document.getElementById('img');
			img.src = URL.createObjectURL(evt.target.files[0]);
		}
    </script>
@endsection
