@extends('layouts.app')
@section('header')
	<strong><i class="fa fa-user"></i> Edit User</strong>
@endsection
@section('content')
<form action="{{route('user.update', $user->id)}}" method="POST" enctype="multipart/form-data">
	@method('PATCH')
<div class="card card-gray">
	<div class="toolbox">
		<button type="submit" name="submit" class="btn btn-oval btn-primary btn-sm">
			<i class="fa fa-save "></i> Save</button>
		<a href="#" class="btn btn-danger btn-sm btn-oval">
			<i class="fa fa-times"></i> Cancel
		</a>
		<a href="{{route('user.create')}}" class="btn btn-primary btn-oval btn-sm">
			<i class="fa fa-plus-circle"></i> Create</a>
		<a href="{{route('user.index')}}" class="btn btn-warning btn-oval btn-sm"> 
			<i class="fa fa-reply"></i> Back</a>
	</div>

	<div class="card-block" >
		@component('layouts.coms.alert')
		@endcomponent
		<div class="row">
			<div class="col-md-7">
				{{csrf_field()}}
				<input type="hidden" name="id" value="{{$user->id}}">
				<div class="form-group row">
					<label for="Name" class="col-sm-4 form-control-label">Full Name <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="name" class="form-control" id="Name" name="name" placeholder="Name" value="{{$user->name}}" required autofocus>
					</div>
				</div>
				<div class="form-group row">
					<label for="Username" class="col-sm-4 form-control-label">Username <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="username" class="form-control" id="Username" name="username" placeholder="Username" value="{{$user->username}}" required >
					</div>
				</div>
				<div class="form-group row">
					<label for="Email" class="col-sm-4 form-control-label">Email <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<input type="email" class="form-control" id="Email" name="email" placeholder="Email" value="{{$user->email}}" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="Role" class="col-sm-4 form-control-label">Role <span class="text-danger">*</span></label>
					<div class="col-sm-8">
						<select name="role_id" class="form-control" id="Role" required>
							<option value="">-- Select --</option>
							@foreach($roles as $role)
							<option value="{{$role->id}}" {{$user->role_id==$role->id?'selected':''}}>{{$role->name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div class="form-group row">
					<label for="Password" class="col-sm-4 form-control-label">Password</label>
					<div class="col-sm-8">
						<input type="password" class="form-control" id="Password" name="password">
						<p class="text-small">Keep it blank to use the old password!</p>
					</div>
				</div>
			</div>
			<div class="col-sm-5">
				<div class="form-group row">
					<label for="language" class="col-sm-3 form-control-label">Language</label>
					<div class="col-sm-8">
						<select name="language" class="form-control" id="language">
							<option value="en" {{$user->language=='en'?'selected':''}}>English</option>
							<option value="km" {{$user->language=='km'?'selected':''}}>ភាសាខ្មែរ</option>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="photo" class="col-sm-3 form-control-label">Photo </label>
					<div class="col-sm-8">
						<input type="file" id="photo" name="photo" onchange="preview(event)"
							accept="image/x-png,image/gif,image/jpeg" >
						<p style='margin-top: 9px;'>
							<img src="{{asset($user->photo)}}" width="120" id='img'>
						</p>
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
</form>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_security").addClass("active open");
			$("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
			
        });
		function preview(evt)
		{
			let img = document.getElementById('img');
			img.src = URL.createObjectURL(evt.target.files[0]);
		}
    </script>
@endsection