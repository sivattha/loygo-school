@extends('layouts.app')
@section('header')
    <strong><i class="fa fa-user"></i> Users</strong>
@endsection
@section('content')
                        
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{route('user.create')}}"class="btn btn-primary btn-oval btn-sm">
                <i class="fa fa-plus-circle"></i> Create</a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>Role</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $pagex = @$_GET['page'];
                        if(!$pagex)
                            $pagex = 1;
                        $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{route('user.detail', $user->id)}}">
                                    <img src="{{asset($user->photo)}}" width="27">
                                </a>
                            </td>
                            <td>
                                <a href="{{route('user.detail', $user->id)}}">{{$user->name}}</a>
                            </td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->username}}</td>
                            <td>{{$user->role_name}}</td>
                            <td>
                                <a href="{{route('user.edit', $user->id)}}" title="Edit" class="text-success">
                                    <i class="fa fa-edit"></i></a>
                                <a href="{{route('user.delete', $user->id)}}" title="Delete" 
                                    onclick="return confirm('You want to delete?')" class="text-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$users->links()}}
            
        </div>
    </div>
                           
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_security").addClass("active open");
			$("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
			
        })
    </script>
@endsection