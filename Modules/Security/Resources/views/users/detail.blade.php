@extends('layouts.app')
@section('header')
	<strong><i class="fa fa-user"></i> User</strong>
@endsection
@section('content')

<div class="card card-gray">
	<div class="toolbox">
        <a href="{{route('user.create')}}" class="btn btn-primary btn-oval btn-sm">
            <i class="fa fa-plus-circle"></i> Create</a>
        <a href="{{route('user.edit', $user->id)}}" class="btn btn-success btn-oval btn-sm">
            <i class="fa fa-edit"></i> Edit</a>
        <a href="{{route('user.delete',$user->id)}}" class="btn btn-danger btn-oval btn-sm" 
            onclick="return confirm('You want to delete?')">
            <i class="fa fa-trash"></i> Delete</a>
		<a href="{{route('user.index')}}" class="btn btn-warning btn-oval btn-sm"> 
			<i class="fa fa-reply"></i> Back</a>
	</div>
	<div class="card-block">
		<div class="card card-block sameheight-item" >
			@component('layouts.coms.alert')
			@endcomponent
			<div class='row'>
				<div class="col-md-7">
					{{csrf_field()}}
					<div class="form-group row">
						<label for="Name" class="col-sm-4 form-control-label">Full Name</label>
						<div class="col-sm-8">
							: {{$user->name}}
						</div>
					</div>
					<div class="form-group row">
						<label for="Username" class="col-sm-4 form-control-label">Username</label>
						<div class="col-sm-8">
							: {{$user->username}}
						</div>
					</div>
					<div class="form-group row">
						<label for="Email" class="col-sm-4 form-control-label">Email</label>
						<div class="col-sm-8">
							: {{$user->email}}
						</div>
					</div>
					<div class="form-group row">
						<label for="Role" class="col-sm-4 form-control-label">Role</label>
						<div class="col-sm-8">
							: {{$user->rname}}
						</div>
					</div>
					<div class="form-group row">
						<label for="Role" class="col-sm-4 form-control-label">Language</label>
						<div class="col-sm-8">
							: {{$user->language=='en'?'English':'ភាសាខ្មែរ'}}
						</div>
					</div>
						
					<div class="form-group row">
						<label for="Password" class="col-sm-4 form-control-label">Password</label>
						<div class="col-sm-8">
							******
						</div>
					</div>
					
				</div>
				<div class="col-sm-4">
                    <img src="{{asset($user->photo)}}" alt="" id='img' width="120">
				</div>
			</div>
			
			<form action="{{route('user.branch.add')}}" method="GET">
				<input type="hidden" name='userid' value="{{$user->id}}">
				<div class="row">
					<div class="col-sm-3">
						<p class="text-success">
							<strong>User Branches</strong>
						</p>
					</div>
					<div class="col-sm-3">
						<select name="branch" id="branch" class="form-control form-control-sm" required>
							<option value=""> </option>
							@foreach($branches as $b)
								<option value="{{$b->id}}">{{$b->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col-sm-2">
						<button class="btn btn-primary btn-sm btn-oval"> Add </button>
					</div>
				</div>
				<p></p>
			</form>
			<div class="row">
				<div class="col-sm-7">
					<table class="table table-sm table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th>Name</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							@php($i=1)
							@foreach($ubranches as $b)
								<tr>
									<td>{{$i++}}</td>
									<td>{{$b->name}}</td>
									<td>
										<a href="{{url('security/branch/delete/'.$b->id .'?uid='.$user->id)}}" 
											title="Delete" class="text-danger" 
											onclick="return confirm('You want to delete?')">
											<i class="fa fa-trash"></i>
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
		
	</div>
</div>

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_security").addClass("active open");
			$("#security_collapse").addClass("collapse in");
            $("#menu_user").addClass("active");
			
        });
	
    </script>
@endsection
