@extends('layouts.app')
@section('header')
    <strong>Permission</strong>
@endsection
@section('content')
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{route('role.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        
        <div class="card-block">
            <p class="text-success">
                Set permission for {{$role->name}}
            </p>
            @csrf
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Function</th>
                        <th>View</th>
                        <th>Insert</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i=1)
                    @foreach($per_role as $per)
                        <tr role-id="{{$role_id}}" permission-id="{{$per->permission_id}}" id="{{$per->id==''?'0':$per->id}}">
                            <td>{{$i++}}</td>
                            <td>{{$per->alias}}</td>
                            <td>
                                <label>
                                    <input type='checkbox' value="{{$per->list?'1':'0'}}" 
                                        {{$per->list==1?'checked':''}} onchange="save(this)">
                                    
                                </label>

                            </td>
                            <td>
                                <label>
                                    <input type='checkbox' value="{{$per->insert?'1':'0'}}" 
                                    {{$per->insert==1?'checked':''}} onchange="save(this)">
                                    
                                </label>

                            </td>
                            <td>
                                <label>
                                    <input type='checkbox' value="{{$per->update?'1':'0'}}" {{$per->update==1?'checked':''}} onchange="save(this)">
                                    
                                </label>

                            </td>
                            <td>
                                <label>
                                    <input type='checkbox' value="{{$per->delete?'1':'0'}}" 
                                    {{$per->delete==1?'checked':''}} onchange="save(this)">
                                    
                                </label>

                            </td>

                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('js/role_permission.js')}}"></script>
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_security").addClass("active open");
			$("#security_collapse").addClass("collapse in");
            $("#role_id").addClass("active");
			
        })
    </script>
@endsection