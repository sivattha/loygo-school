<?php

namespace Modules\Security\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use Auth;
use DB;
class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth'); 
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('Role', 'l'))
        {
            return view('permissions.no');
        }
        $data['roles'] = DB::table('roles')
            ->where("active", 1)
            ->paginate(config('app.row'));
        return view('security::roles.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('Role', 'i')){
            return view('permissions.no');
        }
        return view('security::roles.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('Role', 'i')){
            return view('permissions.no');
        }
        $data = array(
            "name" => $r->name
        );
        $i = DB::table('roles')->insert($data);
        if($i)
        {
            return redirect()->route('role.index')
                ->with('success' , 'New role has been created.');
        }
        else 
        {
            $r->session()->flash('error' , 'Fail to create new role, please check again!');
            return redirect()->route('role.create')
                ->withInput();
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('Role', 'u')){
            return view('permissions.no');
        }
    	$data['role'] = DB::table("roles")->find($id);
        return view('security::roles.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('Role', 'u')){
            return view('permissions.no');
        }
        $data = array(
            "name" => $r->name
        );
        $i = DB::table("roles")->where("id", $id)->update($data);
        if ($i)
        {
            
            return redirect()->route('role.index')
                ->with('success', 'Data has been saved successfully!');
        }
        else{
            return redirect()->route('role.edit', $id)
                ->with("error", "Fail to save changes. You may not make any change!");
        }
    }

    public function delete($id)
    {
        if(!Right::check('Role', 'd')){
            return view('permissions.no');
        }
        $i = DB::table('roles')->where('id', $id)
            ->update(["active"=>0]);
        if($i)
        {
            return redirect()->route('role.index')
                ->with('success', 'Data has been removed!');
        }
        else
        {
            return redirect()->route('role.index')
                ->with('error', 'Fail to remove data, please try again!');
        }
    }
}
