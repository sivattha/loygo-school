<?php

namespace Modules\Security\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use Auth;
use DB;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('user', 'l')){
            return view('permissions.no');
        }

        $data['users'] = DB::table('users')
            ->join("roles", "users.role_id","=", "roles.id")
            ->select("users.*", "roles.name as role_name")
            ->paginate(config('app.row'));

        return view("security::users.index", $data);
    }
    public function detail($id)
    {
        if(!Right::check('user', 'l')){
            return view('permissions.no');
        }
        $data['user'] = DB::table('users')
            ->join('roles', 'users.role_id', 'roles.id')
            ->where('users.id', $id)
            ->select('users.*', 'roles.name as rname')
            ->first();
        // get user branches
        $ubranches = DB::table('user_branches')
            ->join('branches', 'user_branches.branch_id', 'branches.id')
            ->where('user_branches.user_id', $id)
            ->select('user_branches.*', 'branches.name')
            ->get();
        
        $barr = [];
        foreach($ubranches as $b)
        {
            array_push($barr, $b->branch_id);
        }
        $data['ubranches'] = $ubranches;
        $data['branches'] = DB::table('branches')
            ->whereNotIn('id', $barr)
            ->get();
        return view('security::users.detail', $data);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('user', 'i')){
            return view('permissions.no');
        }
        $data['roles'] = DB::table('roles')
            ->where('active', 1)
            ->get();
        return view('security::users.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('User', 'i')){
            return view('permissions.no');
        }

        $data = array(
            'name' => $r->name,
            'email' => $r->email,
            'username' => $r->username,
            'password' => bcrypt($r->password),
            'role_id' => $r->role_id,
            'language' => $r->language
        );
        if($r->photo)
        {
            $data['photo'] = $r->file('photo')->store('uploads/users', 'custom');
        }
		$i = DB::table('users')->insertGetId($data);
        if ($i)
        {
            return redirect()->route('user.detail', $i);
        }
        else
        {
            $r->session()->flash('error' , 'Can not create. Please check and try again');
            return redirect()->route('user.create')
                ->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('User', 'u'))
        {
            return view('permissions.no');
        }
        $data['roles'] = DB::table('roles')->get();
		$data['user'] = DB::table("users")->where("id", $id)->first();
        return view("security::users.edit", $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('User', 'u')){
            return view('permissions.no');
        }
        $data = array(
            'name' => $r->name,
            'email' => $r->email,
            'username' => $r->username,
            'role_id' => $r->role_id,
            'language' => $r->language
        );
        if($r->photo)
        {
            $data['photo'] = $r->file('photo')->store('uploads/users', 'custom');
        }
        if($r->password)
        {
            $data['password'] = bcrypt($r->password);
        }
        $i = DB::table('users')->where("id", $id)->update($data);
        if($i)
        {
            $r->session()->flash('success' , 'Data has been save successfully!');
            return redirect()->route('user.detail', $id);
        }
        else
        {
            $r->session()->flash('error' , 'Can not update. Please check and try again');
            return redirect()->route('user.edit', $id);
        }
    }
    public function delete($id)
    {
        if(!Right::check('User', 'd')){
            return view('permissions.no');
        }
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('user.index')
            ->with('success', 'Data has been removed!');
    }
    // add user branch
    public function add_branch(Request $r)
    {
        DB::table('user_branches')
            ->insert([
                'user_id' => $r->userid,
                'branch_id' => $r->branch
            ]);
        return redirect()->route('user.detail', $r->userid);
    }
    public function delete_branch($id, Request $r)
    {
        DB::table('user_branches')->where('id', $id)
            ->delete();
        return redirect()->route('user.detail', $r->uid);
    }
    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

}
