<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('security')->group(function() {
    // role route
    Route::get('/', 'RoleController@index');
    Route::resource('role', 'RoleController')
        ->except(['show', 'destroy']);
    Route::get('role/delete/{id}', 'RoleController@delete')
        ->name('role.delete');
    // permission
    Route::get('permission/{id}', 'PermissionController@index')
        ->name('permission.index');
    Route::post('permission/save', 'PermissionController@store');
    // user route
    Route::resource('user', 'UserController')
        ->except(['destory','show']);
    Route::get('user/delete/{id}', 'UserController@delete')
        ->name('user.delete');
    Route::get('user/detail/{id}', 'UserController@detail')
        ->name('user.detail');
    Route::get('user/logout', 'UserController@logout')
        ->name('user.logout');
    Route::get('branch/delete/{id}', 'UserController@delete_branch')
        ->name('branch.delete');
    Route::get('user/branch/add', 'UserController@add_branch')
        ->name('user.branch.add');
    
});
