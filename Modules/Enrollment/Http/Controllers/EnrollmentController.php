<?php

namespace Modules\Enrollment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class EnrollmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('enrollment', 'l'))
        {
            return view('permissions.no');
        }
        $data['enrollments'] = DB::table('enrollments')
            ->leftJoin('students', 'enrollments.student_id', 'students.id')
            ->leftJoin('classes', 'enrollments.class_id', 'classes.id')
            ->leftJoin('rooms', 'enrollments.room_id', 'rooms.id')
            ->leftJoin('shifts', 'enrollments.shift_id', 'shifts.id')
            ->where('enrollments.active', 1)
            ->whereIn('enrollments.branch_id', Helper::arr_branch())
            ->orderBy('enrollments.id', 'desc')
            ->select('enrollments.*', 'students.kh_name', 'students.en_name','classes.name as cname',
                'rooms.name as rname', 'shifts.name as sname', 'classes.code')
            ->paginate(config('app.row'));
        return view('enrollment::enrollments.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('enrollment', 'i'))
        {
            return view('permissions.no');
        }
        $data['classes'] = DB::table('classes')
            ->where('active', 1)
            ->where('is_finish', 0)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        $data['students'] = DB::table('students')
            ->where('active', 1)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        
        return view('enrollment::enrollments.create', $data);
    }

    public function save(Request $r)
    {
        if(!Right::check('enrollment', 'i'))
        {
            return view('permissions.no');
        }
        $class = DB::table('classes')->find($r->class);
        // check if student already enroll in that class
        $st = DB::table('enrollments')
            ->where('student_id', $r->student)
            ->where('class_id', $r->class)
            ->where('active', 1)
            ->where('branch_id', $class->branch_id)
            ->count('id');
        if($st>0)
        {
            return redirect()->route('enrollment.create')
                ->with('error', 'Student is already enrolled!');
        }
        $data = array(
            'student_id' => $r->student,
            'class_id' => $r->class,
            'room_id' => $class->room_id,
            'enroll_date' => $r->enroll_date,
            'shift_id' => $class->shift_id,
            'branch_id' => $class->branch_id,
            'create_by' => Auth::user()->id
        );
        $i = DB::table('enrollments')->insert($data);
        if($i)
        {
            return redirect()->route('enrollment.create')
                ->with('success', 'Enrollment has been saved!');
        }
        else
        {
            return redirect()->route('enrollment.create')
                ->with('error', 'Fail to save enrollment!');
        }
    }

    public function edit($id)
    {
        if(!Right::check('enrollment', 'u'))
        {
            return view('permissions.no');
        }
        $data['classes'] = DB::table('classes')
            ->where('active', 1)
            ->where('is_finish', 0)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        $data['students'] = DB::table('students')
            ->where('active', 1)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        $data['enrollment'] = DB::table('enrollments')->find($id);
        return view('enrollment::enrollments.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r)
    {
        if(!Right::check('enrollment', 'u'))
        {
            return view('permissions.no');
        }
        $class = DB::table('classes')->find($r->class);
        // check if the new enroll is already enrolled
        $cur = DB::table('enrollments')->find($r->id);
        $st = DB::table('enrollments')
            ->where('active', 1)
            ->where('student_id', $r->student)
            ->where('class_id', $r->class)
            ->where('class_id', '!=', $cur->class_id)
            ->where('branch_id', $class->branch_id)
            ->count('id');
        if($st>0)
        {
            return redirect()->route('enrollment.edit', $r->id)
                ->with('error', 'Student is already enrolled!');
        }
        $data = array(
            'student_id' => $r->student,
            'class_id' => $r->class,
            'room_id' => $class->room_id,
            'enroll_date' => $r->enroll_date,
            'shift_id' => $class->shift_id
        );
        $i = DB::table('enrollments')
            ->where('id', $r->id)
            ->update($data);
        if($i)
        {
            return redirect()->route('enrollment.index')
                ->with('success', 'Data has been saved!');
        }
        else{
            return redirect()->route('enrollment.edit', $r->id)
                ->with('error', 'Fail to save data, please check again!');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $i = DB::table('enrollments')
            ->where('id', $id)
            ->update(['active'=>0]);
        if($i)
        {
            return redirect()->route('enrollment.index')
                ->with('success', 'Data has been removed!');
        }
        else{
            return redirect()->route('enrollment.index')
                ->with('error', 'Fail to remove data!');
        }
    }
}
