<?php

namespace Modules\Enrollment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class ClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('class', 'l')){
            return view('permissions.no');
        }
        $data['classes'] = DB::table('classes')
            ->join('branches', 'classes.branch_id', 'branches.id')
            ->leftJoin('rooms', 'classes.room_id', 'rooms.id')
            ->leftJoin('shifts', 'classes.shift_id', 'shifts.id')
            ->where('classes.active', 1)
            ->where('classes.is_finish', 0)
            ->whereIn('classes.branch_id', Helper::arr_branch())
            ->orderBy('classes.id', 'desc')
            ->select('classes.*', 'branches.name as bname', 'rooms.name as rname',
                'shifts.name as sname')
            ->paginate(config('app.row'));
        return view('enrollment::classes.index', $data);
    }
    public function archive()
    {
        if(!Right::check('class', 'l')){
            return view('permissions.no');
        }
        $data['classes'] = DB::table('classes')
            ->join('branches', 'classes.branch_id', 'branches.id')
            ->leftJoin('rooms', 'classes.room_id', 'rooms.id')
            ->leftJoin('shifts', 'classes.shift_id', 'shifts.id')
            ->where('classes.active', 1)
            ->where('classes.is_finish', 1)
            ->whereIn('classes.branch_id', Helper::arr_branch())
            ->orderBy('classes.id', 'desc')
            ->select('classes.*', 'branches.name as bname', 'rooms.name as rname',
                'shifts.name as sname')
            ->paginate(config('app.row'));
        return view('enrollment::classes.archive', $data);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('class', 'i')){
            return view('permissions.no');
        }
        $data['rooms'] = DB::table('rooms')
            ->where('active', 1)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        $data['shifts'] = DB::table('shifts')
            ->where('active', 1)
            ->get();
        $data['branches'] = DB::table('branches')
            ->whereIn('id', Helper::arr_branch())
            ->get();
        return view('enrollment::classes.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('class', 'i')){
            return view('permissions.no');
        }
        $data = array(
            'code' => $r->code,
            'name' => $r->name,
            'room_id' => $r->room,
            'shift_id' => $r->shift,
            'branch_id' => $r->branch_id,
            'start_date' => $r->start_date,
            'end_date' => $r->end_date,
            'start_time' => $r->start_time,
            'end_time' => $r->end_time
        );
        $i = DB::table('classes')->insertGetId($data);
        if($i)
        {
            return redirect()->route('class.detail', $i);
        }
        else{
            session()->flash('error', 'Fail to save data, please check again!');
            return redirect()->route('class.create')
                ->withInput();
        }
    }

    public function detail($id)
    {
        if(!Right::check('class', 'l')){
            return view('permissions.no');
        }
        $data['class'] = DB::table('classes')
            ->join('branches', 'classes.branch_id', 'branches.id')
            ->leftJoin('rooms', 'classes.room_id', 'rooms.id')
            ->leftJoin('shifts', 'classes.shift_id', 'shifts.id')
            ->where('classes.active', 1)
            ->whereIn('classes.branch_id', Helper::arr_branch())
            ->orderBy('classes.id', 'desc')
            ->select('classes.*', 'branches.name as bname', 'rooms.name as rname',
                'shifts.name as sname')
            ->first();
        $enrolls = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.class_id', $id)
            ->where('enrollments.active', 1)
            ->select('enrollments.*', 'students.en_name', 'students.kh_name', 'students.gender', 
            'students.phone', 'students.code', 'students.id as sid')
            ->get();
        $arr = array();
        foreach($enrolls as $en)
        {
            array_push($arr, $en->sid);
        }
        $data['students'] = $enrolls;
        $data['sts'] = DB::table('students')
            ->where('active', 1)
            ->whereNotIn('id', $arr)
            ->get();
        return view('enrollment::classes.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('class', 'u')){
            return view('permissions.no');
        }
        $data['rooms'] = DB::table('rooms')
            ->where('active', 1)
            ->whereIn('branch_id', Helper::arr_branch())
            ->get();
        $data['shifts'] = DB::table('shifts')
            ->where('active', 1)
            ->get();
        $data['branches'] = DB::table('branches')
            ->whereIn('id', Helper::arr_branch())
            ->get();
        $data['class'] = DB::table('classes')->find($id);
        return view('enrollment::classes.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('class', 'u'))
        {
            return view('permissions.no');
        }
        $data = array(
            'code' => $r->code,
            'name' => $r->name,
            'room_id' => $r->room,
            'shift_id' => $r->shift,
            'branch_id' => $r->branch_id,
            'start_date' => $r->start_date,
            'end_date' => $r->end_date,
            'start_time' => $r->start_time,
            'end_time' => $r->end_time
        );
        $i = DB::table('classes')
            ->where('id', $id)
            ->update($data);
        if($i)
        {
            return redirect()->route('class.detail', $i)
                ->with('success', 'Data has been saved!');
        }
        else{
            session()->flash('error', 'Fail to save data, please check again!');
            return redirect()->route('class.edit', $id)
                ->withInput();
        }
    }

    public function delete($id)
    {
        if(!Right::check('class', 'd')){
            return view('permissions.no');
        }
        $i = DB::table('classes')
            ->where('id', $id)
            ->update(['active'=>0]);
        if($i)
        {
            DB::table('enrollments')
                ->where('class_id', $id)
                ->update(['active'=>0]);

            return redirect()->route('class.index')
                ->with('success', 'Data has been removed!');
        }
        else{
            return redirect()->route('class.index')
                ->with('error', 'Fail to remove data!');
        }
    }
    // enroll new student
    public function enroll(Request $r)
    {
        $class_id = $r->class_id;
        $class = DB::table('classes')->find($class_id);
        $data = array(
            'student_id' => $r->student,
            'class_id' => $r->class_id,
            'shift_id' => $class->shift_id,
            'room_id' => $class->room_id,
            'branch_id' => $class->branch_id,
            'enroll_date' => $r->enroll_date,
            'create_by' => Auth::user()->id
        );
        $i = DB::table('enrollments')->insert($data);
        return redirect()->route('class.detail', $r->class_id)
            ->with('success', 'New student has been enrolled!');
    }
    public function delete_enrollment(Request $r, $id)
    {
        DB::table('enrollments')
            ->where('id', $id)
            ->update(['active'=>0]);
        return redirect()->route('class.detail', $r->cid);
    }
    public function finish($id)
    {
        DB::table('classes')
            ->where('id', $id)
            ->update(['is_finish'=>1]);
        return redirect()->route('class.index')
            ->with('success', 'Data has been saved!');
    }
}
