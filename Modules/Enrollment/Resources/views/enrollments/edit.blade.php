@extends('layouts.app')
@section('header')
    <strong>Create Enrollment</strong>
@endsection
@section('content')
<form action="{{route('enrollment.update')}}" method="POST">                       
    <input type="hidden" name='id' value="{{$enrollment->id}}">
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <button class="btn btn-sm btn-danger btn-oval" type="reset">
                <i class="fa fa-refresh"></i> Cancel
            </button>
            <a href="{{route('enrollment.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <div class="row">
                <div class="col-md-7">
                
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="Code" class="col-sm-3 form-control-label">Student <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                           <select name="student" id="student" class="form-control" required>
                               <option value="">-- Select --</option>
                               @foreach ($students as $s)
                                    <option value="{{$s->id}}" {{$s->id==$enrollment->student_id?'selected':''}}>({{$s->code}}) - {{$s->kh_name}} {{$s->en_name}}</option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="class" class="col-sm-3 form-control-label">Class <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                           <select name="class" id="class" class="form-control" required>
                               <option value="">-- Select --</option>
                               @foreach($classes as $c)
                                    <option value="{{$c->id}}" {{$c->id==$enrollment->class_id?'selected':''}}>
                                        ({{$c->code}}) - {{$c->name}}
                                    </option>
                               @endforeach
                           </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="enroll_date" class="col-sm-3 form-control-label">Enroll Date <span class="text-danger">*</span></label>
                        <div class="col-sm-9">
                           <input type="date" class="form-control" id="enroll_date" name='enroll_date' 
                            value="{{$enrollment->enroll_date}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#enrollment").addClass("active open");
			$("#enrollment_collapse").addClass("collapse in");
            $("#menu_enrollment").addClass("active");
        });
      
    </script>
@endsection