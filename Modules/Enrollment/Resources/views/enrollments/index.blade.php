@extends('layouts.app')
@section('header')
    <strong>Enrollments</strong>
@endsection
@section('content')
<div class="card card-gray">
    <div class="toolbox">
        <a href="{{route('enrollment.create')}}" class="btn btn-primary btn-oval btn-sm">
            <i class="fa fa-plus-circle"></i> Create
        </a>
    </div>
	<div class="card-block">
        @component('layouts.coms.alert')
        @endcomponent
       <table class="table table-sm table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Student</th>
                <th>Class Code</th>
                <th>Class</th>
                <th>Room</th>
                <th>Shift</th>
                <th>Enroll Date</th>
                <th></th>
            </tr>
        </thead>
        <tbody>			
            <?php
                $pagex = @$_GET['page'];
                if(!$pagex)
                    $pagex = 1;
                $i = config('app.row') * ($pagex - 1) + 1;
            ?>
            @foreach($enrollments as $p)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        <a href="{{route('student.detail', $p->student_id)}}">
                            {{$p->kh_name}} - {{$p->en_name}}
                        </a>
                    </td>
                    <td>{{$p->code}}</td>
                    <td>{{$p->cname}}</td>
                    <td>{{$p->rname}}</td>
                    <td>{{$p->sname}}</td>
                    <td>{{$p->enroll_date}}</td>
                    <td>
                        <a href="{{route('enrollment.edit', $p->id)}}" title="Edit" 
                            class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                        <a href="{{route('enrollment.delete', $p->id)}}" title="Delete" onclick="return confirm('Do you really want to delete?')" 
                            class="text-danger"><i class="fa fa-trash"></i></a> 
                    </td>
                   
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$enrollments->links()}}
	</div>
</div>
@endsection
@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#enrollment").addClass("active open");
			$("#enrollment_collapse").addClass("collapse in");
            $("#menu_enrollment").addClass("active");
			
        })
    </script>
@endsection