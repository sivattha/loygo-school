@extends('layouts.app')
@section('header')
    <strong>Edit Class</strong>
@endsection
@section('content')
<form action="{{route('class.update', $class->id)}}" method="POST"> 
    @method('PATCH')                      
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <button class="btn btn-sm btn-danger btn-oval" type="reset">
                <i class="fa fa-refresh"></i> Cancel
            </button>
            <a href="{{route('class.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
						
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <div class="row">
            <div class="col-md-6">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="code" class="col-sm-4 form-control-label">Code <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="code" name="code" 
                            value="{{$class->code}}" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="Name" class="col-sm-4 form-control-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="Name" name="name"
                            value="{{$class->name}}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="start_date" class="col-sm-4 form-control-label">Start Date</label>
                    <div class="col-sm-8">
                        <input type="date" class="form-control" id="start_date" name="start_date"
                            value="{{$class->start_date}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="end_date" class="col-sm-4 form-control-label">End Date</label>
                    <div class="col-sm-8">
                        <input type="date" class="form-control" id="end_date" name="end_date"
                            value="{{$class->end_date}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="start_time" class="col-sm-4 form-control-label">Start Time</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="start_time" name="start_time"
                            value="{{$class->start_time}}">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="end_time" class="col-sm-4 form-control-label">End Time</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="end_time" name="end_time"
                            value="{{$class->end_time}}">
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group row">
                    <label for="room" class="col-sm-3 form-control-label">Room</label>
                    <div class="col-sm-8">
                        <select name="room" id="room" class="form-control">
                            <option value="0"></option>
                            @foreach($rooms as $b)
                                <option value="{{$b->id}}" {{$b->id==$class->room_id?'selected':''}}>{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="shift" class="col-sm-3 form-control-label">Shift</label>
                    <div class="col-sm-8">
                        <select name="shift" id="shift" class="form-control">
                            <option value="0"></option>
                            @foreach($shifts as $b)
                                <option value="{{$b->id}}" {{$b->id==$class->shift_id?'selected':''}}>{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="branch_id" class="col-sm-3 form-control-label">Branch <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <select name="branch_id" id="branch_id" class="form-control" required>
                            @foreach($branches as $b)
                                <option value="{{$b->id}}" {{$b->id==$class->branch_id?'selected':''}}>{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#enrollment").addClass("active open");
			$("#enrollment_collapse").addClass("collapse in");
            $("#menu_class").addClass("active");
			
        })
    </script>
@endsection