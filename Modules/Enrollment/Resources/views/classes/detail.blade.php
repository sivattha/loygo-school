@extends('layouts.app')
@section('header')
    <strong>Class Detail</strong>
@endsection
@section('content')
<div class="card card-gray">
    <div class="toolbox">
        <a href="{{route('class.create')}}" class="btn btn-primary btn-oval btn-sm ">
            <i class="fa fa-plus-circle"></i> Create
        </a>
        <a href="{{route('class.edit', $class->id)}}" class="btn btn-primary btn-oval btn-sm ">
            <i class="fa fa-edit"></i> Edit
        </a>
        <a href="{{route('class.index')}}" class="btn btn-warning btn-oval btn-sm ">
            <i class="fa fa-reply"></i> Back
        </a>
    </div>	
    <div class="card-block">
        @component('layouts.coms.alert')
        @endcomponent
        <div class="row">
        <div class="col-md-6">
            {{csrf_field()}}
            <div class="form-group row">
                <label class="col-sm-4">Code</label>
                <div class="col-sm-8">
                    : {{$class->code}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4">Name</label>
                <div class="col-sm-8">
                    : {{$class->name}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4">Start Date</label>
                <div class="col-sm-8">
                    : {{$class->start_date}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4">End Date</label>
                <div class="col-sm-8">
                    : {{$class->end_date}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4">Start Time</label>
                <div class="col-sm-8">
                    : {{$class->start_time}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-4">End Time</label>
                <div class="col-sm-8">
                    : {{$class->end_time}}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group row">
                <label class="col-sm-3">Room</label>
                <div class="col-sm-8">
                    : {{$class->rname}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3">Shift</label>
                <div class="col-sm-8">
                    : {{$class->sname}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3">Branch</label>
                <div class="col-sm-8">
                    : {{$class->bname}}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3">Status</label>
                <div class="col-sm-8">
                    : {{$class->is_finish?'Finished':'In Progress'}}
                </div>
            </div>
            @if($class->is_finish==0)
            <div class="form-group row">
                <a href="{{route('class.finish', $class->id)}}" class="btn btn-warning btn-sm btn-oval">
                    <i class="fa fa-star"></i> Finish This Class
                </a>
            </div>
            @endif
        </div>
        </div>
        @if($class->is_finish==0)
        <form action="{{route('class.enroll')}}" method="POST">
            @csrf
            <input type="hidden" name='class_id' value="{{$class->id}}">
            <div class="row">
            
                <div class="col-sm-2">
                    <p class="text-success"><strong>Student List</strong></p>
                </div>
                <div class="col-sm-9">
                    <table border="0">
                        <tr>
                            <td>Enroll Student</td>
                            <td style="width: 18px">:</td>
                            <td style="width:300px">
                                <select name="student" class="form-contro chosen-select" required>
                                    <option value=""></option>
                                    @foreach($sts as $st)
                                        <option value="{{$st->id}}">({{$st->code}}) {{$st->kh_name}} - {{$st->en_name}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="date" class="form-control" name='enroll_date' value="{{date('Y-m-d')}}">
                            </td>
                            <td><button class="btn btn-primary btn-oval">Enroll</button></td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <p></p>
                <table class="table table-sm table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Code</th>
                            <th>Name</th>
                            <th>Gender</th>
                            <th>Phone</th>
                            <th>Register Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php($i=1)
                        @foreach($students as $s)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>
                                    <a href="{{route('student.detail', $s->sid)}}">{{$s->code}}</a>
                                </td>
                                <td>
                                    <a href="{{route('student.detail', $s->sid)}}">
                                        {{$s->kh_name}} - {{$s->en_name}}
                                    </a>
                                </td>
                                
                                <td>{{$s->gender=='m'?'Male':'Female'}}</td>
                                <td>{{$s->phone}}</td>
                                <td>{{$s->enroll_date}}</td>
                                <td>
                                    <a href="{{url('enrollment/class/enrollment/delete/'.$s->id.'?cid='.$class->id)}}" title="Delete" class="text-danger" 
                                        onclick="return confirm('You want to delete?')" ><i class="fa fa-trash"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#enrollment").addClass("active open");
			$("#enrollment_collapse").addClass("collapse in");
            $("#menu_class").addClass("active");
			
        })
    </script>
@endsection