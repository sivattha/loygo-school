@extends('layouts.app')
@section('header')
    <strong>Classes</strong>
@endsection
@section('content')
                       
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{route('class.create')}}" class="btn btn-primary btn-oval btn-sm ">
                <i class="fa fa-plus-circle"></i> Create
            </a>
        </div>

        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Code</th>
                        <th>Name</th>
                        <th>Room</th>
                        <th>Shift</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Total#</th>
                        <th>Branch</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($classes as $class)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{route('class.detail', $class->id)}}">{{$class->code}}</a>
                            </td>
                            <td>
                                <a href="{{route('class.detail', $class->id)}}">{{$class->name}}</a>
                            </td>
                            <td>{{$class->rname}}</td>
                            <td>
                                {{$class->sname}} <br>
                                <small>{{$class->start_time}} - {{$class->end_time}}</small>

                            </td>
                            <td>
                                {{$class->start_date}} - {{$class->end_date}}
                            </td>
                            <td>
                                {{$class->is_finish?'Finished':'Progress'}}
                            </td>
                            <td>{{$class->bname}}</td>
                            <td>
                                
                                <a href="{{route('class.edit', $class->id)}}" title="Update" class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="{{route('class.delete',$class->id)}}" title="Delete" 
                                    onclick="return confirm('You want to delete?')" class="text-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$classes->links()}} 
        </div>
    </div>
                           
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#enrollment").addClass("active open");
			$("#enrollment_collapse").addClass("collapse in");
            $("#menu_archive").addClass("active");
			
        })
    </script>
@endsection