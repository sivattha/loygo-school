<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('enrollment')->group(function() {
    Route::get('/', 'EnrollmentController@index')->name('enrollment.index');
    Route::get('create', 'EnrollmentController@create')->name('enrollment.create');
    Route::post('save', 'EnrollmentController@save')->name('enrollment.save');
    Route::get('edit/{id}', 'EnrollmentController@edit')->name('enrollment.edit');
    Route::post('update', 'EnrollmentController@update')->name('enrollment.update');
    Route::get('delete/{id}', 'EnrollmentController@delete')->name('enrollment.delete');
    // class
    Route::resource('class', 'ClassController')
        ->except(['destroy', 'show']);
    Route::get('class/delete/{id}', 'ClassController@delete')
        ->name('class.delete');
    Route::get('class/detail/{id}', 'ClassController@detail')
        ->name('class.detail');
    Route::post('class/enroll', 'ClassController@enroll')
        ->name('class.enroll');
    Route::get('class/finish/{id}', 'ClassController@finish')
        ->name('class.finish');
    Route::get('class/archive', 'ClassController@archive')
        ->name('class.archive');
    Route::get('class/enrollment/delete/{id}', 'ClassController@delete_enrollment');
});
