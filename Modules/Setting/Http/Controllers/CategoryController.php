<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('Category', 'l'))
        {
            return view('permissions.no');
        }
        $data['categories'] = DB::table('categories')
            ->where("active",1)
            ->paginate(config('app.row'));
            
        return view('setting::categories.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('Category', 'i'))
        {
            return view('permissions.no');
        }
        return view('setting::categories.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('Category', 'i'))
        {
            return view('permissions.no');
        }
        $data = array(
            "name" => $r->name
        );
        $i = DB::table('categories')->insert($data);
        if($i){
            
            return redirect()->route('category.index')
                ->with('success', 'Data has been saved!');
        }else {
            $r->session()->flash('sms1' , 'Can not create. Please check and try again');
            return redirect()->route('category.create')
                ->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('Category', 'u'))
        {
            return view('permissions.no');
        }
    	$data['category'] = DB::table("categories")->find($id);
    	return view("setting::categories.edit",$data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('Category', 'u'))
        {
            return view('permissions.no');
        }
        $data = array(
            "name" => $r->name
        );
        $i = DB::table("categories")->where("id", $id)->update($data);
        if ($i)
        {
           
            return redirect()->route('category.index')
                ->with('success', 'Data has been saved!');
        }
        else{
            $r->session()->flash("error", "Fail to save change. You may not make any change!");
            return redirect()->route("category.edit", $r->id);
        }
    }

    public function delete($id)
    {
        DB::table('categories')->where('id', $id)->update(['active'=> 0]);
        
        return redirect()->route('category.index')
            ->with('success', 'Data has been removed!');
    }
}
