<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class RecurringTermController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('recurring', 'l'))
        {
            return view('permissions.no');
        }
        $data['terms'] = DB::table('recurring_terms')
            ->paginate(config('app.row'));
        return view('setting::recurrings.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('recurring', 'i'))
        {
            return view('permissions.no');
        }
        return view('setting::recurrings.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('recurring', 'i'))
        {
            return view('permissions.no');
        }
        $data = array(
            'name' => $r->name,
            'day' => $r->day
        );
        $i = DB::table('recurring_terms')->insert($data);
        if($i)
        {
            return redirect()->route('recurring.index')
                ->with('success', 'Data has been saved!');
        }
        else{
            session()->flash('error', "Fail to save data, please check again!");
            return redirect()->route('recurring.create')
                ->withInput();
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('recurring', 'u'))
        {
            return view('permissions.no');
        }
        $data['term'] = DB::table('recurring_terms')->find($id);
        return view('setting::recurrings.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('recurring', 'u'))
        {
            return view('permissions.no');
        }
        $data = array(
            'name' => $r->name,
            'day' => $r->day
        );
        $i = DB::table('recurring_terms')
            ->where('id', $id)
            ->update($data);
        if($i)
        {
            return redirect()->route('recurring.index')
                ->with('success', 'Data has been saved!');
        }
        else{
            session()->flash('error', "Fail to save data, please check again!");
            return redirect()->route('recurring.edit', $id);
        }
    }

    public function delete($id)
    {
        DB::table('recurring_terms')->where('id', $id)->delete();
        
        return redirect()->route('recurring.index')
            ->with('success', 'Data has been removed!');
    }
}
