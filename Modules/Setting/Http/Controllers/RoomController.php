<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class RoomController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    public function index()
    {
        if(!Right::check('room', 'l')){
            return view('permissions.no');
        }
        $user_branch = Helper::get_branch(Auth::user()->id);

        $data['rooms'] = DB::table('rooms')
            ->join('branches', 'rooms.branch_id', 'branches.id')
            ->where('rooms.active', 1)
            ->whereIn('rooms.branch_id', $user_branch)
            ->select('rooms.*', 'branches.name as branch_name')
            ->paginate(config('app.row'));
        return view('setting::rooms.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('room', 'i')){
            return view('permissions.no');
        }
        $data['branches'] = Helper::user_branch();
        return view('setting::rooms.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('room', 'i')){
            return view('permissions.no');
        }
        $data = $r->except('_token');

        $i = DB::table('rooms')->insert($data);
        if($i)
        {
           
            return redirect()->route('room.create')
                ->with('success', 'Data has been saved!');
        }
        else{
            session()->flash('error', 'Failed to save data. Please check again.');
            return redirect()->route('room.create')
                ->withInput();
        }
    }

    

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('room', 'u')){
            return view('permissions.no');
        }
        $data['room'] = DB::table('rooms')->find($id);
        $data['branch'] = Helper::user_branch();
        return view('setting::rooms.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('room', 'u')){
            return view('permissions.no');
        }
        $data = $r->except('_token', '_method');
        $i = DB::table('rooms')->where('id', $id)->update($data);
        if($i)
        {
            
            return redirect()->route('room.index')
                ->with('success', 'Data has been saved!');
        }
        else{
            $r->session()->flash('error', 'Failed to save data. Please check again.');
            return redirect()->route('room.edit', $id);
        }
    }

    public function delete($id)
    {
        if(!Right::check('room', 'u')){
            return view('permissions.no');
        }
        DB::table('rooms')->where('id', $id)->update(['active'=> 0]);
        return redirect()->route('room.index')
            ->with('success', 'Data has been removed!');
    }
}
