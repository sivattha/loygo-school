<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class ShiftController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('shift', 'l'))
        {
            return view('permissions.no');
        }
        $data['shifts'] = DB::table('shifts')
            ->where('active', 1)
            ->paginate(config('app.row'));
        return view('setting::shifts.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('shift', 'i'))
        {
            return view('permissions.no');
        }
        return view('setting::shifts.create');
    }
    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('shift', 'i'))
        {
            return view('permissions.no');
        }
        $data = $r->except('_token');
        $i = DB::table('shifts')->insert($data);
        if($i)
        {
            return redirect()->route('shift.index')
                ->with('success', 'Data has been saved!');
        }
        else{
            $r->session()->flash('error', 'Failed to save data. Please check again.');
            return redirect()->route('shift.create')->withInput();
        }
    }

    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('shift', 'u'))
        {
            return view('permissions.no');
        }
        $data['shift'] = DB::table('shifts')->find($id);
        
        return view('setting::shifts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('shift', 'u'))
        {
            return view('permissions.no');
        }
        $data = $r->except('_token', '_method');
        $i = DB::table('shifts')->where('id', $id)->update($data);
        if($i)
        {
            return redirect()->route('shift.index')
                ->with('success', 'Data has been saved!');
        }
        else
        {
            return redirect()->route('shift.edit', $id)
                ->with('error', 'Fail to save data, please check again!');
        }
    }

    public function delete($id)
    {
        if(!Right::check('shift', 'd'))
        {
            return view('permissions.no');
        }
        DB::table('shifts')->where('id', $id)->update(['active'=> 0]);
        return redirect()->route('shift.index')
            ->with('success', 'Data has been removed!');
    }
}
