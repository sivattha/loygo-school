<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Right;
use App\Http\Controllers\Helper;
use DB;
use Auth;
class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(!Right::check('item', 'l')){
            return view('permissions.no');
        }
        $data['items'] = DB::table('items')
            ->join('categories', 'items.category_id', 'categories.id')
            ->where('items.active', 1)
            ->orderBy('items.id', 'desc')
            ->select('items.*', 'categories.name as cname')
            ->paginate(config('app.row'));
        return view('setting::items.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        if(!Right::check('item', 'i')){
            return view('permissions.no');
        }
        $data['cats'] = DB::table('categories')
            ->where('active', 1)
            ->get();
        return view('setting::items.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $r)
    {
        if(!Right::check('item', 'i')){
            return view('permissions.no');
        }
        $data = array(
            'name' => $r->name,
            'price' => $r->price,
            'description' => $r->description,
            'category_id' => $r->category_id
        );
        $i = DB::table('items')->insert($data);
        if($i)
        {
            
            return redirect()->route('item.create')
                ->with('success', 'Data has been saved!');
        }
        else
        {
            $r->session()->flash('error', "Fail to save data, please check again!");
            return redirect()->route('item.create')->withInput();
        }
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        if(!Right::check('item', 'u')){
            return view('permissions.no');
        }
        $data['cats'] = DB::table('categories')
            ->where('active', 1)
            ->get();
        $data['item'] = DB::table('items')->find($id);
        return view('setting::items.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $r, $id)
    {
        if(!Right::check('item', 'u')){
            return view('permissions.no');
        }
        $data = $r->except('_token', '_method');
        $i = DB::table('items')->where('id', $id)->update($data);
        if($i)
        {
            return redirect()->route('item.index')
                ->with('success', 'Data has been saved!');
        }
        else
        {
            return redirect()->route('item.edit', $id)
                ->with('error', 'Failed to save data, please check again!');
        }
    }

    public function delete($id)
    {
        if(!Right::check('item', 'd')){
            return view('permissions.no');
        }
        DB::table('items')
            ->where('id', $id)
            ->update(['active'=>0]);
        return redirect()->route('item.index')
            ->with('success', 'Data has been removed!');
    }
}
