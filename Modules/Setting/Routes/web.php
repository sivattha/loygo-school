<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('setting')->group(function() {
    Route::get('/', 'SettingController@index');
   
    // room
    Route::resource('room', 'RoomController')
        ->except(['destroy', 'show']);
    Route::get('room/delete/{id}', 'RoomController@delete')
        ->name('room.delete');
    // item
    Route::resource('item', 'ItemController')
        ->except(['show', 'destroy']);
    Route::get('item/delete/{id}', 'ItemController@delete')
        ->name('item.delete');
    // category
    Route::resource('category', 'CategoryController')
        ->except(['show', 'destroy']);
    Route::get('category/delete/{id}', 'CategoryController@delete')
        ->name('category.delete');
    // recurring terms
    Route::resource('recurring', 'RecurringTermController')
        ->except(['show', 'destroy']);
    Route::get('recurring/delete/{id}', 'RecurringTermController@delete')
        ->name('recurring.delete');
    // shift
    Route::resource('shift', 'ShiftController')
        ->except(['show', 'destroy']);
    Route::get('shift/delete/{id}', 'ShiftController@delete')
        ->name('shift.delete');
});
