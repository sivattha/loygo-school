@extends('layouts.app')
@section('header')
    <strong>Edit Shift</strong>
@endsection
@section('content')
<form action="{{route('shift.update', $shift->id)}}" method="POST">  
    @method('PATCH')                    
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-oval btn-sm btn-primary"> 
                <i class="fa fa-save "></i> Save
            </button>
            <a href="{{route('shift.create')}}" class="btn btn-primary btn-oval btn-sm">
                <i class="fa fa-plus-circle"></i> Create
            </a>
            <a href="{{route('shift.index')}}" class="btn btn-warning btn-oval btn-sm"> 
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <hr>
        <div class="card-block">
            <div class="col-md-7">
                @component('layouts.coms.alert')
                @endcomponent
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="Name" class="col-sm-4 form-control-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="name" class="form-control" id="Name" name="name" placeholder="Name" value="{{$shift->name}}" required autofocus>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</form>  
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#menu_shift").addClass("active");
			
        })
    </script>
@endsection

