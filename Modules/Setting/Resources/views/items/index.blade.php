@extends('layouts.app')
@section('header')
    <strong><i class="fa fa-star"></i> Items</strong>
@endsection
@section('content')
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{route('item.create')}}"class="btn btn-primary btn-oval btn-sm">
                <i class="fa fa-plus-circle"></i> Create</a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $pagex = @$_GET['page'];
                        if(!$pagex)
                            $pagex = 1;
                        $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($items as $item)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>
                            <td>{{$item->name}}</td>
                            <td>$ {{$item->price}}</td>
                            <td>{{$item->cname}}</td>
                            <td>{{$item->description}}</td>
                            <td>
                                <a href="{{route('item.edit', $item->id)}}" title="កែប្រែ" class="text-success">
                                    <i class="fa fa-edit"></i></a>
                                <a href="{{route('item.delete', $item->id)}}" title="លុប" 
                                    onclick="return confirm('You want to delete?')" class="text-danger">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$items->links()}}
            
        </div>
    </div>
                           
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#menu_item").addClass("active");
			
        });
    </script>
@endsection