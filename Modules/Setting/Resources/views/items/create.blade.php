@extends('layouts.app')
@section('header')
    <strong><i class="fa fa-star"></i> Add Item</strong>
@endsection
@section('content')
<form action="{{route('item.store')}}" method="POST">                       
    <div class="card card-gray">
        <div class="toolbox">
        <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <button class="btn btn-sm btn-danger btn-oval" type="reset">
                <i class="fa fa-refresh"></i> Cancel
            </button>
            <a href="{{route('item.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
		<hr>						
        <div class="card-block">
            <div class="col-md-8">
                @component('layouts.coms.alert')
                @endcomponent
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="name" class="col-sm-3">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="name" name="name" 
                            value="{{old('name')}}" required autofocus>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="price" class="col-sm-3">Price($) <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <input type="number" step="0.01" class="form-control" id="price" name="price" 
                            value="{{old('price')}}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="category_id" class="col-sm-3">Category <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                        <select name="category_id" id="category_id" class="form-control chosen-select" required>
                            <option value="">--Select--</option>
                            @foreach($cats as $b)
                                <option value="{{$b->id}}">{{$b->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-sm-3">Description</label>
                    <div class="col-sm-9">
                        <textarea name="description" id="description" cols="30" rows="2" 
                            class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>                            
@endsection
@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#menu_item").addClass("active");
        });
    </script>
@endsection