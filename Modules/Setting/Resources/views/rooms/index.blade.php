@extends('layouts.app')
@section('header')
    <i class="fa fa-door"></i><strong>Room</strong>
@endsection
@section('content')
                       
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{route('room.create')}}" class="btn btn-primary btn-oval btn-sm ">
                <i class="fa fa-plus-circle"></i> Create
            </a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered ">
                <thead class="flip-header">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Branch</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $pagex = @$_GET['page'];
                        if(!$pagex)
                            $pagex = 1;
                        $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($rooms as $room)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>
                            <td>{{$room->name}}</td>
                            <td>{{$room->branch_name}}</td>
                            <td>
                                
                                <a href="{{route('room.edit', $room->id)}}" title="Update" class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="{{route('room.delete', $room->id)}}" 
                                    title="Delete" onclick="return confirm('You want to delete?')" class="text-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$rooms->links()}}
        </div>
    </div>
                           
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#room_id").addClass("active");
			
        })
    </script>
@endsection