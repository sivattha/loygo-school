@extends('layouts.app')
@section('header')
    <strong>Edit</strong>
@endsection
@section('content')
<form action="{{route('class.update', $class->id)}}" method="POST"> 
    @method('PATCH')                     
    <div class="card card-gray">
        <div class="toolbox">
            <button type="submit" class="btn btn-oval btn-sm btn-primary"> 
                <i class="fa fa-save "></i> Save
            </button>
            <a href="{{route('class.create')}}" class="btn btn-primary btn-oval btn-sm">
                <i class="fa fa-plus-circle"></i> Create
            </a>
            <a href="{{route('class.index')}}" class="btn btn-warning btn-oval btn-sm"> 
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
        <div class="card-block">
            <div class="col-md-7">
                @component('layouts.coms.alert')
                @endcomponent
                    {{csrf_field()}}
                    <div class="form-group row">
                        <label for="Name" class="col-sm-4 form-control-label">Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="name" class="form-control" id="Name" name="name" placeholder="Name" value="{{$class->name}}" required autofocus>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="branch_id" class="col-sm-4 form-control-label">Branch <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="branch_id" id="branch_id" class="form-control" required>
                                @foreach($branch as $b)
                                    <option value="{{$b->id}}" {{$b->id==$class->branch_id?'selected':''}}>{{$b->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    
                
            </div>
        </div>
    </div> 
</form>  
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#class_id").addClass("active");
			
        })
    </script>
@endsection

