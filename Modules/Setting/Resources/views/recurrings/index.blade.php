@extends('layouts.app')
@section('header')
    <strong>Recurring Terms</strong>
@endsection
@section('content')                    
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{route('recurring.create')}}"class="btn btn-primary btn-oval btn-sm">
                <i class="fa fa-plus-circle"></i> Create</a>
        </div>
        <div class="card-block">
            @component('layouts.coms.alert')
            @endcomponent
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Day</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($terms as $term)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$term->name}}</td>
                            <td>{{$term->day}}</td>
                            <td>
                                <a href="{{route('recurring.edit', $term->id)}}" 
                                    title="Edit" class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="{{route('recurring.delete', $term->id)}}" 
                                    title="Delete" onclick="return confirm('You want to delete?')" class="text-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$terms->links()}}
        </div>
    </div>                   
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#menu_recurring").addClass("active");
			
        })
    </script>
@endsection