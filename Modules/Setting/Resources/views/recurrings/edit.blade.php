@extends('layouts.app')
@section('header')
    <strong>Edit Recurring Terms</strong>
@endsection
@section('content')
<form action="{{route('recurring.update', $term->id)}}" method="POST">                  
    @method('PATCH')
    <div class="card card-gray">
    <div class="toolbox">
        <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save
            </button>
            <button class="btn btn-sm btn-danger btn-oval" type="reset">
                <i class="fa fa-refresh"></i> Cancel
            </button>
            <a href="{{route('recurring.index')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
		<hr>
        <div class="card-block">
            <div class="col-md-7">
                @component('layouts.coms.alert')
                @endcomponent
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="Name" class="col-sm-3 form-control-label">Name <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="Name" name="name" 
                         value="{{$term->name}}" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="day" class="col-sm-3 form-control-label">Day <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                        <input type="number" class="form-control" id="day" name="day" 
                        placeholder="Day" value="{{$term->day}}" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_setting").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#menu_recurring").addClass("active");
			
        })
    </script>
@endsection