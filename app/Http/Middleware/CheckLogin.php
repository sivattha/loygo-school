<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $seg = $request->segment(1);
        $check = Auth::check();
        // return response()->json($check);

        if($seg=='login')
        {
            return $next($request);
        }
        else
        {
            return $next($request);
        }
    }
}
