<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class EnrollmentController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }
    public function index()
    {
        $data['enrollments'] = DB::table('registrations')
            ->join('classes', 'classes.id', 'registrations.class_id')
            ->join('students', 'students.id', 'registrations.student_id')
            ->join('shifts', 'shifts.id', 'registrations.shift_id')
            ->join('rooms', 'rooms.id', 'registrations.room_id')
            ->select('classes.name as class', 'shifts.name as shift', 'rooms.name as room', 'students.en_name as en_name', 'students.kh_name as kh_name', 'registrations.*')
            ->where('registrations.active', 1)
            ->orderBy('id', 'desc')
            ->paginate(config('app.row'));
        return view('enrollments.index', $data);
    }
    public function create()
    {
        $data['students'] = DB::table('students')
            ->get();
        $data['classes'] = DB::table('classes')
            ->get();
        $data['rooms'] = DB::table('rooms')
            ->get();
        $data['shifts'] = DB::table('shifts')
            ->get();
        return view('enrollments.create', $data);
    }
    public function save(Request $r)
    {
        $data = $r->except('_token');
        $i = DB::table('registrations')->insert($data);
        if($i)
        {
            $r->session()->flash('success', 'Data has been saved!');
            return redirect('/enrollment/create');
        }
        else{
            $r->session()->flash('error', 'Failed to save data. Please check again.');
            return redirect('/enrollment/create')->withInput();
        }
    }
    public function edit($id)
    {
        $data['students'] = DB::table('students')
            ->get();
        $data['classes'] = DB::table('classes')
            ->get();
        $data['rooms'] = DB::table('rooms')
            ->get();
        $data['shifts'] = DB::table('shifts')
            ->get();
        $data['enrollment'] = DB::table('registrations')
            ->where('id', $id)
            ->first();
        return view('enrollments.edit', $data);
    }
    public function update(Request $r)
    {
        $data = $r->except('_token', 'id');
        $i = DB::table('registrations')->where('id', $r->id)->update($data);
        if($i)
        {
            $r->session()->flash('success', 'Data had been saved!');
            return redirect('/enrollment/edit/'.$r->id);
        }
        else{
            $r->session()->flash('error', 'Failed to save data. Please check again.');
            return redirect('/enrollment/edit/'.$r->id);
        }
    }
    public function delete(Request $r)
    {
        DB::table('registrations')->where('id', $r->id)->update(['active'=> 0]);
        $r->session()->flash('success', 'Data has been deleted!');
        return redirect('/enrollment');
    }
}
