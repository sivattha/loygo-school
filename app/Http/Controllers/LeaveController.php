<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class LeaveController extends Controller
{
    public function __construct(){
        $this->middleware('auth'); 
    }

    public function index()
    {
        if(!Right::check('leave', 'l')){
            return view('permissions.no');
        }
        $data['leaves'] = DB::table('leaves')
            ->join('employees', 'leaves.employee_id', 'employees.id')
            ->orderBy('leaves.id', 'desc')
            ->select('leaves.*', 'employees.first_name', 'employees.last_name')
            ->paginate(18);
        return view('leaves.index', $data);
    }
    public function create()
    {
        if(!Right::check('leave', 'i')){
            return view('permissions.no');
        }
        $data['employees'] = DB::table('employees')
            ->where('active', 1)
            ->get();
        return view('leaves.create', $data);
    }
    public function save(Request $r)
    {
        if(!Right::check('leave', 'i')){
            return view('permissions.no');
        }
        $data = $r->except('_token');
        $i = DB::table('leaves')->insert($data);
        if($i)
        {
            $r->session()->flash('success', 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ!');
            return redirect('leave/create');
        }
        else{
            $r->session()->flash('error', 'មិនអាចរក្សាទុកទីន្នន័យបានទេ សូមពិនិត្យឡើងវិញ!');
            return redirect('leave/create')->withInput();
        }
    }
    public function edit($id)
    {
        if(!Right::check('leave', 'u')){
            return view('permissions.no');
        }
        $data['leave'] = DB::table('leaves')
            ->join('employees', 'leaves.employee_id', 'employees.id')
            ->where('leaves.id', $id)
            ->select('leaves.*', 'employees.first_name', 'employees.last_name')
            ->first();
        return view('leaves.edit', $data);
    }
    public function update(Request $r)
    {
        if(!Right::check('leave', 'u')){
            return view('permissions.no');
        }
        $data = $r->except('_token', 'id');
        $i = DB::table('leaves')->where('id', $r->id)->update($data);
        if($i)
        {
            $r->session()->flash('success', 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ!');
            return redirect('leave/edit/'.$r->id);
        }
        else{
            $r->session()->flash('error', 'មិនអាចរក្សាទុកទីន្នន័យបានទេ សូមពិនិត្យឡើងវិញ!');
            return redirect('leave/edit/'.$r->id);
        }
    }
    public function detail($id)
    {
        if(!Right::check('leave', 'l')){
            return view('permissions.no');
        }
        $data['leave'] = DB::table('leaves')
            ->join('employees', 'leaves.employee_id', 'employees.id')
            ->where('leaves.id', $id)
            ->select('leaves.*', 'employees.first_name', 'employees.last_name')
            ->first();
        return view('leaves.detail', $data);
    }
    public function delete($id)
    {
        if(!Right::check('leave', 'd')){
            return view('permissions.no');
        }
        DB::table('leaves')->where('id', $id)->delete();
        return redirect('leave');
    }
}