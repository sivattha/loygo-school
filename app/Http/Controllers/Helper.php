<?php

namespace App\Http\Controllers;
use DB;
use Auth;
class Helper
{

    public static function save_image($data, $id)
    {
        if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]); // jpg, png, gif
        
            if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
                throw new \Exception('invalid image type');
            }
        
            $data = base64_decode($data);
        
            if ($data === false) {
                throw new \Exception('base64_decode failed');
            }
        } else {
            throw new \Exception('did not match data URI with image data');
        }
        $fname = "uploads/photos/" . md5($id) . ".jpg";
         file_put_contents($fname, $data);
        return $fname;
    }
    // get user branch id as array
    // this method will remove in the future refactor
    public static function get_branch($id)
    {
        $arr = [];
        $branches = DB::table('user_branches')
            ->where('user_id', $id)
            ->get();
        foreach($branches as $b)
        {
            array_push($arr, $b->branch_id);
        }
        return $arr;
    }
    public static function arr_branch()
    {
        $arr = [];
        $branches = DB::table('user_branches')
            ->where('user_id', Auth::user()->id)
            ->get();
        foreach($branches as $b)
        {
            array_push($arr, $b->branch_id);
        }
        return $arr;
    }
    // get user branch of the user as record set
    public static function user_branch()
    {
        $branches = DB::table('branches')
            ->whereIn('id', Self::get_branch(Auth::user()->id))
            ->get();
        return $branches;
    }
}