<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PayrollController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if(!Right::check('payroll', 'l')){
            return view('permissions.no');
        }
       $data['payrolls'] = DB::table('payrolls')
        ->orderBy('id', 'desc')
        ->paginate(12);
        return view('payrolls.index', $data);
    }
    public function create()
    {
        if(!Right::check('payroll', 'i')){
            return view('permissions.no');
        }
        return view('payrolls.create');
    }

    public function save(Request $r)
    {
        if(!Right::check('payroll', 'i')){
            return view('permissions.no');
        }
        $month = $r->month;
        $year = $r->year;
        $payroll_date = $year . '-'. $month . '-28';
        $employees = DB::table('employees')
            ->where('active', 1)
            ->get();
        $pp = DB::table('payrolls')
            ->whereYear('create_at', $year)
            ->whereMonth('create_at', $month)
            ->count('id');
        if($pp>0)
        {
            $r->session()->flash('error', 'បញ្ជីប្រាក់ខែសម្រាប់ខែនេះ មានរួចហើយ!');
            return redirect('payroll/create');
        }
        $data = array(
            'payroll_date' => $payroll_date,
            'total' => 0
        );
        $i = DB::table('payrolls')->insertGetId($data);
        $tt = 0;
        if($i)
        {
            foreach($employees as $emp)
            {
                // get leave of the employee
                // leave with no permission
                $leave1 = DB::table('leaves')
                    ->where('employee_id', $emp->id)
                    ->whereYear('create_at', $year)
                    ->whereMonth('create_at', $month)
                    ->where('status', 0)
                    ->sum('total');
                // leave with permission
                $leave2 = DB::table('leaves')
                    ->where('employee_id', $emp->id)
                    ->whereYear('create_at', $year)
                    ->whereMonth('create_at', $month)
                    ->where('status', 1)
                    ->sum('total');
                // cut money for leave with no permission
                $cutmoneny = $leave1 * $emp->cutmoney;
                // get loan of the employee
                $loan = DB::table('loans')
                    ->where('employee_id', $emp->id)
                    ->whereYear('create_at', $year)
                    ->whereMonth('create_at', $month)
                    ->sum('amount');
                // balance salary
                $total = $emp->salary - ($loan + $cutmoneny);
                $data1 = array(
                    'payroll_id' => $i,
                    'employee_id' => $emp->id,
                    'salary' => $emp->salary,
                    'loan' => $loan,
                    'leave1' => $leave1,
                    'leave2' => $leave2,
                    'cut' => $cutmoneny,
                    'total' => $total
                );
                $tt += $total;
                DB::table('payroll_details')->insert($data1);
            }

            DB::table('payrolls')->where('id', $i)->update(['total'=>$tt]);
            return redirect('payroll/detail/'.$i);
        }
        $r->session()->flash('error', 'មិនអាចបង្កើតតារាងប្រាក់ខែបានទេ!');
        return redirect('payroll/create');
    }
    public function detail($id)
    {
        if(!Right::check('payroll', 'l')){
            return view('permissions.no');
        }
        $data['payroll'] = DB::table('payrolls')
            ->where('id', $id)
            ->first();
        $data['details'] = DB::table('payroll_details')
            ->join('employees', 'payroll_details.employee_id', 'employees.id')
            ->where('payroll_details.payroll_id', $id)
            ->select('payroll_details.*', 'employees.first_name', 'employees.last_name', 'employees.salary')
            ->get();
        return view('payrolls.detail', $data);
    }
    public function delete($id)
    {
        if(!Right::check('payroll', 'd')){
            return view('permissions.no');
        }
        DB::table('payrolls')->where('id', $id)->delete();
        DB::table('payroll_details')->where('payroll_id', $id)->delete();
        return redirect('payroll');
    }
}
