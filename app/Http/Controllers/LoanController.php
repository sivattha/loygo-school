<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class LoanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        if(!Right::check('loan', 'l')){
            return view('permissions.no');
        }
        $data['loans'] = DB::table('loans')
            ->join('employees', 'loans.employee_id', 'employees.id')
            ->orderBy('loans.id', 'desc')
            ->select('loans.*', 'employees.first_name', 'employees.last_name')
            ->paginate(18);
        return view('loans.index', $data);
    }
    public function create()
    {
        if(!Right::check('loan', 'i')){
            return view('permissions.no');
        }
        $data['employees'] = DB::table('employees')
            ->where('active', 1)
            ->get();
        return view('loans.create', $data);
    }
    public function save(Request $r)
    {
        if(!Right::check('loan', 'i')){
            return view('permissions.no');
        }
        $data = $r->except('_token');
        $i = DB::table('loans')->insert($data);
        if($i)
        {
            $r->session()->flash('success', 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ!');
            return redirect('loan/create');
        }
        else{
            $r->session()->flash('error', 'មិនអាចរក្សាទុកទីន្នន័យបានទេ សូមពិនិត្យឡើងវិញ!');
            return redirect('loan/create')->withInput();
        }
    }
    public function edit($id)
    {
        if(!Right::check('loan', 'u')){
            return view('permissions.no');
        }
        $data['loan'] = DB::table('loans')
            ->join('employees', 'loans.employee_id', 'employees.id')
            ->where('loans.id', $id)
            ->select('loans.*', 'employees.first_name', 'employees.last_name')
            ->first();
        return view('loans.edit', $data);
    }
    public function update(Request $r)
    {
        if(!Right::check('loan', 'u')){
            return view('permissions.no');
        }
        $data = $r->except('_token', 'id');
        $i = DB::table('loans')->where('id', $r->id)->update($data);
        if($i)
        {
            $r->session()->flash('success', 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ!');
            return redirect('loan/edit/'.$r->id);
        }
        else{
            $r->session()->flash('error', 'មិនអាចរក្សាទុកទីន្នន័យបានទេ សូមពិនិត្យឡើងវិញ!');
            return redirect('loan/edit/'.$r->id);
        }
    }
    public function delete($id)
    {
        if(!Right::check('loan', 'd')){
            return view('permissions.no');
        }
        DB::table('loans')->where('id', $id)->delete();
        return redirect('loan');
    }
}
