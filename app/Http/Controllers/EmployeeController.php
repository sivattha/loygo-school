<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class EmployeeController extends Controller
{
    public function __construct(){
        $this->middleware('auth'); 
    }

   public function index(){
       if(!Right::check('Employee', 'l')){
           return view('permissions.no');
       }
       $data['employees'] = DB::table('employees')
            ->where('active', 1)
            ->orderBy('id', 'desc')
            ->paginate(18);
        return view('employees.index', $data);
    }
    public function create()
    {
        if(!Right::check('Employee', 'i')){
            return view('permissions.no');
        }
        return view('employees.create');
    }
    public function save(Request $r)
    {
        if(!Right::check('Employee', 'i')){
            return view('permissions.no');
        }
        $validate = $r->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required'
        ]);
        $data = $r->except('_token');
        $i = DB::table('employees')->insertGetId($data);
        if($i)
        {
            return redirect('employee/detail/'.$i);
        }
        else{
            $r->session()->flash('error', 'មិនអាចរក្សាទិន្នន័យបានទេ សូមពិនិត្យម្តងទៀត!');
            return redirect('employee/create')->withInput();
        }
    }
    public function edit($id)
    {
        if(!Right::check('Employee', 'u')){
            return view('permissions.no');
        }
        $data['emp'] = DB::table('employees')
            ->where('id', $id)
            ->first();
        return view('employees.edit', $data);
    }
    public function update(Request $r)
    {
        if(!Right::check('Employee', 'u')){
            return view('permissions.no');
        }
        $validate = $r->validate([
            'first_name' => 'required|min:2|max:30',
            'last_name' => 'required|min:2|max:30',
            'gender' => 'required'
        ]);
        $data = $r->except('_token', 'id');
        $i = DB::table('employees')
            ->where('id', $r->id)
            ->update($data);
        if($i)
        {
            $r->session()->flash('success', 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ!');
        }
        else{
            $r->session()->flash('error', 'មិនអាចរក្សាទិន្នន័យបានទេ សូមពិនិត្យម្តងទៀត!');
        }
        return redirect('employee/edit/'.$r->id);
    }
    public function delete($id)
    {
        if(!Right::check('Employee', 'd')){
            return view('permissions.no');
        }
        DB::table('employees')
            ->where('id', $id)
            ->update(['active'=>0]);
        return redirect('employee');
    }
    public function detail($id)
    {
        if(!Right::check('Employee', 'l')){
            return view('permissions.no');
        }
        $data['emp'] = DB::table('employees')
            ->where('id', $id)
            ->first();
        $data['docs'] = DB::table('employee_documents')
            ->where('employee_id', $id)
            ->get();
        return view('employees.detail', $data);
    }
    public function save_document(Request $r)
    {
        $validate = $r->validate([
            'title' => 'required|min:2',
            'file_name' => 'required'
        ]);
        $data = array(
            'title' => $r->title,
            'employee_id' => $r->id,
            'description' => $r->description
        );
        if($r->file_name)
        {
            $data['file_name'] = $r->file('file_name')->store('uploads/employees/documents', 'custom');
        }
        $i = DB::table('employee_documents')
            ->insert($data);
        $r->session()->flash('success', 'ទិន្នន័យត្រូវបានរក្សាទុកដោយជោគជ័យ!');        
        return redirect('employee/detail/'.$r->id);
    }
    public function delete_document(Request $r)
    {
        $emp_id = $r->empid;
        $doc_id = $r->id;
        DB::table('employee_documents')->where('id', $doc_id)->delete();
        $r->session()->flash('success', 'ទិន្នន័យត្រូវបានលុបដោយជោគជ័យ!');
        return redirect('employee/detail/'.$emp_id);
    }
}
