<?php
namespace App\Http\Controllers;
use Auth;
use DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Event;
use Calendar;
class DashboardController extends Controller
{
    public function __construct(){
         $this->middleware('auth'); 
         $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    public function index()
    {
        $data['today'] = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.active', 1)
            ->where('enrollments.enroll_date', date('Y-m-d'))
            ->count('students.id');
        // this week
        $now = Carbon::now();
        $start = $now->startOfWeek()->format('Y-m-d');
        $end = $now->endOfWeek()->format('Y-m-d');
        $data['week'] = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.active', 1)
            ->where('enrollments.enroll_date', '>=', $start)
            ->where('enrollments.enroll_date', '<=', $end)
            ->count('students.id');

            // this month
        $m = date('m');
        $data['month'] = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.active', 1)
            ->whereMonth('enrollments.enroll_date', $m)
            ->count('students.id');
        // this year
        $y = date('Y');
        $data['year'] = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.active', 1)
            ->whereYear('enrollments.enroll_date', $y)
            ->count('students.id');
        // all
        $data['all'] = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.active', 1)
            ->count('students.id');
        // girl
        $data['f'] = DB::table('enrollments')
            ->join('students', 'enrollments.student_id', 'students.id')
            ->where('enrollments.active', 1)
            ->where('students.gender', 'f')
            ->count('students.id');
        // due invoice
        $data['invoices'] = DB::table('invoices')
            ->join('students', 'invoices.student_id', 'students.id')
            ->whereIn('invoices.branch_id', Helper::arr_branch())
            ->where('invoices.due_amount', '>', 0)
            ->orderBy('invoices.due_date')
            ->select('invoices.*', 'students.kh_name', 'students.en_name')
            ->paginate(5);
        
        return view('dashboard', $data);
    }
}
