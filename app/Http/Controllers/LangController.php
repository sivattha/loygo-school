<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Session;
use DB;
use Auth;
use Illuminate\Support\Facades\Route;

class LangController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    // index
    public function index($id)
    {
        DB::table('users')->where('id', Auth::user()->id)->update(['language'=>$id]);
        Session::put("lang", $id);
        return 1;
    }
}
