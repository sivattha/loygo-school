<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            app()->setLocale(Auth::user()->language);
            return $next($request);
        });
    }
    public function index()
    {
        // if(!Right::check('Report', 'l')){
        //     return view('permissions.no');
        // }
        $data['start_date'] = date('Y-m-d');
        $data['end_date'] = date('Y-m-d');
        $data['enrollments'] = [];
        return view('reports.index', $data);
    }
    public function search(Request $r)
    {
        // if(!Right::check('Report', 'l')){
        //     return view('permissions.no');
        // }
        $data['start_date'] = $r->start_date;
        $data['end_date'] = $r->end_date;
        $data['enrollments'] = DB::table('registrations')
            ->join('classes', 'classes.id', 'registrations.class_id')
            ->join('students', 'students.id', 'registrations.student_id')
            ->join('shifts', 'shifts.id', 'registrations.shift_id')
            ->join('rooms', 'rooms.id', 'registrations.room_id')
            ->select('classes.name as class', 'shifts.name as shift', 'rooms.name as room', 'students.en_name as en_name', 'students.kh_name as kh_name', 'registrations.*')
            ->where('registrations.active', 1)
            ->where('registrations.start_date', '>=', $r->start_date)
            ->where('registrations.start_date', '<=', $r->end_date)
            ->orderBy('id', 'desc')
            ->get();

        return view('reports.index', $data);
    }

    public function room(Request $r) {
        $data['start_date'] = date('Y-m-d');
        $data['enrollments'] = [];
        $data['rooms'] = DB::table('rooms')
            ->get();
        return view('reports.room', $data);
    }

    public function room_search(Request $r)
    {
        // if(!Right::check('Report', 'l')){
        //     return view('permissions.no');
        // }
        $data['rooms'] = DB::table('rooms')
            ->get();
        $data['start_date'] = $r->start_date;
        $data['room_id'] = $r->room_id;
        $data['enrollments'] = DB::table('registrations')
            ->join('classes', 'classes.id', 'registrations.class_id')
            ->join('students', 'students.id', 'registrations.student_id')
            ->join('shifts', 'shifts.id', 'registrations.shift_id')
            ->join('rooms', 'rooms.id', 'registrations.room_id')
            ->select('classes.name as class', 'shifts.name as shift', 'rooms.name as room', 'students.en_name as en_name', 'students.kh_name as kh_name', 'registrations.*')
            ->where('registrations.active', 1)
            ->where('registrations.start_date', '>=', $r->start_date)
            ->where('registrations.room_id', $r->room_id)
            ->orderBy('id', 'desc')
            ->get();

        return view('reports.room', $data);
    }
    public function class(Request $r) {
        $data['start_date'] = date('Y-m-d');
        $data['enrollments'] = [];
        $data['classes'] = DB::table('classes')
            ->get();
        return view('reports.class', $data);
    }

    public function class_search(Request $r)
    {
        // if(!Right::check('Report', 'l')){
        //     return view('permissions.no');
        // }
        $data['classes'] = DB::table('classes')
            ->get();
        $data['start_date'] = $r->start_date;
        $data['class_id'] = $r->class_id;
        $data['enrollments'] = DB::table('registrations')
            ->join('classes', 'classes.id', 'registrations.class_id')
            ->join('students', 'students.id', 'registrations.student_id')
            ->join('shifts', 'shifts.id', 'registrations.shift_id')
            ->join('rooms', 'rooms.id', 'registrations.room_id')
            ->select('classes.name as class', 'shifts.name as shift', 'rooms.name as room', 'students.en_name as en_name', 'students.kh_name as kh_name', 'registrations.*')
            ->where('registrations.active', 1)
            ->where('registrations.start_date', '>=', $r->start_date)
            ->where('registrations.class_id', $r->class_id)
            ->orderBy('id', 'desc')
            ->get();

        return view('reports.class', $data);
    }

    public function shift(Request $r) {
        $data['start_date'] = date('Y-m-d');
        $data['enrollments'] = [];
        $data['shifts'] = DB::table('shifts')
            ->get();
        return view('reports.shift', $data);
    }

    public function shift_search(Request $r)
    {
        // if(!Right::check('Report', 'l')){
        //     return view('permissions.no');
        // }
        $data['shifts'] = DB::table('shifts')
            ->get();
        $data['start_date'] = $r->start_date;
        $data['shift_id'] = $r->shift_id;
        $data['enrollments'] = DB::table('registrations')
            ->join('classes', 'classes.id', 'registrations.class_id')
            ->join('students', 'students.id', 'registrations.student_id')
            ->join('shifts', 'shifts.id', 'registrations.shift_id')
            ->join('rooms', 'rooms.id', 'registrations.room_id')
            ->select('classes.name as class', 'shifts.name as shift', 'rooms.name as room', 'students.en_name as en_name', 'students.kh_name as kh_name', 'registrations.*')
            ->where('registrations.active', 1)
            ->where('registrations.start_date', '>=', $r->start_date)
            ->where('registrations.shift_id', $r->shift_id)
            ->orderBy('id', 'desc')
            ->get();

        return view('reports.shift', $data);
    }
}
