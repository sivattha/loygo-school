<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';
    protected $guarded = ['id'];

    public function score()
    {
        return $this->hasMany('App\Score', 'student_id');
    }

}
