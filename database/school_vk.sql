-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 30, 2019 at 03:35 PM
-- Server version: 5.7.27-0ubuntu0.18.04.1
-- PHP Version: 7.2.26-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `company_id` int(11) NOT NULL,
  `address` varchar(200) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `manager_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `name`, `company_id`, `address`, `email`, `phone`, `created_at`, `updated_at`, `manager_id`) VALUES
(1, 'Vdoo Training', 1, 'Phnom Penh, Cambodia', 'info@vdootraining.com', '086397627', '2019-02-05 02:13:32', '2019-12-15 08:05:14', NULL),
(5, 'Vdoo Kep', 1, 'Kep, Cambodia', 'vdookep@vdootraining.com', '017837754', '2019-12-15 08:04:49', '2019-12-15 08:05:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `active`, `create_at`) VALUES
(1, 'School Fee', 1, '2019-09-16 04:42:48'),
(2, 'Other Service', 1, '2019-09-16 04:42:48'),
(3, 'Test', 0, '2019-12-22 04:25:48');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `room_id` int(11) DEFAULT '0',
  `shift_id` int(11) DEFAULT '0',
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_time` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_finish` tinyint(4) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `code`, `name`, `branch_id`, `room_id`, `shift_id`, `start_date`, `end_date`, `start_time`, `end_time`, `is_finish`, `create_by`, `active`, `create_at`) VALUES
(1, 'EN101', 'English Level 1', 1, 1, 3, '2019-12-31', '2020-01-31', '05:30 PM', '07:30 PM', 0, 1, 0, '2019-12-26 04:37:25'),
(2, 'EN102-1', 'English Level 2', 1, 3, 1, '2019-12-31', '2020-01-31', '7:00 AM', '8:30 AM', 0, 0, 1, '2019-12-26 05:04:36');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `id` tinyint(4) NOT NULL,
  `en_name` varchar(120) NOT NULL,
  `kh_name` varchar(50) DEFAULT NULL,
  `address` varchar(120) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `website` varchar(90) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `description` text,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `header` text,
  `footer` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`id`, `en_name`, `kh_name`, `address`, `email`, `phone`, `website`, `logo`, `description`, `create_at`, `header`, `footer`) VALUES
(1, 'Vdoo School', 'ក្រុមហ៊ុន វីឌូ', 'Phnom Penh, Cambodia', 'info@vdoo.biz', '017 837 754', 'www.vdoo.biz', 'uploads/logos/logo.png', 'A group of profession freelance developers in Cambodia!', '2019-02-03 15:40:10', 'The company you trust!', 'Try it for free!');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` bigint(20) NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `gender` varchar(9) NOT NULL,
  `dob` date DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `position` varchar(90) DEFAULT NULL,
  `join_date` date DEFAULT NULL,
  `salary` float DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `entitlement` int(11) NOT NULL DEFAULT '0',
  `cutmoney` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_documents`
--

CREATE TABLE `employee_documents` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `employee_id` int(11) NOT NULL,
  `file_name` text NOT NULL,
  `description` text,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE `enrollments` (
  `id` bigint(20) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `enroll_date` date DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `enrollments`
--

INSERT INTO `enrollments` (`id`, `student_id`, `class_id`, `shift_id`, `room_id`, `branch_id`, `enroll_date`, `active`, `create_at`, `create_by`) VALUES
(1, 3, 2, 1, 3, 1, '2019-12-26', b'0', '2019-12-26 06:09:29', 1),
(2, 2, 2, 1, 3, 1, '2019-12-26', b'0', '2019-12-26 07:45:39', 1),
(3, 3, 2, 1, 3, 1, '2019-12-26', b'0', '2019-12-26 07:45:46', 1),
(4, 3, 2, 1, 3, 1, '2019-12-26', b'0', '2019-12-26 07:53:15', 1),
(5, 3, 2, 1, 3, 1, '2019-12-26', b'0', '2019-12-26 07:54:58', 1),
(6, 2, 2, 1, 3, 1, '2019-12-31', b'0', '2019-12-26 07:55:11', 1),
(7, 3, 2, 1, 3, 1, '2019-12-26', b'1', '2019-12-26 07:55:14', 1),
(8, 2, 1, 3, 1, 1, '2019-12-26', b'0', '2019-12-26 08:03:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) NOT NULL,
  `student_id` int(11) NOT NULL,
  `invoice_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `total` float NOT NULL DEFAULT '0',
  `paid` float NOT NULL DEFAULT '0',
  `due_amount` float NOT NULL DEFAULT '0',
  `note` text,
  `reference` varchar(50) DEFAULT NULL,
  `recurring` tinyint(4) NOT NULL DEFAULT '0',
  `recurring_day` int(11) NOT NULL DEFAULT '0',
  `is_paid` tinyint(4) NOT NULL DEFAULT '0',
  `already_create_next` tinyint(4) NOT NULL DEFAULT '0',
  `next_date` date DEFAULT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `student_id`, `invoice_date`, `due_date`, `total`, `paid`, `due_amount`, `note`, `reference`, `recurring`, `recurring_day`, `is_paid`, `already_create_next`, `next_date`, `branch_id`, `create_at`, `create_by`) VALUES
(1, 3, '2019-12-24', '2019-12-24', 10, 10, 0, 'Test', 'R001', 1, 90, 0, 0, NULL, 1, '2019-12-24 16:11:32', 1),
(2, 2, '2019-12-25', '2019-12-31', 10, 10, 0, NULL, NULL, 1, 30, 0, 0, '2020-01-24', 1, '2019-12-25 00:43:43', 1),
(3, 3, '2019-12-25', '2019-12-31', 10, 0, 10, NULL, NULL, 0, 0, 0, 0, NULL, 1, '2019-12-25 01:47:33', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` bigint(20) NOT NULL,
  `invoice_id` bigint(20) NOT NULL,
  `item_id` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `unitprice` float NOT NULL,
  `discount` float NOT NULL,
  `subtotal` float NOT NULL,
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `invoice_id`, `item_id`, `quantity`, `unitprice`, `discount`, `subtotal`, `branch_id`, `create_by`, `create_at`) VALUES
(1, 1, 1, 1, 10, 0, 10, 1, 1, '2019-12-24 16:11:32'),
(2, 2, 1, 1, 10, 0, 10, 1, 1, '2019-12-25 00:43:43'),
(3, 3, 1, 1, 10, 0, 10, 1, 1, '2019-12-25 01:47:33');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_payments`
--

CREATE TABLE `invoice_payments` (
  `id` bigint(20) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `pay_date` date DEFAULT NULL,
  `amount` float NOT NULL,
  `method` varchar(30) NOT NULL DEFAULT 'cash',
  `branch_id` int(11) NOT NULL DEFAULT '1',
  `student_id` int(11) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_payments`
--

INSERT INTO `invoice_payments` (`id`, `invoice_id`, `pay_date`, `amount`, `method`, `branch_id`, `student_id`, `create_at`, `create_by`) VALUES
(2, 1, '2019-12-24', 10, 'Cash', 1, 0, '2019-12-24 16:32:34', 1),
(3, 2, '2019-12-25', 10, 'Cash', 1, 0, '2019-12-25 00:44:22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `category_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `discount` float NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `description`, `category_id`, `price`, `discount`, `active`, `create_at`) VALUES
(1, 'School Fee', NULL, 2, 10, 0, 1, '2019-09-16 04:53:45'),
(2, 'Test Item', 'Test', 1, 10, 0, 0, '2019-12-22 03:39:46');

-- --------------------------------------------------------

--
-- Table structure for table `leaves`
--

CREATE TABLE `leaves` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total` float NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reason` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` int(11) NOT NULL,
  `loan_date` varchar(50) DEFAULT NULL,
  `employee_id` int(11) NOT NULL DEFAULT '0',
  `amount` float NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` bigint(20) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `student_id` bigint(20) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`id`, `name`, `gender`, `phone`, `address`, `student_id`, `active`, `create_at`) VALUES
(1, 'HENG Lay', 'm', '092958419', 'Kep', 3, 1, '2019-12-22 16:03:42');

-- --------------------------------------------------------

--
-- Table structure for table `payrolls`
--

CREATE TABLE `payrolls` (
  `id` int(11) NOT NULL,
  `payroll_date` varchar(30) NOT NULL,
  `total` float NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_details`
--

CREATE TABLE `payroll_details` (
  `id` bigint(20) NOT NULL,
  `payroll_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `salary` float NOT NULL DEFAULT '0',
  `loan` float NOT NULL DEFAULT '0',
  `leave1` float NOT NULL DEFAULT '0',
  `leave2` float NOT NULL DEFAULT '0',
  `cut` float NOT NULL DEFAULT '0',
  `total` float NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `name` varchar(90) NOT NULL,
  `alias` varchar(120) DEFAULT NULL,
  `list` tinyint(4) NOT NULL DEFAULT '0',
  `insert` tinyint(4) NOT NULL DEFAULT '0',
  `update` tinyint(4) NOT NULL DEFAULT '0',
  `delete` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `alias`, `list`, `insert`, `update`, `delete`) VALUES
(1, 'user', 'User', 0, 0, 0, 0),
(2, 'role', 'Role', 0, 0, 0, 0),
(3, 'item', 'Item', 0, 0, 0, 0),
(4, 'category', 'Category', 0, 0, 0, 0),
(5, 'shift', 'Shift', 0, 0, 0, 0),
(6, 'class', 'Classes', 0, 0, 0, 0),
(7, 'room', 'Rooms', 0, 0, 0, 0),
(8, 'recurring', 'Recurring Terms', 0, 0, 0, 0),
(9, 'student', 'Students', 0, 0, 0, 0),
(10, 'parent', 'Parents', 0, 0, 0, 0),
(11, 'invoice', 'Invoices', 0, 0, 0, 0),
(12, 'payment', 'Payments', 0, 0, 0, 0),
(13, 'enrollment', 'Enrollments', 0, 0, 0, 0),
(14, 'Employee', 'Employees', 0, 0, 0, 0),
(15, 'leave', 'Leave', 0, 0, 0, 0),
(16, 'loan', 'Loan', 0, 0, 0, 0),
(17, 'payroll', 'Payroll', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `recurring_terms`
--

CREATE TABLE `recurring_terms` (
  `id` int(11) NOT NULL,
  `name` varchar(120) NOT NULL,
  `day` int(11) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recurring_terms`
--

INSERT INTO `recurring_terms` (`id`, `name`, `day`, `create_at`) VALUES
(1, '1 month', 30, '2019-12-22 05:31:12'),
(2, '3 months', 90, '2019-12-22 05:32:17');

-- --------------------------------------------------------

--
-- Table structure for table `registrations`
--

CREATE TABLE `registrations` (
  `id` bigint(20) NOT NULL,
  `registration_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `student_id` bigint(20) NOT NULL,
  `class_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `active` bit(1) NOT NULL DEFAULT b'1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `registrations`
--

INSERT INTO `registrations` (`id`, `registration_date`, `start_date`, `end_date`, `student_id`, `class_id`, `shift_id`, `room_id`, `active`, `create_at`) VALUES
(1, NULL, '2019-10-04', '2019-10-26', 2, 2, 3, 2, b'1', '2019-10-05 02:49:37'),
(2, '2019-10-05 10:04:40', '2019-10-05', NULL, 1, 1, 1, 1, b'0', '2019-10-05 03:04:40'),
(3, '2019-10-05 10:18:24', '2019-10-05', '2019-10-05', 2, 1, 1, 1, b'1', '2019-10-05 03:18:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `active`) VALUES
(1, 'Administrator', 1),
(5, 'Staff', 1),
(20, 'teslkdjfasf', 0),
(19, 'test 1', 0),
(18, 'test', 0);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `list` int(11) NOT NULL DEFAULT '0',
  `insert` int(11) NOT NULL DEFAULT '0',
  `update` int(11) NOT NULL DEFAULT '0',
  `delete` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `role_id`, `permission_id`, `list`, `insert`, `update`, `delete`) VALUES
(1, 1, 1, 1, 1, 1, 1),
(2, 1, 2, 1, 1, 1, 1),
(3, 1, 3, 1, 1, 1, 1),
(4, 1, 4, 1, 1, 1, 1),
(5, 1, 5, 1, 1, 1, 1),
(6, 5, 1, 1, 1, 0, 0),
(7, 5, 2, 1, 1, 0, 0),
(8, 5, 3, 1, 1, 0, 0),
(9, 5, 4, 1, 1, 0, 0),
(10, 5, 5, 1, 1, 0, 0),
(11, 1, 6, 1, 1, 1, 1),
(12, 1, 7, 1, 1, 1, 1),
(13, 1, 8, 1, 1, 1, 1),
(14, 1, 9, 1, 1, 1, 1),
(15, 1, 10, 1, 1, 1, 1),
(16, 1, 11, 1, 1, 1, 1),
(17, 1, 12, 1, 1, 1, 1),
(18, 1, 13, 1, 1, 1, 1),
(19, 1, 14, 1, 1, 1, 1),
(20, 1, 15, 1, 1, 1, 1),
(21, 1, 16, 1, 1, 1, 1),
(22, 1, 17, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `branch_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `name`, `branch_id`, `active`, `create_at`) VALUES
(1, 'Room A1', 4, 1, '2019-10-04 16:43:16'),
(2, 'Room A2', 4, 1, '2019-10-05 02:42:41'),
(3, '101', 1, 1, '2019-12-22 02:56:43'),
(4, '102', 5, 0, '2019-12-22 02:57:16');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` tinyint(4) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `name`, `active`, `create_at`) VALUES
(1, 'Morning', 1, '2019-09-28 11:48:12'),
(2, 'Afternoon', 1, '2019-10-05 02:25:56'),
(3, 'Evening', 1, '2019-10-05 02:25:59'),
(4, 'Hello', 0, '2019-12-22 06:02:24');

-- --------------------------------------------------------

--
-- Table structure for table `stop_students`
--

CREATE TABLE `stop_students` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `stop_date` date NOT NULL,
  `reason` text NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kh_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `en_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dob` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `photo` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_stop` tinyint(4) NOT NULL DEFAULT '0',
  `is_finish` tinyint(4) NOT NULL DEFAULT '0',
  `branch_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `code`, `kh_name`, `en_name`, `gender`, `dob`, `phone`, `address`, `photo`, `active`, `create_at`, `is_stop`, `is_finish`, `branch_id`) VALUES
(1, 'Code-11', 'ស វិជ័យ', 'Sor Vichey1', 'm', '1996-11-11', '010 300 600 / 076 9300 600', '#244, ផ្លូវ 392 , សង្កាត់បឹងកេងកងទី ១,ខណ្ឌចំការម, ភ្នំពេញ', 'uploads/students/Kdioj4VXcblAXmIBcIQmUkYUqgjIo3ODozjSqYWt.jpeg', 0, '2019-09-28 11:34:31', 0, 0, 1),
(2, 'ST002', 'រិទ្ធី សំ', 'Rithy Sam', 'm', '2019-10-05', '017837754', '#244, ផ្លូវ 392 , សង្កាត់បឹងកេងកងទី ១,ខណ្ឌចំការម, ភ្នំពេញ', 'uploads/students/DiTiQQIB6FluceHAhw9GkTs8uThWS8BsSsnVwkBk.jpeg', 1, '2019-10-05 02:45:20', 0, 0, 1),
(3, 'ST001', 'HENG Vongkol', 'ហេង វង្សកុល', 'm', '1987-02-10', '017837754', 'Phnom Penh, Cambodia', 'uploads/students/hKeL5H0qyzSbEIP8ou71PqIpohwdOjVe1zbsWnlM.jpeg', 1, '2019-12-22 14:24:18', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `photo` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT 'uploads/users/default.png',
  `language` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `photo`, `language`, `role_id`, `username`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$/qD60q8mFQnqwnle0S2FLOKQcUMbdcqUDu4w0f7cNsl1lbIGTpq3m', 'Sh10td7AP3H5QJgabEwxT9pCOoDXhHa8QlCIub0XThcsQKVNoKzpYyr1qn1B', '2017-05-27 22:35:52', '2017-05-27 22:35:52', 'uploads/users/P8AXvTrckXv1DK6NYx9eKJZ1xM3toO8WlCojgVLm.jpeg', 'km', 1, 'admin'),
(12, 'SAM Vuthy', 'vuthy@gmail.com', '$2y$10$s2cIzrDuiPmVZ752tSJY3eGK6fR6wr0rnjUNaSl9RwoLFfSJzwooS', NULL, NULL, NULL, 'uploads/users/uV6lmqNXyHbXPcjMHoJByUncxbGxnFwZEIsbtk9f.jpeg', 'en', 1, 'vuthy1');

-- --------------------------------------------------------

--
-- Table structure for table `user_branches`
--

CREATE TABLE `user_branches` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_branches`
--

INSERT INTO `user_branches` (`id`, `user_id`, `branch_id`, `created_at`) VALUES
(6, 1, 1, '2019-12-19 13:54:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_documents`
--
ALTER TABLE `employee_documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollments`
--
ALTER TABLE `enrollments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leaves`
--
ALTER TABLE `leaves`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payrolls`
--
ALTER TABLE `payrolls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_details`
--
ALTER TABLE `payroll_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `recurring_terms`
--
ALTER TABLE `recurring_terms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registrations`
--
ALTER TABLE `registrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stop_students`
--
ALTER TABLE `stop_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `user_branches`
--
ALTER TABLE `user_branches`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `companies`
--
ALTER TABLE `companies`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employee_documents`
--
ALTER TABLE `employee_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enrollments`
--
ALTER TABLE `enrollments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_payments`
--
ALTER TABLE `invoice_payments`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `leaves`
--
ALTER TABLE `leaves`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payrolls`
--
ALTER TABLE `payrolls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll_details`
--
ALTER TABLE `payroll_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `recurring_terms`
--
ALTER TABLE `recurring_terms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `registrations`
--
ALTER TABLE `registrations`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` tinyint(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `stop_students`
--
ALTER TABLE `stop_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_branches`
--
ALTER TABLE `user_branches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
