<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  'DashboardController@index');
Route::get('home', 'DashboardController@index');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index');

// employee
Route::get('employee', 'EmployeeController@index');
Route::get('employee/create', 'EmployeeController@create');
Route::get('employee/edit/{id}', 'EmployeeController@edit');
Route::get('employee/delete/{id}', 'EmployeeController@delete');
Route::get('employee/detail/{id}', 'EmployeeController@detail');
Route::post('employee/save', 'EmployeeController@save');
Route::post('employee/update', 'EmployeeController@update');
Route::post('employee/document/save', 'EmployeeController@save_document');
Route::get('employee/document/delete', 'EmployeeController@delete_document');

// leave request
Route::get('leave', 'LeaveController@index');
Route::get('leave/create', 'LeaveController@create');
Route::get('leave/delete/{id}', 'LeaveController@delete');
Route::get('leave/edit/{id}', 'LeaveController@edit');
Route::get('leave/detail/{id}', 'LeaveController@detail');
Route::get('leave/approve/{id}', 'LeaveController@approve');
Route::post('leave/save', 'LeaveController@save');
Route::post('leave/update', 'LeaveController@update');

// loan route
Route::get('loan', 'LoanController@index');
Route::get('loan/create', 'LoanController@create');
Route::get('loan/edit/{id}', 'LoanController@edit');
Route::get('loan/delete/{id}', 'LoanController@delete');
Route::post('loan/save', 'LoanController@save');
Route::post('loan/update', 'LoanController@update');

// payroll route
Route::get('payroll', 'PayrollController@index');
Route::get('payroll/create', 'PayrollController@create');
Route::get('payroll/delete/{id}', 'PayrollController@delete');
Route::get('payroll/detail/{id}', 'PayrollController@detail');
Route::post('payroll/save', 'PayrollController@save');

// enrollment
Route::get('enrollment', 'EnrollmentController@index');
Route::get('enrollment/create', 'EnrollmentController@create');
Route::post('enrollment/save', 'EnrollmentController@save');
Route::get('enrollment/edit/{id}', 'EnrollmentController@edit');
Route::get('enrollment/delete', 'EnrollmentController@delete');
Route::post('enrollment/update', 'EnrollmentController@update');

// student route
Route::get('student', 'StudentController@index');
Route::get('student/create', 'StudentController@create');
Route::get('student/edit/{id}', 'StudentController@edit');
Route::get('student/delete/{id}', 'StudentController@delete');
Route::post('student/save', 'StudentController@save');
Route::post('student/update', 'StudentController@update');
Route::get('student/detail/{id}', 'StudentController@detail');
Route::get('student/search', 'StudentController@search');

Route::post('student/enrollment/save', 'StudentController@student_enrollment');
Route::get('student/enrollment/delete/{id}', 'StudentController@delete_enrollment');

// report student
Route::get('report', 'ReportController@index');
Route::get('report/search', 'ReportController@search');
Route::get('report/room', 'ReportController@room');
Route::get('report/room/search', 'ReportController@room_search');
Route::get('report/class', 'ReportController@class');
Route::get('report/class/search', 'ReportController@class_search');
Route::get('report/shift', 'ReportController@shift');
Route::get('report/shift/search', 'ReportController@shift_search');