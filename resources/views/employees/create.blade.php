@extends('layouts.app')
@section('header')
	<i class="fa fa-users"></i>	<strong>បញ្ចូល បុគ្គលិកថ្មី</strong>
@endsection
@section('content')
<form action="{{url('employee/save')}}" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}
	<div class="card card-gray">
		<div class="toolbox">
			<button type="submit" class="btn btn-oval btn-primary btn-sm">
				<i class="fa fa-save"></i> រក្សាទុក
			</button>
			<button type="reset" class="btn btn-danger btn-oval btn-sm">
				<i class="fa fa-refresh"></i> បោះបង់
			</button>
			<a href="{{url('employee')}}" class="btn btn-oval btn-warning btn-sm">
				<i class="fa fa-reply"></i> ថយក្រោយ
			</a>
		</div>
		<div class="card-block">
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('error')}}
				</div>
			</div>
		@endif

			<div class="row">
				<div class="col-sm-6">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">នាមត្រកូល <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="first_name"
								value="{{old('first_name')}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">នាមខ្លួន <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="last_name"  
								value="{{old('last_name')}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ភេទ <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<select name="gender" class="form-control">
								<option value="ប្រុស">ប្រុស</option>
								<option value="ស្រី">ស្រី</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ភាគរយ <span class="text-danger"> (%)</span></label>
						<div class="col-sm-8">
							<input type="percent" class="form-control" name="percent" value="{{old('percent')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃខែឆ្នាំកំណើត</label>
						<div class="col-sm-8">
							<input type="date" class="form-control" name="dob" value="{{old('dob')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">អ៊ីម៉ែល</label>
						<div class="col-sm-8">
							<input type="email" class="form-control" name="email" value="{{old('email')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">លេខទូរស័ព្ទ</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="phone" value="{{old('phone')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">តួនាទី</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="position" value="{{old('position')}}">
						</div>
					</div>
					
				</div>
				<div class="col-sm-6">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃចូលធ្វើការ</label>
						<div class="col-sm-8">
							<input type="date" class="form-control" name="join_date" value="{{date('Y-m-d')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ប្រាក់ខែ($)</label>
						<div class="col-sm-8">
							<input type="number" class="form-control" step="0.1" min="1" name="salary" value="{{old('salary')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" 
							title="ចំនួនច្បាប់អនុញ្ញាត ឈប់សម្រាកក្នុងមួយខែ">ចំនួនច្បាប់ <i class="fa fa-question text-danger"></i></label>
						<div class="col-sm-8">
							<input type="number" class="form-control" step="1" 
								min="0" name="entitlement" value="{{old('entitelment')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" 
							title="លុយកាត់ពីបុគ្គលិក ពេលឈប់អត់ច្បាប់ក្នុងមួយថ្ងៃ">កាត់លុយឈប់ <i class="fa fa-question text-danger"></i></label>
						<div class="col-sm-8">
							<input type="number" class="form-control" step="1" 
								min="0" name="cutmoney" value="{{old('cutmoney')}}">
						</div>
					</div>
					<div class="form-group row">
						<label for="submit" class="col-sm-3 form-control-label">&nbsp;</label>
						<div class="col-sm-8">
							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</form>
@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_staff").addClass("active");
		
	})
</script>
@endsection
