@extends('layouts.app')
@section('header')
	<i class="fa fa-users"></i>	<strong>បុគ្គលិក</strong>
@endsection
@section('content')

<div class="card card-gray">
	<div class="toolbox">
		<a href="{{url('employee/create')}}" class="btn btn-primary btn-oval btn-sm">
			<i class="fa fa-plus-circle"></i> {{trans('label.create')}}</a>
	</div>
	<div class="card-block">
		<table class="table table-sm table-bordered">
			<thead >
				<tr>
					<th>ល.រ</th>
					<th>ឈ្មោះ</th>
					<th>ភេទ</th>
					<th>ថ្ងៃខែឆ្នាំកំណើត</th>
					<th>អ៊ីម៉ែល</th>
					<th>ទូរស័ព្ទ</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
					$pagex = @$_GET['page'];
					if(!$pagex)
					$pagex = 1;
					$i = 18 * ($pagex - 1) + 1;
				?>
				@foreach($employees as $emp)
				<tr class="odd gradeX">
					<td>{{$i++}}</td>
					<td>
						<a href="{{url('employee/detail/'.$emp->id)}}">​{{$emp->last_name}}​ {{$emp->first_name}} </a>
					</td>
					<td>{{$emp->gender}}</td>
					<td>{{$emp->dob}}</td>
					<td>{{$emp->email}}</td>
					<td>{{$emp->phone}}</td>
					<td>
						
						<a href="{{url('employee/edit/'.$emp->id)}}" title="កែប្រែ" class="text-success">
							<i class="fa fa-edit"></i>
						</a>&nbsp;&nbsp;
						<a href="{{url('employee/delete/'.$emp->id)}}" title="លុប" 
							onclick="return confirm('តើអ្នកពិតជាចង់លុបមែនទេ?')" class="text-danger">
							<i class="fa fa-trash"></i>
						</a>
					</td>
				</tr>
				@endforeach
				
			</tbody>
		</table>
		{{$employees->links()}}
	</div>
</div>

@endsection

@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");

		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_staff").addClass("active");
		
	})
</script>
@endsection