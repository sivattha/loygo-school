@extends('layouts.app')
@section('content')

<div class="card card-gray">
	<div class="card-header">
            <div class="header-block">
			<strong>ព័ត៌មានលម្អិត បុគ្គលិក</strong> 
			<a href="{{url('employee')}}" class="btn btn-oval btn-primary btn-sm mx-left"><i class="fa fa-reply"></i> ត្រលប់ក្រោយ</a>
			<a href="{{url('employee/create')}}" class="btn btn-oval btn-primary btn-sm mx-left">
				<i class="fa fa-plus"></i> បង្កើត</a>
			<a href="{{url('employee/edit/'.$emp->id)}}" class="btn btn-oval btn-warning btn-sm mx-left">
				<i class="fa fa-edit"></i> កែប្រែ</a>
			<a href="{{url('employee/delete/'.$emp->id)}}" 
				class="btn btn-oval btn-danger btn-sm mx-left" onclick="return confirm('តើអ្នកពិតជាចង់លុបមែនទេ?')">
				<i class="fa fa-trash"></i> លុប</a>
		</div>
	</div>
	<div class="card-block">
        @if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		<form>
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">នាមត្រកូល</label>
						<div class="col-sm-8">
							: {{$emp->first_name}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">នាមខ្លួន</label>
						<div class="col-sm-8">
							: {{$emp->last_name}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ភេទ</label>
						<div class="col-sm-8">
							: {{$emp->gender}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ភាគរយ</label>
						<div class="col-sm-8">
							: {{$emp->percent}} %
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃខែឆ្នាំកំណើត</label>
						<div class="col-sm-8">
							: {{$emp->dob}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">អ៊ីម៉ែល</label>
						<div class="col-sm-8">
                            : {{$emp->email}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">លេខទូរស័ព្ទ</label>
						<div class="col-sm-8">
                            : {{$emp->phone}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">តួនាទី</label>
						<div class="col-sm-8">
                            : {{$emp->position}}
						</div>
					</div>
					
				</div>
				<div class="col-sm-6">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃចូលធ្វើការ</label>
						<div class="col-sm-8">
                            : {{$emp->join_date}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ប្រាក់ខែ($)</label>
						<div class="col-sm-8">
                            : $ {{$emp->salary}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" 
							title="ចំនួនច្បាប់អនុញ្ញាត ឈប់សម្រាកក្នុងមួយខែ">ចំនួនច្បាប់ <i class="fa fa-question text-danger"></i></label>
						<div class="col-sm-8">
							: {{$emp->entitlement}} ថ្ងៃ
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" 
							title="លុយកាត់ពីបុគ្គលិក ពេលឈប់អត់ច្បាប់ក្នុងមួយថ្ងៃ">កាត់លុយឈប់ <i class="fa fa-question text-danger"></i></label>
						<div class="col-sm-8">
							: $ {{$emp->cutmoney}}
						</div>
					</div>
					
				</div>
			</div>
		
        </form>
        <div class="row">
            <div class="col-sm-2">
                <strong class="text-primary"><u>ឯកសារភ្ជាប់</u></strong>
            </div>
            <div class="col-sm-6">
                <a href="#" class='btn btn-primary btn-oval btn-sm' 
                    data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> បញ្ចូលឯកសារ</a>
            </div>
        </div>
        <p></p>
        <table class="table table-bordered table-sm">
            <thead>
                <tr>
                    <th>ល.រ</th>
                    <th>ឈ្មោះឯកសារ</th>
                    <th>បរិយាយ</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @php($i=1)
                @foreach($docs as $doc)
                    <tr>
                        <td>{{$i++}}</td>
                        <td>
                            <a href="{{asset($doc->file_name)}}" target="_blank">{{$doc->title}}</a>
                        </td>
                        <td>{{$doc->description}}</td>
                        <td>
                            <a href="{{url('employee/document/delete?id='.$doc->id .'&empid='.$emp->id)}}" 
                                class="text-danger" title="លុប" onclick="return confirm('តើអ្នកពិតជាចង់លុមមែនទេ?')">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <form action="{{url('employee/document/save')}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{$emp->id}}">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">បញ្ចូលឯកសារថ្មី</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
				<div class="form-group row">
					<label for="title" class="col-sm-3 form-control-label">ចំណងជើង <span class="text-danger">*</span></label>
					<div class="col-sm-8">
                        <input type="text" class="form-control" id="title" name="title" 
                            value="{{old('title')}}" required >
					</div>
				</div>
				<div class="form-group row">
					<label for="file_name" class="col-sm-3 form-control-label">ឈ្មោះឯកសារ <span class="text-danger">*</span> </label>
					<div class="col-sm-8">
                        <input type="file" class="form-control" id="file_name" name="file_name" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="description" class="col-sm-3 form-control-label">បរិយាយ</label>
					<div class="col-sm-8">
                        <textarea name="description" class='form-control' id="description" cols="30" rows="3">{{old('description')}}</textarea>
					</div>
				</div>
            </div>
            <div class="modal-footer">
                <div style='padding: 5px'>
                    <button type="submit" class="btn btn-primary">រក្សាទុក</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">ចាកចេញ</button>
                </div>
            </div>
        </div>
    </form>
  </div>
</div>
@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_staff").addClass("active");
		
	})
</script>
@endsection
