@extends('layouts.app')
@section('content')

<div class="card card-gray">
		<div class="card-header">
            <div class="header-block">
			<strong>កែប្រែ បុគ្គលិក</strong> 
			<a href="{{url('employee')}}" class="btn btn-oval btn-primary btn-sm mx-left">
				<i class="fa fa-reply"></i> ត្រលប់ក្រោយ</a>
			</div>
	</div>
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('error')}}
				</div>
			</div>
		@endif
			<div class="card-block">
		<form action="{{url('employee/update')}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}
			<input type="hidden" name="id" value="{{$emp->id}}">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">គោត្តនាម <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="first_name"
								value="{{$emp->first_name}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">នាម <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="last_name"  
								value="{{$emp->last_name}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ភេទ <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<select name="gender" class="form-control">
								<option value="ប្រុស" {{$emp->gender=='ប្រុស'?'selected':''}}>ប្រុស</option>
								<option value="ស្រី" {{$emp->gender=='ស្រី'?'selected':''}}>ស្រី</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ភាគរយ <span class="text-danger"> (%)</span></label>
						<div class="col-sm-8">
							<input type="percent" class="form-control" name="percent" value="{{$emp->percent}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃខែឆ្នាំកំណើត</label>
						<div class="col-sm-8">
							<input type="date" class="form-control" name="dob" value="{{$emp->dob}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">អ៊ីម៉ែល</label>
						<div class="col-sm-8">
							<input type="email" class="form-control" name="email" value="{{$emp->email}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">លេខទូរស័ព្ទ</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="phone" value="{{$emp->phone}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">តួនាទី</label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="position" value="{{$emp->position}}">
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃចូលធ្វើការ</label>
						<div class="col-sm-8">
							<input type="date" class="form-control" name="join_date" value="{{$emp->join_date}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ប្រាក់ខែ</label>
						<div class="col-sm-8">
							<input type="number" class="form-control" step="0.1" min="1" name="salary" value="{{$emp->salary}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" 
							title="ចំនួនច្បាប់អនុញ្ញាត ឈប់សម្រាកក្នុងមួយខែ">ចំនួនច្បាប់ <i class="fa fa-question text-danger"></i></label>
						<div class="col-sm-8">
							<input type="number" class="form-control" step="1" 
								min="0" name="entitlement" value="{{$emp->entitlement}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label" 
							title="លុយកាត់ពីបុគ្គលិក ពេលឈប់អត់ច្បាប់ក្នុងមួយថ្ងៃ">កាត់លុយឈប់ <i class="fa fa-question text-danger"></i></label>
						<div class="col-sm-8">
							<input type="number" class="form-control" step="1" 
								min="0" name="cutmoney" value="{{$emp->cutmoney}}">
						</div>
					</div>
					<div class="form-group row">
						<label for="submit" class="col-sm-3 form-control-label">&nbsp;</label>
						<div class="col-sm-8">
							<button type="submit" class="btn btn-oval btn-primary">
								<i class="fa fa-save "></i> រក្សាទុក
							</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_staff").addClass("active");
		
	})
</script>
@endsection
