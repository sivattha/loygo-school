<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title> Vdoo Freelance Team</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="{{asset('css/vendor.css')}}">
        <link rel="stylesheet" id="theme-style" href="{{asset('css/app.css')}}">
        <link rel="stylesheet" id="theme-style" href="{{asset('css/custom.css')}}">
        <style>
            .logo{
                margin-top: -20px;
            }
        </style>
       
    </head>
    <body>
        <div class="auth">
                <div class="auth-container">
                    <div class="card">
                        <header class="auth-header">
                            <h1 class="auth-title">
                                <div class="logo">
                                 <img src="{{asset('logo.png')}}" alt="Logo" width="45">
                                </div> Vdoo Freelance Team
                            </h1>
                        </header>
                        <div class="auth-content">
                            <p class="text-center">LOGIN TO CONTINUE</p>
                            <form id="login-form" method="POST" action="{{ route('login') }}" novalidate="">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input type="username" class="form-control underlined" placeholder="Your username" name="username" value="{{ old('username') }}" required autofocus>
                                    
                                    @if ($errors->has('username'))
                                        <strong style="color: red;">{{ $errors->first('username') }}</strong>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control underlined"  id="password" placeholder="Your password" required name="password" >
                                    @if ($errors->has('password'))
                                            <strong style="color: red;">{{ $errors->first('password') }}</strong>
                                    @endif
                                </div>
                              
                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-primary btn-lg">Login</button>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                   
                </div>
            </div>
            <!-- Reference block for JS -->
            <div class="ref" id="ref">
                <div class="color-primary"></div>
                <div class="chart">
                    <div class="color-primary"></div>
                    <div class="color-secondary"></div>
                </div>
            </div>
        </body>
    </html>



