@extends('layouts.app')

@section('content')
                       
    <div class="card card-gray">
        <div class="card-header">
           <div class="col-sm-3">
                <div class="header-block">
                    <p class="title">  Report Student by Shift
                    </p>
                    
                </div>
           </div>
           <div class="col-sm-8">
                <form action="{{url('report/shift/search')}}">
                    Shift
                    <span class="text-danger">*</span>&nbsp;&nbsp; 
                    <select name="shift_id" id="shift_id">
                        @foreach($shifts as $r)
                        <option value="{{$r->id}}">{{$r->name}}</option>
                        @endforeach
                    </select>  &nbsp;&nbsp;
                    Start Date <span class="text-danger">*</span>&nbsp;&nbsp;<input type="date" name="start_date" value="{{$start_date}}"> 
               
                 
                   
                    <button type="submit">ស្វែងរក</button>
                </form>
           </div>
        </div>
        <hr>
        <div class="card-block">
           @if(count($enrollments)>0)
           <div class="table-flip-scroll">
                <p>
                    <strong class='text-danger'>All Student {{count($enrollments)}} </strong>
                </p>
                <table class="table table-sm table-bordered table-striped flip-content table-hover">
                <thead class="flip-header">
                    <tr>
                        <th>#</th>
                        <th>English Name</th>
                        <th>Khmer Name</th>
                        <th>Enrollment Date</th>
                        <th>Class</th>
                        <th>Shift</th>
                        <th>Room</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $pagex = @$_GET['page'];
                        if(!$pagex)
                            $pagex = 1;
                        $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($enrollments as $e)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>
                            <td>{{$e->en_name}}</td>
                            <td>{{$e->kh_name}}</td>
                            <td>{{date('d-M-Y', strtotime($e->registration_date))}} </td>
                            <td >{{$e->class}}</td>
                            <td class="text-success font-weight-bold">{{$e->shift}}</td>
                            <td>{{$e->room}}</td>
                            <td  class="text-success font-weight-bold">{{date('d-M-Y', strtotime($e->start_date))}}</td>
                            <td>{{date('d-M-Y', strtotime($e->end_date))}}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
           </div>
           @endif
        </div>
    </div>
                           
@endsection

@section('js')
	<script>
         $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_report").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#report_shift").addClass("active");
			
        })
    </script>
@endsection