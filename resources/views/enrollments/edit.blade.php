@extends('layouts.app')
@section('header')
    <strong>Edit Student Enrollment</strong>
@endsection
@section('content')
<form action="{{url('enrollment/update')}}" method="POST">                       
    <div class="card card-gray">
        <div class="toolbox">
        <button type="submit" class="btn btn-sm btn-oval btn-primary">
                <i class="fa fa-save "></i> Save Change
            </button>

            <a href="{{url('enrollment')}}" class="btn btn-warning btn-oval btn-sm ">
                <i class="fa fa-reply"></i> Back
            </a>
        </div>
		<hr>						
        <div class="card-block">
            <div class="col-md-6">
                @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div>
                            {{session('success')}}
                        </div>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div>
                            {{session('error')}}
                        </div>
                    </div>
                @endif
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{$enrollment->id}}">
                    <div class="form-group row">
                        <label for="student_id" class="col-sm-4 form-control-label">Student <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="student_id" id="student_id" class="form-control chosen-select" required>
                                <option value="">--Select--</option>
                                @foreach($students as $s)
                                <option value="{{$s->id}}" {{$s->id==$enrollment->student_id?'selected':''}}>{{$s->en_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="class_id" class="col-sm-4 form-control-label">Class <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="class_id" id="class_id" class="form-control chosen-select" required>
                                <option value="">--Select--</option>
                                @foreach($classes as $c)
                                <option value="{{$c->id}}" {{$c->id==$enrollment->class_id?'selected':''}}>{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="room_id" class="col-sm-4 form-control-label">Room <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="room_id" id="room_id" class="form-control chosen-select" required>
                                <option value="">--Select--</option>
                                @foreach($rooms as $r)
                                <option value="{{$r->id}}" {{$r->id==$enrollment->room_id?'selected':''}}>{{$r->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="shift_id" class="col-sm-4 form-control-label">Shift <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="shift_id" id="shift_id" class="form-control chosen-select" required>
                                <option value="">--Select--</option>
                                @foreach($shifts as $s)
                                <option value="{{$s->id}}" {{$s->id==$enrollment->shift_id?'selected':''}}>{{$s->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="start_date" class="col-sm-4 form-control-label">Start Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="start_date" name="start_date"  required value="{{$enrollment->start_date}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="end_date" class="col-sm-4 form-control-label">Start End </label>
                        <div class="col-sm-8">
                            <input type="date" class="form-control" id="end_date" name="end_date" value="{{$enrollment->end_date}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="start_date" class="col-sm-4 form-control-label"></label>
                        <div class="col-sm-8">
                        <button type="submit" class="btn btn-sm btn-oval btn-primary">
                            <i class="fa fa-save "></i> Save Change
                        </button>
                    </div>
            </div>

            
        
  
    </div>

</form>                            
@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_enrollment").addClass("active open");
			$("#setting_collapse").addClass("collapse in");
            $("#enrollment_id").addClass("active");
			
        })
    </script>
@endsection