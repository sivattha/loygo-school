@extends('layouts.app')
@section('header')
    <i class="fa fa-door"></i><strong>Student Enrollment</strong>
@endsection
@section('content')
                       
    <div class="card card-gray">
        <div class="toolbox">
            <a href="{{url('enrollment/create')}}" class="btn btn-primary btn-oval btn-sm ">
                <i class="fa fa-plus-circle"></i> Create
            </a>
        </div>
        <div class="card-block">
            @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <div>
                            {{session('success')}}
                        </div>
                    </div>
            @endif
            <table class="table table-sm table-bordered table-striped flip-content table-hover">
                <thead class="flip-header">
                    <tr>
                        <th>#</th>
                        <th>English Name</th>
                        <th>Khmer Name</th>
                        <th>Enrollment Date</th>
                        <th>Class</th>
                        <th>Shift</th>
                        <th>Room</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $pagex = @$_GET['page'];
                        if(!$pagex)
                            $pagex = 1;
                        $i = config('app.row') * ($pagex - 1) + 1;
                    ?>
                    @foreach($enrollments as $e)
                        <tr class="odd gradeX">
                            <td>{{$i++}}</td>
                            <td>{{$e->en_name}}</td>
                            <td>{{$e->kh_name}}</td>
                            <td>{{date('d-M-Y', strtotime($e->registration_date))}} </td>
                            <td>{{$e->class}}</td>
                            <td>{{$e->shift}}</td>
                            <td>{{$e->room}}</td>
                            <td>{{date('d-M-Y', strtotime($e->start_date))}}</td>
                            <td>{{date('d-M-Y', strtotime($e->end_date))}}</td>
                            <td>
                                <a href="{{url('enrollment/edit/'.$e->id)}}" title="Update" class="text-success"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
                                <a href="{{url('enrollment/delete?id='.$e->id)}}" title="Delete" onclick="return confirm('You want to delete?')" class="text-danger"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$enrollments->links()}}
        </div>
    </div>
                           
@endsection

@section('js')
<script>
       $(document).ready(function () {
            $("#sidebar-menu li").removeClass('active');
            $("#menu_enrollment").addClass('active');
        })
    </script>
@endsection