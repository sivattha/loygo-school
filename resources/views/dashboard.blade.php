@extends('layouts.app')

@section('content')
<div class="row sameheight-container">
    <div class="col col-12 col-sm-12 col-md-5 col-xl-5 stats-col">
        <div class="card sameheight-item stats" data-exclude="xs" style="height: 322.6px;">
            <div class="card-block">
                <div class="title-block">
                    <h4 class="title text-warning"> Student Information </h4>
                    </p>
                </div>
                <hr>
                <div class="row row-sm stats-container">
                    <div class="col-12 col-sm-6 stat-col">
                        <div class="stat-icon">
                            <i class="fa fa-arrow-right"></i>
                        </div>
                        <div class="stat text-primary">
                            <div class="value"> {{$today}} នាក់</div>
                            <div class="name"> ថ្ងៃនេះ </div>
                        </div>
                        <div class="progress stat-progress">
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 stat-col">
                        <div class="stat-icon">
                            <i class="fa fa-arrow-up"></i>
                        </div>
                        <div class="stat text-warning">
                            <div class="value"> {{$week}} នាក់</div>
                            <div class="name"> អាទិត្យនេះ </div>
                        </div>
                        <div class="progress stat-progress">
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6  stat-col">
                        <div class="stat-icon">
                            <i class="fa fa-circle"></i>
                        </div>
                        <div class="stat text-success">
                            <div class="value"> {{$month}} នាក់</div>
                            <div class="name"> ខែនេះ </div>
                        </div>
                        <div class="progress stat-progress">
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6  stat-col">
                        <div class="stat-icon">
                            <i class="fa fa-arrow-right"></i>
                        </div>
                        <div class="stat text-info">
                            <div class="value"> {{$year}} នាក់</div>
                            <div class="name"> ឆ្នាំនេះ </div>
                        </div>
                        <div class="progress stat-progress">
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6  stat-col">
                        <div class="stat-icon">
                            <i class="fa fa-star"></i>
                        </div>
                        <div class="stat">
                            <div class="value"> {{$all}} នាក់</div>
                            <div class="name"> សរុបរួម</div>
                        </div>
                        <div class="progress stat-progress">
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 stat-col">
                        <div class="stat-icon">
                            <i class="fa fa-dollar"></i>
                        </div>
                        <div class="stat">
                            <div class="value"> {{$f}} នាក់</div>
                            <div class="name"> សរុប ស្រី </div>
                        </div>
                        <div class="progress stat-progress">
                            <div class="progress-bar" style="width: 50%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col col-12 col-sm-12 col-md-7 col-xl-7 stats-col">
        <div class="card sameheight-item stats" data-exclude="xs" style="height: 322.6px;">
            <div class="card-block">
                <div class="title-block">
                    <h4 class="title text-success"> Due Payments</h4>
                    </p>
                </div>
                <div class="row row-sm stats-container">
                    <table class="table table-sm table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice No.</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Total</th>
                                <th>Paid</th>
                                <th>Due Amount</th>
                                <th>Student</th>
                            </tr>
                        </thead>
                        <tbody>	
                            <?php
                $pagex = @$_GET['page'];
                if(!$pagex)
                    $pagex = 1;
                $i = 5 * ($pagex - 1) + 1;
            ?>
            @foreach($invoices as $p)
                <tr>
                    <td>{{$i++}}</td>
                    <td>
                        <a href="{{route('invoice.detail', $p->id)}}">INV00{{$p->id}}</a>
                    </td>
                    <td>{{$p->invoice_date}}</td>
                    <td>{{$p->due_date}}</td>
                    <td>$ {{$p->total}}</td>
                    <td>$ {{$p->paid}}</td>
                    <td>$ {{$p->total - $p->paid}}</td>
                    <td>
                        <a href="{{route('student.detail', $p->student_id)}}">{{$p->kh_name}}</a>
                    </td>
                   
                </tr>
            @endforeach		
                        </tbody>
                    </table>
                    <p>
                        <a href="{{route('invoice.due')}}" class="btn btn-success btn-sm btn-oval">View All >></a>
                    </p>
                </div>
            </div>
        </div>
    </div>                 
</div>
@endsection
@section('js')
    <script>
        $(document).ready(function(){
            $("#sidebar-menu li").removeClass('active');
            $("#menu_dashboard").addClass('active');
        });
    </script>
@stop