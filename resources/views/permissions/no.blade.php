@extends('layouts.app')
@section('header')
    <strong>Warning</strong>
@endsection
@section('content')
    <div class="card card-gray">
        <div class="card-block">
            <p></p>
           <h5 class="text-danger text-center">
               You don't have permission to access this page.
            </h5>
           <p></p>
            
        </div>
    </div>
    
@endsection
@section('js')

   <script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
        });
    </script>
@endsection