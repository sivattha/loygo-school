<!doctype html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Vdoo School | Vdoo Freelance Team</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="{{asset('css/vendor.css')}}">
        <link rel="stylesheet" href="{{asset('fontaw/css/all.min.css')}}">
        <!-- Theme initialization -->
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
		
        <link rel="stylesheet" href="{{asset('css/custom.css')}}">
		
		<!-- js chosen -->
        <link rel="stylesheet" href="{{asset('css/component-chosen.css')}}">
		
	</head>
    <body>
        <div class="main-wrapper">
            <div class="app" id="app">
                <header class="header">
                    <div class="header-block header-block-collapse d-lg-none d-xl-none">
                        <button class="collapse-btn" id="sidebar-collapse-btn">
                            <i class="fa fa-bars"></i>
						</button>
					</div>
                    <div class="header-block header-block-search">

                       @yield('header')

					</div>
                    <div class="header-block header-block-buttons">
						
					</div>
                    <div class="header-block header-block-nav">
                        <ul class="nav-profile">
                            <li class="profile dropdown">
                            
                                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    <div class="img" style="background-image: url('{{asset(Auth::user()->photo)}}')">
                                    </div>
                                    <span class="name"> {{Auth::user()->name}} </span>
                                </a>
                                
                             
                                

                                
                                <div class="dropdown-menu profile-dropdown-menu">
                                    <a class="dropdown-item" href="{{url('user/edit/'.Auth::user()->id)}}">
									<i class="fa fa-user icon"></i> Profile</a>
                                    
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{route('user.logout')}}">
									<i class="fa fa-power-off icon"></i> Logout</a>
								</div>
							</li>
						</ul>
					</div>
				</header>
                <aside class="sidebar">
                    <div class="sidebar-container">
                        <div class="sidebar-header">
                            <div class="brand">
                                <div class="logo">
									<img src="{{asset('logo.png')}}" alt="Logo" width="45">
								</div> Vdoo
							</div>
						</div>
                        {{-- <form action="{{url('student/search')}}" method="GET">
                            <div class="search-box" style='padding: 0 8px 8px 4px;'>
                                <input type="text" name='q' placeholder="search student..."
                                    class='form-control form-control-sm search'> 
                                <button class="btn btn-sm" style="border-radius:4px; margin-bottom:-1px; display:none">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </form>  --}}
                        <nav class="menu">
                            <ul class="sidebar-menu metismenu" id="sidebar-menu">
                                <li id="menu_dashboard">
                                    <a href="{{url('/')}}">
									<i class="fa fa-home text-info"></i> Dashboard</a>
								</li>
                                <li id="student">
                                    <a href="#">
                                        <i class="fas fa-user text-warning"></i> Students <i class="fa arrow"></i>
                                    </a>
                                    <ul class="sidebar-nav" id="student_collapse">
                                        <li id="menu_student">
                                            <a href="{{route('student.index')}}"><i class="fa fa-arrow-right"></i> Students</a>
                                        </li>
                                        <li id="menu_add_student">
                                            <a href="{{route('student.create')}}"><i class="fa fa-arrow-right"></i> Add Student</a>
                                        </li>
                                        <li id="menu_parent">
                                            <a href="{{route('parent.index')}}"><i class="fa fa-arrow-right"></i> Parents</a>
                                        </li>
                                        {{-- <li id="new_student">
                                            <a href="#"><i class="fa fa-arrow-right"></i> Drop Students</a>
                                        </li>
                                        <li id="new_student">
                                            <a href="#"><i class="fa fa-arrow-right"></i> Finished Students</a>
                                        </li> --}}
                                    </ul>
                                </li>
                                <li id="payment">
                                    <a href="#">
                                        <i class="fa fa-dollar text-success"></i> Payments <i class="fa arrow"></i>
                                    </a>
                                    <ul class="sidebar-nav" id="payment_collapse">
                                        <li id="menu_invoice">
                                            <a href="{{route('invoice.index')}}"><i class="fa fa-arrow-right"></i> Invoices</a>
                                        </li>
                                        <li id="menu_add_invoice">
                                            <a href="{{route('invoice.create')}}"><i class="fa fa-arrow-right"></i> Create Invoice</a>
                                        </li>
                                        <li id="menu_due_invoice">
                                            <a href="{{route('invoice.due')}}"><i class="fa fa-arrow-right"></i> Due Invoices</a>
                                        </li>
                                        <li id="menu_payment">
                                            <a href="{{route('invoice.payment')}}"><i class="fa fa-arrow-right"></i> Payments</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="enrollment">
                                    <a href="#">
                                        <i class="fa fa-star text-info"></i> Enrollments <i class="fa arrow"></i>
                                    </a>
                                    <ul class="sidebar-nav" id="enrollment_collapse">
                                        <li id="menu_class">
                                            <a href="{{route('class.index')}}"><i class="fa fa-arrow-right"></i> Classes</a>
                                        </li>
                                        <li id="menu_archive">
                                            <a href="{{route('class.archive')}}"><i class="fa fa-arrow-right"></i> Finished Classes</a>
										</li>
                                        <li id="menu_enrollment">
                                            <a href="{{route('enrollment.index')}}"><i class="fa fa-arrow-right"></i> Enrollments</a>
                                        </li>
                                        <li id="menu_enrollment">
                                            <a href="{{route('enrollment.create')}}"><i class="fa fa-arrow-right"></i> New Enrollment</a>
                                        </li>
                                        
                                    </ul>
								</li>
                                <li id="menu_employee">
                                    <a href="">
                                        <i class="fa fa-users"></i> Employees <i class="fa arrow"></i>
                                    </a>
                                    <ul class="sidebar-nav" id="employee_collapse">
                                        <li id="menu_staff">
                                            <a href="{{url('employee')}}"><i class="fa fa-arrow-right"></i> Staff</a>
                                        </li>
                                        <li id="menu_leave">
                                            <a href="{{url('leave')}}"><i class="fa fa-arrow-right"></i> Leave</a>
                                        </li>
                                        <li id="menu_loan">
                                            <a href="{{url('loan')}}"><i class="fa fa-arrow-right"></i> Loan</a>
                                        </li>
                                        <li id="menu_payroll">
                                            <a href="{{url('payroll')}}"><i class="fa fa-arrow-right"></i> Payroll</a>
                                        </li>
                                    </ul>
                                </li>
                                {{-- <li  id="menu_report">
                                    <a href="">
                                        <i class="fa fa-file text-warning"></i>Report<i class="fa arrow"></i>
									</a>
                                    <ul class="sidebar-nav"  id="report_collapse">
                                        <li id="report_id">
                                            <a href="{{url('report')}}"><i class="fa fa-arrow-right"></i> Student Enrollment</a>
										</li>
                                        <li id="report_class">
                                            <a href="{{url('report/class')}}"><i class="fa fa-arrow-right"></i> Student by Class</a>
										</li>
                                        <li id="report_room">
                                            <a href="{{url('report/room')}}"><i class="fa fa-arrow-right"></i> Student by Room</a>
										</li>
                                        <li id="report_shift">
                                            <a href="{{url('report/shift')}}"><i class="fa fa-arrow-right"></i> Student by Shift</a>
										</li>
									</ul>
								</li> --}}
								<li  id="menu_setting">
                                    <a href="">
                                        <i class="fa fa-cog text-success"></i>Settings<i class="fa arrow"></i>
									</a>
                                    <ul class="sidebar-nav"  id="setting_collapse">
                                        
                                        <li id="room_id">
                                            <a href="{{route('room.index')}}"><i class="fa fa-arrow-right"></i> Rooms</a>
										</li>
										<li id="menu_item">
                                            <a href="{{route('item.index')}}"><i class="fa fa-arrow-right"></i> Items</a>
                                        </li>
                                        <li id="menu_category">
                                            <a href="{{route('category.index')}}"><i class="fa fa-arrow-right"></i> Categories</a>
                                        </li>
                                        <li id="menu_recurring">
                                            <a href="{{route('recurring.index')}}"><i class="fa fa-arrow-right"></i> Recurring Terms</a>
                                        </li>
                                        <li id="menu_shift">
                                            <a href="{{route('shift.index')}}"><i class="fa fa-arrow-right"></i> Shifts</a>
										</li>
									</ul>
								</li>
								
                                <li  id="menu_security">
                                    <a href="">
                                        <i class="fa fa-key text-warning"></i> Security<i class="fa arrow"></i>
									</a>
                                    <ul class="sidebar-nav"  id="security_collapse">
                                        <li id="role_id">
                                            <a href="{{route('role.index')}}"><i class="fa fa-arrow-right"></i> Roles</a>
										</li>
                                       
                                        <li id='menu_user'>
                                            <a href="{{route('user.index')}}"><i class="fa fa-arrow-right"></i> Users</a>
										</li>
									</ul>
								</li>
							</ul>
						</nav>
					</div>
                   
				</aside>
                <div class="sidebar-overlay" id="sidebar-overlay"></div>
                <div class="sidebar-mobile-menu-handle" id="sidebar-mobile-menu-handle"></div>
                <div class="mobile-menu-handle"></div>
                <article class="content dashboard-page">
                    <section class="section">
                        @yield('content')
					</section>
				</article>
                <footer class="footer">
                    <div class="footer-block buttons">
                        <!-- Copy&copy; 2018 by <a href="#" target="_blank">EDC Database</a> -->
					</div>
                    <div class="footer-block author">
                        <ul>
                            <li> Powered by <a href="https://vdoo.biz" target="_blank">Vdoo Freelance Team</a>
							</li>
                            
						</ul>
					</div>
				</footer>
               
			</div>
		</div>
        <!-- Reference block for JS -->
        <div class="ref" id="ref">
            <div class="color-primary"></div>
            <div class="chart">
                <div class="color-primary"></div>
                <div class="color-secondary"></div>
			</div>
		</div>
		
		
		
        <script>
            var burl = "{{url('/')}}";
        </script>
        
		<?php
		if(Request::segment(2) != 'schedule'){
			?>
			<script src="{{asset('js/jquery.min.js')}}"></script>
			<script src="{{asset('js/vendor.js')}}"></script>
			<script src="{{asset('js/app.js')}}"></script>
			<?php
		}
		?>
		
		
		<!-- js chosen -->
        <script src="{{asset('js/chosen.jquery.min.js')}}"></script>
        <script src="{{asset('js/init.js')}}"></script>
        
		
		
		
		<script>
            var burl = "{{url('/')}}";
			$('.number').keypress(function(event) {
				if (event.which != 46 && (event.which < 47 || event.which > 59))
				{
					event.preventDefault();
					if ((event.which == 46) && ($(this).indexOf('.') != -1)) {
						event.preventDefault();
					}
				}
			});
            function chLang(evt, ln)
            {
                evt.preventDefault();
                $.ajax({
                    type: "GET",
                    url: burl + "/language/" + ln,
                    success: function(sms){
                        if(sms>0)
                        {
                            location.reload();
                        }
                    }
                });
            }
		</script>
        @yield('js')
		
	</body>
</html>