@extends('layouts.app')
@section('content')

<div class="card card-gray">
<div class="card-header">
            <div class="header-block">
	
	<strong>បង្កើត ពាក្យសុំច្បាប់ថ្មី</strong> 
    <a href="{{url('leave')}}" class="btn btn-oval btn-primary btn-sm mx-left">
        <i class="fa fa-reply"></i> ត្រលប់ក្រោយ
    </a>
	</div>
	</div>
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('error')}}
				</div>
			</div>
		@endif
		<div class="card-block">
		<form action="{{url('leave/save')}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="row">
				<div class="col-sm-8">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">បុគ្គលិក <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="employee_id" id="employee_id" class="form-control chosen-select">
                                <option value="">-- ជ្រើសរើស --</option>
                                @foreach($employees as $m)
                                    <option value="{{$m->id}}">{{$m->first_name}} {{$m->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ចំនួនថ្ងៃ <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="number" class="form-control" name="total"
								value="{{old('total')}}" required step="0.1">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ចាប់ពី <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="date" class="form-control" name="start_date"  
								value="{{date('Y-m-d')}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ដល់ថ្ងៃ <span class="text-danger">*</span></label>
						<div class="col-sm-8">
                            <input type="date" class="form-control" required name="end_date" 
                                value="{{date('Y-m-d')}}">
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ច្បាប់អនុញ្ញាត <span class="text-danger">*</span></label>
						<div class="col-sm-8">
                            <select name="status" id="status" class="form-control" required>
								<option value="0">អត់ច្បាប់</option>
								<option value="1">មានច្បាប់</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">មូលហេតុ</label>
						<div class="col-sm-8">
							<textarea name="reason" id="reason" cols="30" rows="3" class='form-control'>{{old('reason')}}</textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="submit" class="col-sm-3 form-control-label">&nbsp;</label>
						<div class="col-sm-8">
							<button type="submit" class="btn btn-oval btn-primary">
								<i class="fa fa-save "></i> រក្សាទុក
							</button>
						</div>
					</div>
					
				</div>
				
			</div>
		
		</form>
	</div>
</div>

@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_leave").addClass("active");
		
	})
</script>
@endsection
