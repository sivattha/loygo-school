@extends('layouts.app')
@section('content')
                        
    <div class="card card-gray">
    <div class="card-header">
            <div class="header-block">
        <strong>ច្បាប់ឈប់សម្រាក</strong> 
    <a href="{{url('leave/create')}}"class="btn btn-primary btn-oval btn-sm mx-left">
        <i class="fa fa-plus-circle"></i> បង្កើតថ្មី</a>
            </div>
            </div>
        <div class="card-block">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>ល.រ</th>
                        <th>ឈ្មោះបុគ្គលិក</th>
                        <th>ចំនួន</th>
                        <th>ពីថ្ងៃ</th>
                        <th>ដល់ថ្ងៃ</th>
                        <th>មូលហេតុ</th>
                        <th>ស្ថានភាព</th>
                        <th>សកម្មភាព</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = 18 * ($pagex - 1) + 1;
                    ?>
                    @foreach($leaves as $l)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{url('leave/detail/'.$l->id)}}">{{$l->first_name}} {{$l->last_name}}</a>
                            </td>
                            <td>{{$l->total}} ថ្ងៃ</td>
                            <td>{{$l->start_date}}</td>
                            <td>{{$l->end_date}}</td>
                            <td>{{$l->reason}}</td>
                            <td>
                                @if($l->status==0)
                                    អត់ច្បាប់
                                @else
                                    មានច្បាប់
                                @endif
                            </td>
                            <td>
                                <a href="{{url('leave/edit/'.$l->id)}}" class="text-success" title="កែប្រែ">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{url('leave/delete/'.$l->id)}}" class="text-danger" title="លុប" onclick="return confirm('តើអ្នកពិតជាចង់លុបមែនទេ?')">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$leaves->links()}}
        </div>
    </div>

@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_employee").addClass("active open");
		    $("#employee_collapse").addClass("collapse in");
		    $("#menu_leave").addClass("active");
        });
    </script>
@endsection