@extends('layouts.app')
@section('content')

<div class="card card-gray">
<div class="card-header">
            <div class="header-block">

	<strong>លម្អិត ពាក្យសុំច្បាប់ថ្មី</strong> 
    <a href="{{url('leave')}}" class="btn btn-oval btn-primary btn-sm mx-left">
        <i class="fa fa-reply"></i> ត្រលប់ក្រោយ
    </a>
	</div>
	</div>
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('error')}}
				</div>
			</div>
		@endif
		<div class="card-block">
		<form>
			<div class="row">
				<div class="col-sm-6">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">បុគ្គលិក</label>
                        <div class="col-sm-8">
                            : {{$leave->first_name}} {{$leave->last_name}}
                        </div>
                    </div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ចំនួនថ្ងៃ</label>
						<div class="col-sm-8">
							: {{$leave->total}} ថ្ងៃ
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ចាប់ពី</label>
						<div class="col-sm-8">
							: {{$leave->start_date}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ដល់ថ្ងៃ</label>
						<div class="col-sm-8">
                            : {{$leave->end_date}}
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">មូលហេតុ</label>
						<div class="col-sm-8">
                            : {{$leave->reason}}
						</div>
					</div>
					<div class="form-group row">
                        <label class="col-sm-3 form-control-label">ស្ថានភាព</label>
                        <div class="col-sm-8">
                            @if($leave->status==0)
                                អត់ច្បាប់
                            @else
                                មានច្បាប់
                            @endif
                        </div>
                    </div>
				</div>
				<div class="col-sm-6">
                    
                </div>
			</div>
		
		</form>
	</div>
</div>

@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_leave").addClass("active");
		
	})
</script>
@endsection
