@extends('layouts.app')
@section('content')

<div class="card card-gray">
<div class="card-header">
            <div class="header-block">

	<strong>បង្កើត ខ្ចីប្រាក់</strong> 
    <a href="{{url('loan')}}" class="btn btn-oval btn-primary btn-sm mx-left">
        <i class="fa fa-reply"></i> ត្រលប់ក្រោយ
    </a>
	</div>
        </div>
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('error')}}
				</div>
			</div>
		@endif
		<div class="card-block">
		<form action="{{url('loan/save')}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="row">
				<div class="col-sm-6">
                    <div class="form-group row">
                        <label class="col-sm-3 form-control-label">បុគ្គលិក <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                            <select name="employee_id" id="employee_id" class="form-control chosen-select">
                                <option value="">-- ជ្រើសរើស --</option>
                                @foreach($employees as $m)
                                    <option value="{{$m->id}}">{{$m->first_name}} {{$m->last_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
					
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ថ្ងៃខ្ចី <span class="text-danger">*</span></label>
						<div class="col-sm-8">
							<input type="date" class="form-control" name="loan_date"  
								value="{{date('Y-m-d')}}" required>
						</div>
					</div>
					<div class="form-group row">
						<label class="col-sm-3 form-control-label">ចំនួន <span class="text-danger">*</span></label>
						<div class="col-sm-8">
                            <input type="text" class="form-control" required name="amount" >
						</div>
					</div>
					
					<div class="form-group row">
						<label for="submit" class="col-sm-3 form-control-label">&nbsp;</label>
						<div class="col-sm-8">
							<button type="submit" class="btn btn-oval btn-primary">
								<i class="fa fa-save "></i> រក្សាទុក
							</button>
						</div>
					</div>
					
				</div>
				
			</div>
		
		</form>
	</div>
</div>

@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_loan").addClass("active");
		
	})
</script>
@endsection
