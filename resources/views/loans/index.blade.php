@extends('layouts.app')
@section('content')
                        
    <div class="card card-gray">
         <div class="card-header">
            <div class="header-block">
        <strong>ខ្ចីប្រាក់</strong> 
    <a href="{{url('loan/create')}}"class="btn btn-primary btn-oval btn-sm mx-left">
        <i class="fa fa-plus-circle"></i> បង្កើតថ្មី</a>
        </div>
        </div>
        <div class="card-block">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>ល.រ</th>
                        <th>ឈ្មោះបុគ្គលិក</th>
                        <th>ថ្ងៃខ្ចី</th>
                        <th>ចំនួន</th>
                        <th>សកម្មភាព</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = 18 * ($pagex - 1) + 1;
                    ?>
                    @foreach($loans as $l)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                {{$l->first_name}} {{$l->last_name}}
                            </td>
                            <td>{{$l->loan_date}}</td>
                            <td>$ {{$l->amount}}</td>
                            
                            <td>
                                <a href="{{url('loan/edit/'.$l->id)}}" class="text-success" title="កែប្រែ">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{url('loan/delete/'.$l->id)}}" class="text-danger" title="លុប" onclick="return confirm('តើអ្នកពិតជាចង់លុបមែនទេ?')">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$loans->links()}}
        </div>
    </div>

@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_employee").addClass("active open");
		    $("#employee_collapse").addClass("collapse in");
		    $("#menu_loan").addClass("active");
        });
    </script>
@endsection