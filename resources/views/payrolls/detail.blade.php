@extends('layouts.app')
@section('content')
                        
    <div class="card card-gray">
    <div class="card-header">
            <div class="header-block">
        <strong>លម្អិត បញ្ជីប្រាក់ខែ</strong> 
    <a href="{{url('payroll')}}" class="btn btn-oval btn-primary btn-sm mx-left">
        <i class="fa fa-reply"></i> ត្រលប់ក្រោយ
    </a>
    </div>
        </div>
        <div class="card-block">
            <p class='text-success'>បញ្ជីប្រាក់ខែសម្រាប់: {{date_format(date_create($payroll->payroll_date), 'm - Y')}}</p>
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ឈ្មោះ</th>
                        <th>ប្រាក់ខែ</th>
                        <th>ប្រាក់ខ្ចី</th>
                        <th>ឈប់អត់ច្បាប់</th>
                        <th>ឈប់មានច្បាប់</th>
                        <th>ប្រាក់កាត់</th>
                        <th>សរុប</th>
                    </tr>
                </thead>
                <tbody>
                    @php($i=1)
                    @foreach($details as $d)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>{{$d->first_name}} {{$d->last_name}}</td>
                            <td>$ {{$d->salary}}</td>
                            <td>$ {{$d->loan}}</td>
                            <td>{{$d->leave1}} ថ្ងៃ</td>
                            <td>{{$d->leave2}} ថ្ងៃ</td>
                            <td>$ {{$d->cut}}</td>
                            <td>$ {{$d->total}}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan='7' class='text-success text-right'>សរុប</td>
                        <td class='text-success'>$ {{$payroll->total}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_employee").addClass("active open");
		    $("#employee_collapse").addClass("collapse in");
		    $("#menu_payroll").addClass("active");
        });
    </script>
@endsection