@extends('layouts.app')
@section('content')

<div class="card card-gray">
<div class="card-header">
            <div class="header-block">
	<strong>បង្កើត បញ្ជីប្រាក់ខែ</strong> 
    <a href="{{url('payroll')}}" class="btn btn-oval btn-primary btn-sm mx-left">
        <i class="fa fa-reply"></i> ត្រលប់ក្រោយ
    </a>
	</div>
        </div>
		@if(Session::has('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('success')}}
				</div>
			</div>
		@endif
		@if(Session::has('error'))
			<div class="alert alert-danger" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div>
					{{session('error')}}
				</div>
			</div>
		@endif
		<div class="card-block">
		<form action="{{url('payroll/save')}}" method="POST" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="row">
				<div class="col-sm-8">
					
					<div class="form-group row">
						<label class="col-sm-2 form-control-label">ខែ - ឆ្នាំ<span class="text-danger">*</span></label>
						<div class="col-sm-10">
							<div class="row">
								<div class="col-sm-6">
									<select name="month" id="month" class="form-control">
									<?php $m = date('m'); ?>
									@for($i=1;$i<=12;$i++)
										<option value="{{$i}}" {{$i==$m?'selected':''}}>{{$i}}</option>
									@endfor
									</select>
								</div>
								<div class="col-sm-6">
									<select name="year" id="year" class="form-control">
									<?php $y = date('Y'); ?>
									@for($i=2019;$i<=2025;$i++)
										<option value="{{$i}}" {{$i==$y?'selected':''}}>{{$i}}</option>
									@endfor
									</select>
								</div>
							</div>
						</div>
					</div>
					
					<div class="form-group row">
						<label for="submit" class="col-sm-2 form-control-label">&nbsp;</label>
						<div class="col-sm-8">
							<button type="submit" class="btn btn-oval btn-primary">
								<i class="fa fa-save "></i> រក្សាទុក
							</button>
						</div>
					</div>
					
				</div>
				
			</div>
		
		</form>
	</div>
</div>

@endsection
@section('js')
<script>
	$(document).ready(function () {
		$("#sidebar-menu li ").removeClass("active open");
		$("#sidebar-menu li ul li").removeClass("active");
		
		$("#menu_employee").addClass("active open");
		$("#employee_collapse").addClass("collapse in");
		$("#menu_payroll").addClass("active");
		
	})
</script>
@endsection
