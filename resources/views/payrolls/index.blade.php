@extends('layouts.app')

@section('content')
                        
    <div class="card card-gray">
    <div class="card-header">
            <div class="header-block">
        <strong>បញ្ជីប្រាក់ខែ</strong> 
    <a href="{{url('payroll/create')}}"class="btn btn-primary btn-oval btn-sm mx-left">
        <i class="fa fa-plus-circle"></i> បង្កើតថ្មី</a>
        </div>
        </div>
        <div class="card-block">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>ល.រ</th>
                        <th>ខែឆ្នាំ</th>
                        <th>សរុប</th>
                        <th>សកម្មភាព</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $pagex = @$_GET['page'];
                    if(!$pagex)
                        $pagex = 1;
                    $i = 12 * ($pagex - 1) + 1;
                    ?>
                    @foreach($payrolls as $l)
                        <tr>
                            <td>{{$i++}}</td>
                            <td>
                                <a href="{{url('payroll/detail/'.$l->id)}}"> {{date_format(date_create($l->payroll_date), 'm-Y')}}</a>
                            </td>
                            <td>$ {{$l->total}}</td>
                            <td>
                                
                                <a href="{{url('payroll/delete/'.$l->id)}}" class="text-danger" title="លុប" onclick="return confirm('តើអ្នកពិតជាចង់លុបមែនទេ?')">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{$payrolls->links()}}
        </div>
    </div>

@endsection

@section('js')
	<script>
        $(document).ready(function () {
            $("#sidebar-menu li ").removeClass("active open");
			$("#sidebar-menu li ul li").removeClass("active");
			
            $("#menu_employee").addClass("active open");
		    $("#employee_collapse").addClass("collapse in");
		    $("#menu_payroll").addClass("active");
        });
    </script>
@endsection